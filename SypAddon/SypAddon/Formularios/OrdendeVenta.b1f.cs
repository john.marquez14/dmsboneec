﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace DmsB1AddOn.Formularios
{
    [FormAttribute("139", "Formularios/OrdendeVenta.b1f")]
    class OrdendeVenta : SystemFormBase
    {
        public OrdendeVenta()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.btnLote = ((SAPbouiCOM.Button)(this.GetItem("btnLote").Specific));
            this.btnPagopr = ((SAPbouiCOM.Button)(this.GetItem("btnPagopr").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            btnPagopr.Item.FromPane = 100;
            btnPagopr.Item.ToPane = 100;
        }

        private SAPbouiCOM.Button btnLote, btnPagopr;

    }
}
