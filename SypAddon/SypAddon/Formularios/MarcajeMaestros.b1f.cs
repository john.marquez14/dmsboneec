﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using System.Drawing;
using SypDevs;
using SAPbouiCOM;
using SypAddonController.Servicio;
using SypAddonController.Util;

namespace DmsB1AddOn.Formularios
{
    [FormAttribute("DmsB1AddOn.Formularios.MarcajeMaestros", "Formularios/MarcajeMaestros.b1f")]
    class MarcajeMaestros : UserFormBase
    {
        private readonly DmsBOneEc _dmsBOneEc = new DmsBOneEc();
        private int numeroLinea = 0;
        private string motivoGlobal = "";

        public MarcajeMaestros()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.btnIni = ((SAPbouiCOM.Button)(this.GetItem("btnIni").Specific));
            this.btnIni.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnIni_ClickBefore);
            this.lblEtiNom = ((SAPbouiCOM.StaticText)(this.GetItem("Item_1").Specific));
            this.lblEtiOt = ((SAPbouiCOM.StaticText)(this.GetItem("Item_2").Specific));
            this.btnFin = ((SAPbouiCOM.Button)(this.GetItem("btnFin").Specific));
            this.btnFin.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnFin_ClickBefore);
            this.btnPau = ((SAPbouiCOM.Button)(this.GetItem("btnPau").Specific));
            this.btnPau.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnPau_ClickBefore);
            this.btnSeg = ((SAPbouiCOM.Button)(this.GetItem("btnSeg").Specific));
            this.btnSeg.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnSeg_ClickBefore);
            this.nro1 = ((SAPbouiCOM.Button)(this.GetItem("nro1").Specific));
            this.nro1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro1_ClickBefore);
            this.lblMae = ((SAPbouiCOM.StaticText)(this.GetItem("lblMae").Specific));
            this.lblOt = ((SAPbouiCOM.StaticText)(this.GetItem("lblOt").Specific));
            this.mtxMaes = ((SAPbouiCOM.Matrix)(this.GetItem("mtxMaes").Specific));
            this.mtxMaes.LinkPressedBefore += new SAPbouiCOM._IMatrixEvents_LinkPressedBeforeEventHandler(this.mtxMaes_LinkPressedBefore);
            this.nro2 = ((SAPbouiCOM.Button)(this.GetItem("nro2").Specific));
            this.nro2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro2_ClickBefore);
            this.nro3 = ((SAPbouiCOM.Button)(this.GetItem("nro3").Specific));
            this.nro3.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro3_ClickBefore);
            this.nro4 = ((SAPbouiCOM.Button)(this.GetItem("nro4").Specific));
            this.nro4.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro4_ClickBefore);
            this.nro5 = ((SAPbouiCOM.Button)(this.GetItem("nro5").Specific));
            this.nro5.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro5_ClickBefore);
            this.nro6 = ((SAPbouiCOM.Button)(this.GetItem("nro6").Specific));
            this.nro6.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro6_ClickBefore);
            this.nro7 = ((SAPbouiCOM.Button)(this.GetItem("nro7").Specific));
            this.nro7.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro7_ClickBefore);
            this.nro8 = ((SAPbouiCOM.Button)(this.GetItem("nro8").Specific));
            this.nro8.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro8_ClickBefore);
            this.nro9 = ((SAPbouiCOM.Button)(this.GetItem("nro9").Specific));
            this.nro9.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro9_ClickBefore);
            this.nro10 = ((SAPbouiCOM.Button)(this.GetItem("nro10").Specific));
            this.nro10.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro10_ClickBefore);
            this.nro11 = ((SAPbouiCOM.Button)(this.GetItem("nro11").Specific));
            this.nro11.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro11_ClickBefore);
            this.btnNew = ((SAPbouiCOM.Button)(this.GetItem("btnNew").Specific));
            this.btnNew.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnNew_ClickBefore);
            this.txtOt = ((SAPbouiCOM.EditText)(this.GetItem("txtOt").Specific));
            this.btnVal = ((SAPbouiCOM.Button)(this.GetItem("btnVal").Specific));
            this.btnVal.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnVal_ClickBefore);
            this.nro12 = ((SAPbouiCOM.Button)(this.GetItem("nro12").Specific));
            this.nro12.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro12_ClickBefore);
            this.nro13 = ((SAPbouiCOM.Button)(this.GetItem("nro13").Specific));
            this.nro13.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro13_ClickBefore);
            this.nro14 = ((SAPbouiCOM.Button)(this.GetItem("nro14").Specific));
            this.nro14.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro14_ClickBefore);
            this.nro15 = ((SAPbouiCOM.Button)(this.GetItem("nro15").Specific));
            this.nro15.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro15_ClickBefore);
            this.nro16 = ((SAPbouiCOM.Button)(this.GetItem("nro16").Specific));
            this.nro16.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro16_ClickBefore);
            this.nro17 = ((SAPbouiCOM.Button)(this.GetItem("nro17").Specific));
            this.nro17.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro17_ClickBefore);
            this.nro18 = ((SAPbouiCOM.Button)(this.GetItem("nro18").Specific));
            this.nro18.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro18_ClickBefore);
            this.nro19 = ((SAPbouiCOM.Button)(this.GetItem("nro19").Specific));
            this.nro19.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro19_ClickBefore);
            this.nro20 = ((SAPbouiCOM.Button)(this.GetItem("nro20").Specific));
            this.nro20.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.nro20_ClickBefore);
            this.btnBack = ((SAPbouiCOM.Button)(this.GetItem("btnBack").Specific));
            this.btnBack.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnBack_ClickBefore);
            this.mtxMaes.AutoResizeColumns();
            this.lblEtiObs = ((SAPbouiCOM.StaticText)(this.GetItem("lblEtiObs").Specific));
            this.btnObs = ((SAPbouiCOM.Button)(this.GetItem("btnObs").Specific));
            this.btnObs.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnObs_ClickBefore);
            this.txtLinea = ((SAPbouiCOM.EditText)(this.GetItem("txtLinea").Specific));
            //                  this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.btnQuitar = ((SAPbouiCOM.Button)(this.GetItem("btnQuitar").Specific));
            this.btnQuitar.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnQuitar_ClickBefore);
            this.mtxDet = ((SAPbouiCOM.Matrix)(this.GetItem("mtxDet").Specific));
            this.mtxDet.AutoResizeColumns();
            this.lblBahia = ((SAPbouiCOM.StaticText)(this.GetItem("lblBahia").Specific));
            this.lblNumBah = ((SAPbouiCOM.StaticText)(this.GetItem("lblNumBah").Specific));
            this.btnCambio = ((SAPbouiCOM.Button)(this.GetItem("btnCambio").Specific));
            this.btnCambio.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnCambio_ClickBefore);
            this.tabMar = ((SAPbouiCOM.Folder)(this.GetItem("tabMar").Specific));
            this.tabOpc = ((SAPbouiCOM.Folder)(this.GetItem("tabOpc").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            #region Paneles
            //(ini)Se definen los objetos del panel #1
            mtxDet.Item.FromPane = 1;
            mtxDet.Item.ToPane = 1;
            mtxMaes.Item.FromPane = 1;
            mtxMaes.Item.ToPane = 1;
            //(fin)Se definen los objetos del panel #1

            #endregion
            tabOpc.Select();
            #region Apariencia Interfaz
            lblEtiNom.Item.Height = 18;
            lblEtiNom.Item.FontSize = 18;
            lblEtiNom.Item.ForeColor = ColorTranslator.ToOle(Color.Brown);

            lblEtiOt.Item.Height = 18;
            lblEtiOt.Item.FontSize = 18;
            lblEtiOt.Item.ForeColor = ColorTranslator.ToOle(Color.Brown);

            lblEtiObs.Item.Height = 18;
            lblEtiObs.Item.FontSize = 18;
            lblEtiObs.Item.ForeColor = ColorTranslator.ToOle(Color.Brown);

            lblBahia.Item.Height = 18;
            lblBahia.Item.FontSize = 18;
            lblBahia.Item.ForeColor = ColorTranslator.ToOle(Color.Brown);

            lblMae.Item.Height = 18;
            lblMae.Item.FontSize = 18;
            lblMae.Item.ForeColor = ColorTranslator.ToOle(Color.DarkBlue);
            lblMae.Caption = "JOHN MARQUEZ M.";//_dmsBOneEc.ObtieneUserName(Globals.oCompany.UserName.ToUpper());

            lblOt.Item.Height = 18;
            lblOt.Item.FontSize = 18;
            lblOt.Item.ForeColor = ColorTranslator.ToOle(Color.DarkBlue);
            lblOt.Caption = "";

            lblNumBah.Item.Height = 18;
            lblNumBah.Item.FontSize = 18;
            lblNumBah.Item.ForeColor = ColorTranslator.ToOle(Color.DarkBlue);

            txtOt.Item.Height = 18;
            txtOt.Item.FontSize = 18;
            txtOt.Item.ForeColor = ColorTranslator.ToOle(Color.DarkBlue);

            nro1.Item.TextStyle = 1;
            nro2.Item.TextStyle = 1;
            nro3.Item.TextStyle = 1;
            nro4.Item.TextStyle = 1;
            nro5.Item.TextStyle = 1;
            nro6.Item.TextStyle = 1;
            nro7.Item.TextStyle = 1;
            nro8.Item.TextStyle = 1;
            nro9.Item.TextStyle = 1;
            nro10.Item.TextStyle = 1;
            nro11.Item.TextStyle = 1;
            nro12.Item.TextStyle = 1;
            nro13.Item.TextStyle = 1;
            nro14.Item.TextStyle = 1;
            nro15.Item.TextStyle = 1;
            nro16.Item.TextStyle = 1;
            nro17.Item.TextStyle = 1;
            nro18.Item.TextStyle = 1;
            nro19.Item.TextStyle = 1;
            nro20.Item.TextStyle = 1;
            btnIni.Item.TextStyle = 1;
            btnFin.Item.TextStyle = 1;
            btnPau.Item.TextStyle = 1;
            btnSeg.Item.TextStyle = 1;
            btnNew.Item.TextStyle = 1;
            btnVal.Item.TextStyle = 1;
            btnBack.Item.TextStyle = 1;
            btnObs.Item.TextStyle = 1;
            btnQuitar.Item.TextStyle = 1;

            nro1.Item.FontSize = 20;
            nro2.Item.FontSize = 20;
            nro3.Item.FontSize = 20;
            nro4.Item.FontSize = 20;
            nro5.Item.FontSize = 20;
            nro6.Item.FontSize = 20;
            nro7.Item.FontSize = 20;
            nro8.Item.FontSize = 20;
            nro9.Item.FontSize = 20;
            nro10.Item.FontSize = 20;
            nro11.Item.FontSize = 20;
            nro12.Item.FontSize = 20;
            nro13.Item.FontSize = 20;
            nro14.Item.FontSize = 20;
            nro15.Item.FontSize = 20;
            nro16.Item.FontSize = 20;
            nro17.Item.FontSize = 20;
            nro18.Item.FontSize = 20;
            nro19.Item.FontSize = 20;
            nro20.Item.FontSize = 20;
            btnIni.Item.FontSize = 20;
            btnFin.Item.FontSize = 20;
            btnPau.Item.FontSize = 20;
            btnSeg.Item.FontSize = 20;
            btnNew.Item.FontSize = 20;
            btnVal.Item.FontSize = 20;
            btnBack.Item.FontSize = 20;
            btnObs.Item.FontSize = 18;
            btnQuitar.Item.FontSize = 18;
            

            mtxMaes.Item.FontSize = 20;

            txtOt.Item.Visible = false;
            btnVal.Item.Visible = false;
            btnBack.Item.Visible = false;
            //txtLinea.Item.Visible = false;
            #endregion Apariencia Interfaz 
        }

        private StaticText lblEtiNom, lblEtiOt, lblMae, lblOt, lblEtiObs;

        private void btnNew_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            txtOt.Item.Visible = true;
            btnVal.Item.Visible = true;
            btnBack.Item.Visible = true;
            txtOt.Value = "";
            lblOt.Caption = "";
        }

        

        private void btnVal_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            txtOt.Item.Visible = false;
            btnVal.Item.Visible = false;
            btnBack.Item.Visible = false;

            Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
            _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);

            lblOt.Caption = txtOt.Value;
        }

        private SAPbouiCOM.Button btnIni, btnFin, btnPau, btnSeg, btnNew, btnVal, btnBack, btnObs, btnQuitar, nro1, nro2, nro3, nro4, nro5, nro6, nro7, nro8, nro9, nro10, nro11, nro12, nro13, nro14, nro15, nro16, nro17, nro18, nro19, nro20;
        private SAPbouiCOM.Button obs1, obs2, obs3, obs4, obs5, obs6, obs7, obs8, obs9, obs10, obs11, obs12, obs13, obs14, obs15, obs16, obs17, obs18;

        private void btnQuitar_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivosBorrar(pVal);
        }

        private void btnFin_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            BubbleEvent = true;
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                _dmsBOneEc.ActualizaInicio(row, codUdo.Value, ot, "F", motivoGlobal);
                motivoGlobal = "";
                _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }
        }

        private void mtxMaes_LinkPressedBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

        }

        private void btnSeg_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                _dmsBOneEc.ActualizaInicio(row, codUdo.Value, ot, "PF", motivoGlobal);
                motivoGlobal = "";
                _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }
        }

        private void btnPau_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                _dmsBOneEc.ActualizaInicio(row, codUdo.Value, ot, "PI", motivoGlobal);
                motivoGlobal = "";
                _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }
        }

        private void btnIni_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                _dmsBOneEc.ActualizaInicio(row, codUdo.Value, ot, "I", motivoGlobal);
                motivoGlobal = "";
                _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }

        }

        private void btnObs_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
            #region MotivosMarcaje
            try
            {
                int widthForm = oForm.Width;

                if (widthForm == 690)
                    oForm.Width = 970;

                if (widthForm == 970)
                    oForm.Width = 690;

                int left = 700;
                int top = 64;
                int width = 245;
                int heigt = 33;
                string id = "obs";
                int con = 0;
                try
                {
                    oForm.Items.Item("obs1");
                }
                catch
                {
                    foreach (var mot in _dmsBOneEc.ConsultaMotivosMarcaje())
                    {
                        con++;
                        _dmsBOneEc.AgregarBotonMotivos(oForm, id + mot.id, left, top, width, heigt, mot.motivo);
                        top = top + 40;
                    }

                    #region Declaracion de metodos dinamicos
                    if (1 <= con)
                    {
                        this.obs1 = ((SAPbouiCOM.Button)(this.GetItem("obs1").Specific));
                        this.obs1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs1_ClickBefore);
                    }

                    if (2 <= con)
                    {
                        this.obs2 = ((SAPbouiCOM.Button)(this.GetItem("obs2").Specific));
                        this.obs2.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs2_ClickBefore);
                    }

                    if (3 <= con)
                    {
                        this.obs3 = ((SAPbouiCOM.Button)(this.GetItem("obs3").Specific));
                        this.obs3.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs3_ClickBefore);
                    }

                    if (4 <= con)
                    {
                        this.obs4 = ((SAPbouiCOM.Button)(this.GetItem("obs4").Specific));
                        this.obs4.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs4_ClickBefore);
                    }

                    if (5 <= con)
                    {
                        this.obs5 = ((SAPbouiCOM.Button)(this.GetItem("obs5").Specific));
                        this.obs5.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs5_ClickBefore);
                    }

                    if (6 <= con)
                    {
                        this.obs6 = ((SAPbouiCOM.Button)(this.GetItem("obs6").Specific));
                        this.obs6.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs6_ClickBefore);
                    }

                    if (7 <= con)
                    {
                        this.obs7 = ((SAPbouiCOM.Button)(this.GetItem("obs7").Specific));
                        this.obs7.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs7_ClickBefore);
                    }

                    if (8 <= con)
                    {
                        this.obs8 = ((SAPbouiCOM.Button)(this.GetItem("obs8").Specific));
                        this.obs8.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs8_ClickBefore);
                    }

                    if (9 <= con)
                    {
                        this.obs9 = ((SAPbouiCOM.Button)(this.GetItem("obs9").Specific));
                        this.obs9.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs9_ClickBefore);
                    }

                    if (10 <= con)
                    {
                        this.obs10 = ((SAPbouiCOM.Button)(this.GetItem("obs10").Specific));
                        this.obs10.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs1_ClickBefore);
                    }

                    if (11 <= con)
                    {
                        this.obs11 = ((SAPbouiCOM.Button)(this.GetItem("obs11").Specific));
                        this.obs11.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs1_ClickBefore);
                    }

                    if (12 <= con)
                    {
                        this.obs12 = ((SAPbouiCOM.Button)(this.GetItem("obs12").Specific));
                        this.obs12.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs1_ClickBefore);
                    }

                    if (13 <= con)
                    {
                        this.obs13 = ((SAPbouiCOM.Button)(this.GetItem("obs13").Specific));
                        this.obs13.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs1_ClickBefore);
                    }

                    if (14 <= con)
                    {
                        this.obs14 = ((SAPbouiCOM.Button)(this.GetItem("obs14").Specific));
                        this.obs14.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs14_ClickBefore);
                    }

                    if (15 <= con)
                    {
                        this.obs15 = ((SAPbouiCOM.Button)(this.GetItem("obs15").Specific));
                        this.obs15.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.obs15_ClickBefore);
                    }
                    #endregion

                }


            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(MotivosMarcaje)  : " + ex.Message);
            }
            #endregion

        }

        private void obs1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 1);
        }

        private void btnCambio_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            

            if (tabMar.Selected)
            {
                tabOpc.Select();
            }
            else
            {
                tabMar.Select();
            }
               

            
                

        }

        private void GestionaMotivos(SBOItemEventArg pVal, int motivo)
        {
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                string idMotivo = motivo.ToString();
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                //_dmsBOneEc.ActualizaMotivos(row, codUdo.Value, ot, _dmsBOneEc.ObtieneMotivo(idMotivo));
                motivoGlobal = _dmsBOneEc.ObtieneMotivo(idMotivo);
                //_dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);
                
                //Utils.MostrarMensajeFlotante(motivo.ToString(), Utils.TiposMensajesFlotantes.Normal);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }
        }

        private void GestionaMotivosBorrar(SBOItemEventArg pVal)
        {
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                int row = Convert.ToInt32(txtLinea.Value);
                string ot = lblOt.Caption;
                EditText codUdo = (EditText)oMatrix.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(numeroLinea).Specific;

                _dmsBOneEc.ActualizaMotivosBorrar(row, codUdo.Value, ot);
                _dmsBOneEc.CargaTareasMarcaje(oForm, txtOt.Value);

                //mtxMaes.SelectRow(nro, true, false);
                //Utils.MostrarMensajeFlotante(motivo.ToString(), Utils.TiposMensajesFlotantes.Normal);
            }
            catch (Exception)
            {
                Utils.MostrarMensaje("Seleccione una linea", Utils.TiposMensajes.SmtWarning);
            }
        }

        private void obs2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 2);
        }

        private void obs3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 3);
        }

        private void obs4_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 4);
        }

        private void obs5_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 5);
        }

        private void obs6_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 6);
        }

        private void obs7_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 7);
        }

        private void obs8_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 8);
        }

        private void obs9_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 9);
        }

        private void obs10_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 10);
        }

        private void obs11_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 11);
        }

        private void obs12_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 12);
        }

        private void obs13_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 13);
        }

        private void obs14_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 14);
        }

        private void obs15_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            GestionaMotivos(pVal, 15);
        }

        private void nro20_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(20);
        }

        private void nro19_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(19);
        }

        private void nro18_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(18);
        }

        private void nro17_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(17);
        }

        private void nro16_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(16);
        }

        private void nro15_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(15);
        }

        private void nro14_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(14);
        }

        private void nro13_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(13);
        }

        private void nro12_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(12);
        }

        private void nro11_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(11);
        }

        private void nro10_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(10);
        }

        private void nro9_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(9);
        }

        private void nro8_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(8);
        }

        private void nro7_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(7);
        }

        private void nro6_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(6);
        }

        private void nro5_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(5);
        }

        private void nro4_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(4);
        }

        private void nro3_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(3);
        }

        private void btnBack_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            BorrarDigitos();
        }

        private void nro2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SeleccionDigitos(2);
        }

        private void nro1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            int val = lblOt.Caption.Length;

            SeleccionDigitos(1);
        }

        private SAPbouiCOM.Matrix mtxMaes;
        private SAPbouiCOM.EditText txtOt, txtLinea;

        private void SeleccionDigitos(int nro)
        {
            try
            {
                numeroLinea = nro;
                string texto = txtOt.Value;

                if (numeroLinea == 10)
                    numeroLinea = 0;

                string boton = numeroLinea.ToString();

                string textoNuevo = String.Concat(texto, boton);

                int val = lblOt.Caption.Length;

                if (val == 0)
                {
                    if (numeroLinea < 11)
                        txtOt.Value = textoNuevo;
                }
                else
                {
                    mtxMaes.SelectRow(nro, true,false);

                    EditText codUdo = (EditText)mtxMaes.Columns.Item(Constantes.MTX_CODUDO).Cells.Item(nro).Specific;
                    txtLinea.Active = true;
                    txtLinea.Value = codUdo.Value;
                    //Utils.MostrarMensajeFlotante("Boom", Utils.TiposMensajesFlotantes.Normal);
                }
                    
            }
            catch (Exception) { }
        }

        private void BorrarDigitos()
        {
            try
            {
                string texto = txtOt.Value;
                int cantidadLetras = texto.Length - 1;

                string textoNuevo = texto.Substring(0, cantidadLetras);

                txtOt.Value = textoNuevo;
            }
            catch (Exception) { }
        }

        private StaticText StaticText0;
        private StaticText StaticText1;
        private Matrix mtxDet;
        private StaticText lblBahia;
        private StaticText lblNumBah;
        private Button btnCambio;
        private Folder tabOpc, tabMar;
    }
}
