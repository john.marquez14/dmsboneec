﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using SypAddonController.DBStructure;
using SypAddonController.Modelo;
using SypAddonController.Servicio;
using SypAddonController.Util;
using SypDevs;
using DmsB1AddOn.Formularios;
using SAPbobsCOM;

namespace SypAddon.App
{
    public class SypMain
    {
        #region General
        public static bool enCall; //Para controlar si se encuentra en una llamada de Servicio o No.
        public Form oFormOT;
        public static string numeroOt;
        //public static string claseVenta;
        public static string sucursal;
        public static string numSiniestro;
        public static string codCiaSeguros;
        public static string desCiaSeguros;
        public static string liquidador;
        public static string cliente;

        private readonly DmsBOneEc _dmsBOneEc = new DmsBOneEc();
        #endregion General

        #region Iniciar
        public void Iniciar()
        {
            Connect.ConnectToCompany();
            SetFilters();
            Globals.SAPVersion = Globals.oCompany.Version;

            //CREACIÓN DE ESTRUCTURAS
            Globals.ORec = Globals.varificarAddon(Constantes.NombreAddon);
            Globals.Addon = Globals.ORec.Fields.Item(0).Value.ToString();
            Globals.Version = Globals.ORec.Fields.Item(1).Value.ToString();
            Globals.release(Globals.ORec);

            if (Globals.Addon == "")
            {
                Globals.Addon = Assembly.GetEntryAssembly().GetName().Name;
                Version version = Assembly.GetEntryAssembly().GetName().Version;
                Globals.Version = version.ToString().Replace(".0.0", "");
            }

            Setup oSetup = new Setup();

            Globals.Actual = oSetup.validarVersion(Globals.Addon, Globals.Version);
            if (Globals.Actual == false)
            {

                CreateStructure.CrearEstructuras();
                oSetup.confirmarVersion(Globals.Addon, Globals.Version);
                oSetup.confirmarVersionUpdate(Globals.Addon, Globals.Version);
            }

            InitMenu.AddMenuItems();

            //VARIABLE QUE PERMITE QUE EL PROGRAMA SE EJECUTE CONTINUAMENTE
            Globals.continuar = 0;

            Globals.SBO_Application.SetStatusBarMessage("Addon " + Constantes.NombreAddon + " Conectado", BoMessageTime.bmt_Medium, false);
        }
        #endregion Iniciar

        #region SBO_Application_AppEvent
        private void SBO_Application_AppEvent(BoAppEventTypes eventType)
        {

            switch (eventType)
            {
                case BoAppEventTypes.aet_ShutDown:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_CompanyChanged:
                    break;
                case BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }
        #endregion SBO_Application_AppEvent

        #region SBO_Application_MenuEvent
        public void SBO_Application_MenuEvent(ref MenuEvent pVal, out bool bubbleEvent)
        {
            bubbleEvent = true;
            Form oForm = null;
            try
            {
                if (pVal.BeforeAction && pVal.MenuUID == "MTEC")
                {
                    MarcajeMaestros activeForm = new MarcajeMaestros();
                    activeForm.Show();
                }

                #region CabeceraFormularios
                //if (pVal.BeforeAction == false)
                //{
                //    switch (pVal.MenuUID)
                //    {
                //        case "3587":
                //            oForm = Globals.SBO_Application.Forms.ActiveForm;
                //            oForm.Title = "Orden de Trabajo";
                //            break;

                //        case "3591":
                //            oForm = Globals.SBO_Application.Forms.ActiveForm;
                //            oForm.Title = "Tarjeta de Vehículo";
                //            break;
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.MessageBox("MenuEvent : " + ex.Message);
            }
        }
        #endregion SBO_Application_MenuEvent

        #region SBO_Application_ItemEvent
        public void SBO_Application_ItemEvent(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);

                if (pVal.FormTypeEx == "60117")
                {
                    if (pVal.EventType == BoEventTypes.et_FORM_LOAD & pVal.ActionSuccess & pVal.BeforeAction == false) enCall = true;
                    if (pVal.EventType == BoEventTypes.et_FORM_CLOSE & pVal.ActionSuccess & pVal.BeforeAction == false) enCall = false;
                }

                #region Orden de venta u Oferta de venta - entrega Cuando se abre desde una llamada de servicio 
                if (pVal.FormTypeEx == "-139" || pVal.FormTypeEx == "-149" || pVal.FormTypeEx == "-140"
                        || pVal.FormTypeEx == "139" || pVal.FormTypeEx == "149" || pVal.FormTypeEx == "140")
                {
                    #region Paso de informacion de OV a Llamada de Servicio
                    if (pVal.EventType == BoEventTypes.et_FORM_LOAD & pVal.ActionSuccess & pVal.BeforeAction == false & enCall)
                    {

                        try
                        {
                            if (pVal.FormTypeEx == "-139" || pVal.FormTypeEx == "-149" || pVal.FormTypeEx == "-140")
                            {
                                try
                                {
                                    string otFin = ((EditText)oForm.Items.Item(Constantes.DMA_NUMOT).Specific).Value;

                                    if (String.IsNullOrEmpty(otFin))
                                    {
                                        ((EditText)oForm.Items.Item(Constantes.DMA_NUMOT).Specific).Value = numeroOt;
                                        ((EditText)oForm.Items.Item(Constantes.DMA_NUMSINIESTRO).Specific).Value = numSiniestro;
                                        ((EditText)oForm.Items.Item(Constantes.DMA_COD_CIASEGURO).Specific).Value = codCiaSeguros;
                                        ((EditText)oForm.Items.Item(Constantes.DMA_DES_CIASEGURO).Specific).Value = desCiaSeguros;
                                        ((EditText)oForm.Items.Item(Constantes.DMA_LIQUIDADOR).Specific).Value = liquidador;
                                        ((ComboBox)oForm.Items.Item(Constantes.DMA_SUCURSAL).Specific).Select(sucursal);
                                        ((EditText)oForm.Items.Item(Constantes.DMA_CLIENTE).Specific).Value = cliente;
                                        LimpiaFormulario();
                                    }
                                }
                                catch (Exception) { LimpiaFormulario(); }

                            }
                        }
                        catch
                        {
                            Globals.SBO_Application.SetStatusBarMessage("Error al asignar numero de OT", BoMessageTime.bmt_Short);
                        }

                        try
                        {
                            if (pVal.FormTypeEx == "139" || pVal.FormTypeEx == "149" || pVal.FormTypeEx == "140")
                            {
                                try
                                {
                                    ((ComboBox)oForm.Items.Item(Constantes.DMA_SUCURSAL).Specific).Select(sucursal);
                                }
                                catch (Exception) { }
                            }
                        }
                        catch
                        {
                            Globals.SBO_Application.SetStatusBarMessage("Error al asignar clase de venta y sucursal", BoMessageTime.bmt_Short);
                        }
                    }
                    #endregion

                    #region Actualiza Precios
                    string estado = "";

                    if ((pVal.EventType == BoEventTypes.et_COMBO_SELECT) && pVal.Before_Action == false && pVal.Action_Success
                            && pVal.ItemChanged && (pVal.ItemUID == Constantes.TIPOVENTA))
                    {
                        estado = ((ComboBox)oForm.Items.Item(Constantes.ESTADO).Specific).Value;
                        if (!estado.Equals("1"))
                        {
                            Globals.SBO_Application.SetStatusBarMessage("La Orden de venta se encuentra Cerrada.", BoMessageTime.bmt_Short);
                            return;
                        }
                        _dmsBOneEc.ActualizaPrecios(oForm, pVal.FormTypeCount);//2
                    }

                    if (pVal.FormTypeEx == "139" || pVal.FormTypeEx == "149")
                    {
                        if ((pVal.EventType == BoEventTypes.et_LOST_FOCUS) && pVal.Before_Action == false &&
                            pVal.Action_Success
                            && pVal.ItemUID == "38" && pVal.ColUID == Constantes.ITEMCODE)
                        {
                            _dmsBOneEc.ActualizaPrecios(oForm, pVal.FormTypeCount);
                        }

                        if ((pVal.EventType == BoEventTypes.et_COMBO_SELECT) && pVal.Before_Action == false &&
                            pVal.Action_Success && pVal.ItemChanged && (pVal.ItemUID == "U_SYP_TPOVENTA"))
                        {
                            _dmsBOneEc.ActualizaPrecios(oForm, pVal.FormTypeCount);
                        }
                    }
                    #endregion Actualiza Precios

                    #region Asiento de Dispositivo
                    if (pVal.EventType == BoEventTypes.et_CLICK && pVal.Before_Action && pVal.ItemUID == Constantes.BTN_SOLICITUD)//1
                    {
                        estado = ((ComboBox)oForm.Items.Item(Constantes.ESTADO).Specific).Value;
                        if (!estado.Equals("1"))
                        {
                            Globals.SBO_Application.SetStatusBarMessage("La Orden de venta se encuentra Cerrada.", BoMessageTime.bmt_Short);
                            return;
                        }
                        string docNum = ObtieneValoresCampos(oForm, Constantes.DOCNUM, Globals.UdfType.EditText, pVal.FormTypeCount);
                        string serie = ObtieneValoresCampos(oForm, Constantes.SERIE, Globals.UdfType.ComboBox, pVal.FormTypeCount).Trim();

                        string tieneDis = ObtieneValoresCampos(oForm, Constantes.DISPTIENE, Globals.UdfType.ComboBox, pVal.FormTypeCount);
                        string pagoDisp = ObtieneValoresCampos(oForm, Constantes.DISPPAGO, Globals.UdfType.ComboBox, pVal.FormTypeCount);
                        double valorDisp = Convert.ToDouble(ObtieneValoresCampos(oForm, Constantes.DISPVALOR, Globals.UdfType.EditText, pVal.FormTypeCount),CultureInfo.InvariantCulture);
                        string valorEnt = ObtieneValoresCampos(oForm, Constantes.DISPENTRADA, Globals.UdfType.EditText, pVal.FormTypeCount);
                        string idUdo = ObtieneValoresCampos(oForm, Constantes.DISPIDUDO, Globals.UdfType.EditText, pVal.FormTypeCount);

                        string docEntry = General.ConsultarDocEntry(docNum, "ORDR", serie);

                        if (!String.IsNullOrEmpty(docEntry))
                        {
                            Documents oOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                            oOrder.GetByKey(Convert.ToInt32(docEntry));

                            double valorDispAnt =
                                Convert.ToDouble(oOrder.UserFields.Fields.Item(Constantes.DISPVALOR).Value,
                                    CultureInfo.InvariantCulture);

                            if (valorDispAnt != valorDisp)
                            {
                                Globals.SBO_Application.SetStatusBarMessage(
                                            "Primero actualice los cambios realizados en la Orden de Venta antes de crear el Asiento.",
                                            BoMessageTime.bmt_Short);
                                return;
                            }

                            if (tieneDis.Equals("SI"))
                            {
                                if (!String.IsNullOrEmpty(pagoDisp))
                                {
                                    if (valorDisp <= 0)
                                    {
                                        Globals.SBO_Application.SetStatusBarMessage(
                                            "El Valor del pago del dispositivo tiene que ser mayor a 0.",
                                            BoMessageTime.bmt_Short);
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                Globals.SBO_Application.SetStatusBarMessage(
                                            "Para crear el Asiento cambie a 'SI' el valor del campo 'Tiene Dispositivo'.",
                                            BoMessageTime.bmt_Short);
                                return;
                            }


                            if (String.IsNullOrEmpty(valorEnt))
                            {
                                Globals.SBO_Application.SetStatusBarMessage(
                                    "El Valor de la entrada del dispositivo tiene que ser mayor a 0.",
                                    BoMessageTime.bmt_Short);
                                return;
                            }
                            else
                            {
                                if (Convert.ToDouble(valorEnt,CultureInfo.InvariantCulture) <= 0)
                                {
                                    Globals.SBO_Application.SetStatusBarMessage("El Valor de la entrada del dispositivo tiene que ser mayor a 0.", BoMessageTime.bmt_Short);
                                    return;
                                }
                            }

                            

                            _dmsBOneEc.CreaAsientoDispositivo(docEntry);

                        }
                        else
                        {
                            Globals.SBO_Application.SetStatusBarMessage("Se debe grabar la Orden de venta", BoMessageTime.bmt_Short);
                        }

                    }
                    #endregion Asiento de Dispositivo

                    #region  Asigna datos del Lote en Entrega
                    if (pVal.ItemUID == Constantes.BTN_SERIES && pVal.EventType == BoEventTypes.et_CLICK && pVal.BeforeAction == false && pVal.ActionSuccess)
                    {
                        estado = ((ComboBox)oForm.Items.Item(Constantes.ESTADO).Specific).Value;
                        if (!estado.Equals("1"))
                        {
                            Globals.SBO_Application.SetStatusBarMessage("La Orden de venta se encuentra Cerrada.", BoMessageTime.bmt_Short);
                            return;
                        }
                        _dmsBOneEc.AsignarDatosDelLoteEntrega(oForm, pVal);
                    }
                    #endregion  Asigna datos del Lote en Entrega
                }
                #endregion
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage("ItemEvent : " + ex.Message);
            }
        }
        #endregion SBO_Application_ItemEvent

        #region SBO_Application_ProgressBarEvent
        private void SBO_Application_ProgressBarEvent(ref ProgressBarEvent pVal, out bool bubbleEvent)
        {
            BoProgressBarEventTypes eventEnum = 0;
            eventEnum = pVal.EventType;
            bubbleEvent = true;
        }
        #endregion SBO_Application_ProgressBarEvent

        #region SBO_Application_FormDataEvent
        public void SBO_Application_FormDataEvent(ref BusinessObjectInfo businessObjectInfo, out bool bubbleEvent)
        {
            bubbleEvent = true;
            try
            {
                Form oForm = Globals.SBO_Application.Forms.Item(businessObjectInfo.FormUID);

                #region Udo Citas
                if (businessObjectInfo.FormTypeEx == "UDO_FT_CIJM" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD ||
                     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                {
                    try
                    {
                        _dmsBOneEc.CreaOtPorAgendamiendoCitas(businessObjectInfo);
                        _dmsBOneEc.CreaActividadPorAgendamiendoCitas(businessObjectInfo);
                    }
                    catch (Exception) { }
                }
                #endregion Udo Citas

                #region Udo Tecnicos
                if (businessObjectInfo.FormTypeEx == "UDO_FT_SLRT" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD ||
                     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                {
                    try
                    {
                        _dmsBOneEc.AsignaTareasMarcaje(businessObjectInfo);
                    }
                    catch (Exception) { }
                }
                #endregion Udo Tecnicos

                #region Llamada de Servicio
                if (businessObjectInfo.FormTypeEx == "60110" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD)
                {
                    try
                    {
                        string keyXml = businessObjectInfo.ObjectKey;
                        string docEntry = General.ObtenerTagXml(keyXml, "ServiceCallID");

                        ServiceCalls oLlamada = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                        oLlamada.GetByKey(Convert.ToInt32(docEntry));

                        numeroOt = oLlamada.ServiceCallID.ToString();
                        sucursal = oLlamada.ProblemSubType.ToString();
                        //claseVenta = oLlamada.CallType.ToString();
                        cliente = oLlamada.CustomerCode;
                        numSiniestro = oLlamada.UserFields.Fields.Item(Constantes.LLS_NUMSINIESTRO).Value.ToString();
                        codCiaSeguros = oLlamada.UserFields.Fields.Item(Constantes.LLS_COD_CIASEGURO).Value.ToString();
                        desCiaSeguros = oLlamada.UserFields.Fields.Item(Constantes.LLS_DES_CIASEGURO).Value.ToString();
                        liquidador = oLlamada.UserFields.Fields.Item(Constantes.LLS_LIQUIDADOR).Value.ToString();
                    }
                    catch { }

                }

                if (businessObjectInfo.FormTypeEx == "60110" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD)
                {
                    try
                    {
                        _dmsBOneEc.CreaUdoEstadoVehiculo(businessObjectInfo);
                        _dmsBOneEc.CreaUdoInventarioVehiculos(businessObjectInfo);
                        _dmsBOneEc.CreaUdoTecnico(businessObjectInfo);

                    }
                    catch { }

                }

                if (businessObjectInfo.FormTypeEx == "60110" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE)
                {
                    try
                    {
                        _dmsBOneEc.CreaUdoEstadoVehiculo(businessObjectInfo);
                        _dmsBOneEc.CreaUdoInventarioVehiculos(businessObjectInfo);
                        _dmsBOneEc.CreaUdoTecnico(businessObjectInfo);
                        _dmsBOneEc.UpdateUdoTecnico(businessObjectInfo);

                    }
                    catch { }

                }
                #endregion Llamada de Servicio

                #region Orden de Venta
                if (businessObjectInfo.FormTypeEx == "139" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD ||
                     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                {
                    try
                    {
                        _dmsBOneEc.AsociarDocumentoLlamadaServicio(businessObjectInfo, "139");
                        _dmsBOneEc.CreaUdoFormaPago(businessObjectInfo);
                        _dmsBOneEc.CrearUdoDesgloseBonos(businessObjectInfo);
                    }
                    catch (Exception) { }
                }
                #endregion

                #region Udo Forma de Pago
                if (businessObjectInfo.FormTypeEx == "UDO_FT_OFORPAGO" & businessObjectInfo.ActionSuccess &
                    businessObjectInfo.BeforeAction == false &
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE
                     || businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD))
                {
                    _dmsBOneEc.ActualizaOvTotalFormaPago(businessObjectInfo);
                }
                #endregion

                #region Factura de Ventas - LR
                if (businessObjectInfo.FormTypeEx == "133" & businessObjectInfo.ActionSuccess &
                    businessObjectInfo.BeforeAction == false &
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD || businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                {
                    _dmsBOneEc.ExternoFacturaInterna(businessObjectInfo);
                    _dmsBOneEc.CreaAsientoContablizacionVehiculosUsados(businessObjectInfo);
                }
                #endregion

                #region Nota de Crédito de Ventas - LR
                if (businessObjectInfo.FormTypeEx == "179" & businessObjectInfo.ActionSuccess &
                    businessObjectInfo.BeforeAction == false &
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD))
                {
                    _dmsBOneEc.ExternoNcInterna(businessObjectInfo);
                }
                #endregion

                #region Pagos recibidos - Actualización de estado de contabilzación de formas de pago
                //if (businessObjectInfo.FormTypeEx == "170" && businessObjectInfo.ActionSuccess &&
                //    businessObjectInfo.BeforeAction == false &&
                //    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD ||
                //     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                //{
                //    _dmsBOneEc.ActualizarEstadoFormaDePago(businessObjectInfo);
                //}
                #endregion

                #region Trasferencia de Stock 
                if (businessObjectInfo.FormTypeEx == "940" & businessObjectInfo.ActionSuccess & businessObjectInfo.BeforeAction == false
                    & (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE || businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD)
                    )
                {
                    _dmsBOneEc.AsociarDocumentoLlamadaServicio(businessObjectInfo, "940");
                    _dmsBOneEc.CreateStockTransferRequest(businessObjectInfo);
                }
                #endregion

                #region Creacion de Asiento y Anulacion de Asiento por Bono Factura de Reserva
                if ((businessObjectInfo.FormTypeEx == "60091" || businessObjectInfo.FormTypeEx == "65302" || businessObjectInfo.FormTypeEx == "133")
                       & businessObjectInfo.ActionSuccess &
                    businessObjectInfo.BeforeAction == false &
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE ||
                     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD))
                {
                    if (oForm.Title.IndexOf("Cancel") != -1)
                    {
                        if (General.ObtieneParametro("DMS_ASI_BONO", "U_SYP_ATRIB1").Equals("Y"))
                        {
                            _dmsBOneEc.AnulaAsientoBonosFactura(businessObjectInfo, "U_SYP_ASBO");
                        }
                        if (General.ObtieneParametro("DMS_ASI_APRO", "U_SYP_ATRIB1").Equals("Y"))
                        {
                            _dmsBOneEc.AnulaAsientoBonosFactura(businessObjectInfo, "U_SYP_ASAP");
                        }
                    }
                    else
                    {
                        if (General.ObtieneParametro("DMS_ASI_BONO", "U_SYP_ATRIB1").Equals("Y"))
                        {
                            _dmsBOneEc.CreaAsientoBonos(businessObjectInfo);
                        }
                        if (General.ObtieneParametro("DMS_ASI_APRO", "U_SYP_ATRIB1").Equals("Y"))
                        {
                            _dmsBOneEc.CreaAsientoAprovisionamiento(businessObjectInfo);
                        }
                    }
                    

                    
                }

                if (businessObjectInfo.FormTypeEx == "179" & businessObjectInfo.ActionSuccess &
                    businessObjectInfo.BeforeAction == false &
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE ||
                     businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD))
                {
                    if (General.ObtieneParametro("DMS_ASI_BONO", "U_SYP_ATRIB1").Equals("Y"))
                    {
                        _dmsBOneEc.AnulaAsientoBonos(businessObjectInfo, "U_SYP_ASBO");
                    }
                    if (General.ObtieneParametro("DMS_ASI_APRO", "U_SYP_ATRIB1").Equals("Y"))
                    {
                        _dmsBOneEc.AnulaAsientoBonos(businessObjectInfo, "U_SYP_ASAP");
                    }
                }
                #endregion

                #region Orden de Venta
                if (businessObjectInfo.FormTypeEx == "139" && businessObjectInfo.ActionSuccess &&
                    businessObjectInfo.BeforeAction == false &&
                    (businessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                {
                    try
                    {
                        _dmsBOneEc.ActualizarDatosEntradaVehiculos(businessObjectInfo);
                    }
                    catch (Exception) { }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.MessageBox("FormDataEvent : " + ex.Message);
            }


        }
        #endregion SBO_Application_FormDataEvent

        #region LimpiaFormulario
        public static void LimpiaFormulario()
        {
            numeroOt = "";
            //claseVenta = "";
            sucursal = "";
            numSiniestro = "";
            codCiaSeguros = "";
            desCiaSeguros = "";
            liquidador = "";
            cliente = "";
        }
        #endregion

        #region ObtieneValoresCampos
        public static string ObtieneValoresCampos(Form oForm, string item, Globals.UdfType tipoItem, int formTypeCode)
        {
            string resp = "";
            try
            {
                try
                {
                    resp = Globals.getUdfValue(item, tipoItem, -1 * oForm.Type, formTypeCode);
                }
                catch
                {
                    resp = Globals.getUdfValue(item, tipoItem, oForm.Type, formTypeCode);
                }

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage("ObtieneValoresCampos : " + ex.Message);
                return resp;
            }
        }
        #endregion ObtieneValoresCampos

        #region SetFilters
        public static void SetFilters()
        {
            Globals.oFilters = new EventFilters();

            #region FORM_DATA_ADD
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_DATA_ADD);
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("UDO_FT_OVPPAGO");  // UDO VH Parte de Pago
            Globals.oFilter.AddEx("UDO_FT_OFORPAGO"); //UDO Forma de Pago
            Globals.oFilter.AddEx("133");
            Globals.oFilter.AddEx("60091");
            Globals.oFilter.AddEx("60110");
            Globals.oFilter.AddEx("UDO_FT_CIJM");
            Globals.oFilter.AddEx("179");
            #endregion
            
            #region FORM_DATA_UPDATE
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_DATA_UPDATE);
            Globals.oFilter.AddEx("UDO_FT_OVPPAGO");  // UDO VH Parte de Pago
            Globals.oFilter.AddEx("UDO_FT_OFORPAGO"); //UDO Forma de Pago
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("60110");
            Globals.oFilter.AddEx("60091");
            Globals.oFilter.AddEx("133");
            Globals.oFilter.AddEx("UDO_FT_SLRH");
            Globals.oFilter.AddEx("179");
            Globals.oFilter.AddEx("UDO_FT_CIJM");
            Globals.oFilter.AddEx("142"); //Orden de Compra
            #endregion
            
            #region et_MATRIX_LOAD
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_MATRIX_LOAD);
            Globals.oFilter.AddEx("UDO_FT_OFORPAGO");
            #endregion
            
            #region LOST_FOCUS
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_LOST_FOCUS);
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("149"); //Oferta de venta
            Globals.oFilter.AddEx("992");
            Globals.oFilter.AddEx("frmStatusNeg");
            #endregion
            
            #region FORM_LOAD
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_LOAD);
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("992");
            Globals.oFilter.AddEx("60110");
            Globals.oFilter.AddEx("-60110");
            Globals.oFilter.AddEx("frmStatusNeg");
            Globals.oFilter.AddEx("143"); // Entrada de mercancias
            Globals.oFilter.AddEx("60117"); //Llamada de Servicio - Lista de Documentos
            Globals.oFilter.AddEx("149"); //Cotizacion
            Globals.oFilter.AddEx("-139"); //Nota de Venta - Campos de Usuario
            Globals.oFilter.AddEx("-149"); //Cotizacion - Campos de Usuarios
            Globals.oFilter.AddEx("DmsB1AddOn.Formularios.MarcajeMaestros");
            #endregion

            #region KEY_DOWN
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_KEY_DOWN);
            //Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("frmStatusNeg");
            #endregion

            #region et_COMBO_SELECT - LR
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_COMBO_SELECT);
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("149"); //Orden de venta
            Globals.oFilter.AddEx("65051"); //Orden de venta
            #endregion

            #region FORM_CLOSE
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_CLOSE);
            Globals.oFilter.AddEx("60117"); //Llamada de Servicio - Lista de Documentos
            #endregion

            #region FORM_DATA_LOAD
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_DATA_LOAD);
            Globals.oFilter.AddEx("60110"); //Llamada de Servicio - Lista de Documentos
            Globals.oFilter.AddEx("-60110"); //Llamada de Servicio - Lista de Documentos
            #endregion

            #region et_CLICK
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_CLICK);
            Globals.oFilter.AddEx("139");
            Globals.oFilter.AddEx("-9876");
            Globals.oFilter.AddEx("frmStatusNeg");
            Globals.oFilter.AddEx("143"); // Entrada de mercancias
            Globals.oFilter.AddEx("DmsB1AddOn.Formularios.MarcajeMaestros");
            #endregion

            #region et_FORMAT_SEARCH_COMPLETED
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORMAT_SEARCH_COMPLETED);
            Globals.oFilter.AddEx("UDO_FT_SLRH");
            #endregion
            
            #region et_FORM_RESIZE
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_RESIZE);
            Globals.oFilter.AddEx("133");
            Globals.oFilter.AddEx("60110");
            #endregion
            
            #region et_FORM_DEACTIVATE
            //Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_DEACTIVATE);
            #endregion
            
            #region et_GOT_FOCUS
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_GOT_FOCUS);
            Globals.oFilter.AddEx("60110");
            Globals.oFilter.AddEx("frmStatusNeg");
            #endregion
            
            #region et_FORM_KEY_DOWN
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_FORM_KEY_DOWN);
            Globals.oFilter.AddEx("139"); //Orden de venta
            Globals.oFilter.AddEx("frmStatusNeg");
            #endregion
            
            #region et_ITEM_PRESSED
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_ITEM_PRESSED);
            Globals.oFilter.AddEx("992");
            Globals.oFilter.AddEx("UDO_FT_SLRH");
            Globals.oFilter.AddEx("60110");
            Globals.oFilter.AddEx("frmStatusNeg");
            #endregion

            #region CHOOSE_FROM_LIST
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_CHOOSE_FROM_LIST);
            #endregion

            #region et_MENU_CLICK
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_MENU_CLICK);
            #endregion et_MENU_CLICK

            #region et_ALL_EVENTS
            Globals.oFilter = Globals.oFilters.Add(BoEventTypes.et_ALL_EVENTS);
            Globals.oFilter.AddEx("F01");

            #endregion

            Globals.SBO_Application.SetFilter(Globals.oFilters);
        }
        #endregion SetFilters
    }
}
