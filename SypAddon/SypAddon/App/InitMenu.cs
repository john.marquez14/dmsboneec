﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;
using SypAddonController.Util;
using SypDevs;

namespace SypAddon
{
    public class InitMenu
    {
        public static void AddMenuItems()
        {
            MenuCreationParams oCreationPackage = (MenuCreationParams)Globals.SBO_Application.CreateObject(BoCreatableObjectType.cot_MenuCreationParams);

            try
            {
                #region CabeceraFormularios
                //MenuItem oMenu = Globals.SBO_Application.Menus.Item("3587");
                //oMenu.String = "Orden de Trabajo";

                //oMenu = Globals.SBO_Application.Menus.Item("3591");
                //oMenu.String = "Tarjeta de Vehículo";
                #endregion CabeceraFormularios
                
                Menus oMenus = null;
                MenuItem oMenuItem = null;

                //oMenus = Globals.SBO_Application.Menus;

                //oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Globals.SBO_Application.CreateObject(BoCreatableObjectType.cot_MenuCreationParams)));
                //oMenuItem = Globals.SBO_Application.Menus.Item("3584"); // moudles'43520

                //oCreationPackage.Type = BoMenuType.mt_POPUP;
                //oCreationPackage.UniqueID = "MTEC";
                //oCreationPackage.String = "Marcaje de Servicios";
                //oCreationPackage.Enabled = true;
                //oCreationPackage.Position = -1;

                //oMenus = oMenuItem.SubMenus;

                //try
                //{
                //    //  If the manu already exists this code will fail
                //    oMenus.AddEx(oCreationPackage);
                //}
                //catch (Exception e)
                //{

                //}

                try
                {
                    // Get the menu collection of the newly added pop-up item
                    oMenuItem = Globals.SBO_Application.Menus.Item("3584");
                    oMenus = oMenuItem.SubMenus;

                    // Create s sub menu
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                    oCreationPackage.UniqueID = "MTEC";
                    oCreationPackage.String = "Marcaje de Servicios";
                    oMenus.AddEx(oCreationPackage);
                }catch (Exception){}
            }
            catch (Exception ex)
            {
                Utils.MostrarMensaje(ex.Message, Utils.TiposMensajes.SmtError);
            }
        }



    }
}
