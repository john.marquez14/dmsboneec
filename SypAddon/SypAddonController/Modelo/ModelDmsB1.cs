﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SypAddonController.Modelo
{
    public class ModelDmsB1
    {
        public class MotivosMarcaje
        {
            public string id { get; set; }
            public string motivo { get; set; }
        }

        public class CitasPendientes
        {
            public int lineId { get; set; }
            public string placa { get; set; }
            
        }

        public class TareasPendiente
        {
            public int lineId { get; set; }
            public string tecnico { get; set; }
            public string estado { get; set; }
            public string ot { get; set; }
            public string servicio { get; set; }
            public string nameTecnico { get; set; }
        }


        public class DatosLlamadaServicio
        {
            public string cardCode { get; set; }
            public string chasis { get; set; }
            public string itemcode { get; set; }
            public string sucursal { get; set; }
            public string tipoOt { get; set; }
            public string comentario { get; set; }
            public int activityCode { get; set; }
            public string hora { get; set; }
            public string fecha { get; set; }
            public string servicio { get; set; }
        }

        public class FormaPago
        {
            public int lineId { get; set; }
            public double importePago { get; set; }
            public string formapago { get; set; }
            public DateTime fechaEmision { get; set; }
            public DateTime fechaVencimiento { get; set; }
            public string codigoBanco { get; set; }
            public string nombreBanco { get; set; }
            public string documento { get; set; }
            public string cuentaContable { get; set; }
            public string medioPago { get; set; }
            public string codigoFp { get; set; }
        }
    }
}
