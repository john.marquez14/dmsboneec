﻿using SypDevs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;

namespace SypAddonController.Servicio
{
    public class General
    {
        #region ObtenerTagXml
        public static string ObtenerTagXml(string xml, string tag)
        {

            string iniTag = "<" + tag + ">";
            string finTag = "</" + tag + ">";
            if (xml.Contains(iniTag))
            {
                int idxof = xml.IndexOf(iniTag);
                int idxofFin = xml.IndexOf(finTag);
                int posIni = idxof + iniTag.Length;
                int posFin = idxofFin - posIni;
                return xml.Substring(posIni, posFin);
            }
            string iniTagR = "&lt;" + tag + "&gt;";
            string finTagR = "&lt;/" + tag + "&gt";
            if (xml.Contains(iniTagR))
            {
                int idxof = xml.IndexOf(iniTagR);
                int idxofFin = xml.IndexOf(finTagR);
                return xml.Substring(idxof + iniTagR.Length, idxofFin);
            }
            return "";
        }
        #endregion

        #region ObtieneParametro
        public static string ObtieneParametro (string codigo, string atributo)
        {
            var query = Globals.isHana()
                 ? "SELECT \"" + atributo + "\" FROM \"@SYP_PARAM_GENERAL\" WHERE \"Code\" = '" + codigo + "'"
                 : "SELECT \"" + atributo + "\" FROM \"@SYP_PARAM_GENERAL\" WHERE \"Code\" = '" + codigo + "'";

            Globals.Query = query;
            Globals.runQuery(Globals.Query);
            Globals.ORec.MoveFirst();
            string resul = Globals.ORec.Fields.Item(0).Value.ToString();
            Globals.release(Globals.ORec);

            return resul;
        }
        #endregion

        #region ConsultarDocEntry
        public static string ConsultarDocEntry(string docNum, string tablaDocumento, string serie)
        {
            string docentry = "";
            try
            {

                string query = "SELECT A.\"DocEntry\" FROM \"" + tablaDocumento + "\" A WHERE A.\"DocNum\"  = '" + docNum + "' AND A.\"Series\"  = '" + serie + "'";
                Globals.Query = query;
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();
                while (!Globals.ORec.EoF)
                {
                    docentry = Globals.ORec.Fields.Item(0).Value.ToString();
                    Globals.ORec.MoveNext();
                }
                Globals.release(Globals.ORec);
                return docentry;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, false);
            }
            return docentry;
        }
        #endregion 
    }
}
