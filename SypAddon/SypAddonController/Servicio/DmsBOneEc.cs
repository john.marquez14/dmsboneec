﻿using SAPbobsCOM;
using SAPbouiCOM;
using SypAddonController.Util;
using SypDevs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SypAddonController.Modelo.ModelDmsB1;

namespace SypAddonController.Servicio
{
    public class DmsBOneEc
    {
        #region Declaraciones Generales
        public static GeneralService oGeneralService;
        public static GeneralData oGeneralData;
        public static GeneralData oChild;
        public static GeneralDataParams oGeneralParams;
        public static GeneralDataCollection oChildren;
        #endregion

        #region ConsultaMotivosMarcaje
        public List<MotivosMarcaje> ConsultaMotivosMarcaje()
        {
            List<MotivosMarcaje> motivos = new List<MotivosMarcaje>();
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('',0,'','','','','','MOTIVOS')";
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();

                while (!Globals.ORec.EoF)
                {
                    motivos.Add(new MotivosMarcaje()
                    {
                        id = Globals.ORec.Fields.Item("Id").Value.ToString(),
                        motivo = Globals.ORec.Fields.Item("Motivo").Value.ToString(),
                    });

                    Globals.ORec.MoveNext();
                }

                Globals.release(Globals.ORec);

                return motivos;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ConsultaMotivosMarcaje)  : " + ex.Message);
                return motivos;
            }
        }
        #endregion

        #region AgregarBotonMotivos
        public void AgregarBotonMotivos(Form oForm, string idComponente, int left, int top, int width, int heigt, string caption)
        {
            try
            {
                try
                {
                    oForm.Items.Item(idComponente);
                }
                catch
                {
                    oForm.Items.Add(idComponente, BoFormItemTypes.it_BUTTON);
                }
                var item = (Button)oForm.Items.Item(idComponente).Specific;
                item.Item.Width = width;
                item.Item.Height = heigt;
                item.Item.Left = left;
                item.Item.Top = top;
                item.Item.TextStyle = 1;
                item.Item.FontSize = 16;
                item.Caption = caption;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(AgregarBotonMotivos)  : " + ex.Message);
            }
        }
        #endregion

        #region CargaTareasMarcaje 
        public void CargaTareasMarcaje(Form oForm, string ot)
        {
            try
            {
                Matrix oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX).Specific;
                SAPbouiCOM.DataTable oDataTable = oForm.DataSources.DataTables.Item(Constantes.PLT_DATA_TABLE);
                oDataTable.Clear();
                string userSap = Globals.oCompany.UserName.ToUpper();


                EjecutaSpConsultaTareas(userSap, ot, oDataTable);

                if (oDataTable.Rows.Count >= 1)
                {
                    for (int i = 0; i < oDataTable.Columns.Count; i++)
                    {
                        string columName = oDataTable.Columns.Item(i).Name;
                        oMatrix.Columns.Item(columName).DataBind.Bind(Constantes.PLT_DATA_TABLE, columName);
                    }
                }

                oMatrix.Clear();
                oMatrix.LoadFromDataSource();
                oMatrix.AutoResizeColumns();


                oMatrix = (Matrix)oForm.Items.Item(Constantes.PLT_MATRIX_DET).Specific;
                oDataTable = oForm.DataSources.DataTables.Item(Constantes.PLT_DATA_TABLE_DET);
                oDataTable.Clear();

                EjecutaSpConsultaTareasDetalle(userSap, ot, oDataTable);

                if (oDataTable.Rows.Count >= 1)
                {
                    for (int i = 0; i < oDataTable.Columns.Count; i++)
                    {
                        string columName = oDataTable.Columns.Item(i).Name;
                        oMatrix.Columns.Item(columName).DataBind.Bind(Constantes.PLT_DATA_TABLE_DET, columName);
                    }
                }

                oMatrix.Clear();
                oMatrix.LoadFromDataSource();
                oMatrix.AutoResizeColumns();

                Utils.MostrarMensaje("Consulta realizada de manera exitosa!", Utils.TiposMensajes.SmtSuccess);

            }
            catch (Exception ex)
            {
                Utils.MostrarMensaje("(CargaTareasMarcaje) " + ex.Message, Utils.TiposMensajes.SmtError);
            }
        }
        #endregion CargaTareasMarcaje

        #region EjecutaSpConsultaTareas
        public void EjecutaSpConsultaTareas(string userSap, string ot, SAPbouiCOM.DataTable oDataTable)
        {
            try
            {
                string query = "";
                if (Globals.isHana())
                    query = "CALL SYP_SP_CITAS_DMS('',0,'','" + ot + "','" + userSap + "','','','MARCAJECON')";
                else
                    query = "";

                oDataTable.ExecuteQuery(query);
            }
            catch (Exception ex)
            {
                Utils.MostrarMensaje("(EjecutaSpConsultaTareas) " + ex.Message, Utils.TiposMensajes.SmtError);
            }
        }
        #endregion EjecutaSpConsultaTareas

        #region EjecutaSpConsultaTareasDetalle
        public void EjecutaSpConsultaTareasDetalle(string userSap, string ot, SAPbouiCOM.DataTable oDataTable)
        {
            try
            {
                string query = "";
                if (Globals.isHana())
                    query = "CALL SYP_SP_CITAS_DMS('',0,'','" + ot + "','" + userSap + "','','','MARCAJECONDET')";
                else
                    query = "";

                oDataTable.ExecuteQuery(query);
            }
            catch (Exception ex)
            {
                Utils.MostrarMensaje("(EjecutaSpConsultaTareasDetalle) " + ex.Message, Utils.TiposMensajes.SmtError);
            }
        }
        #endregion EjecutaSpConsultaTareas

        #region ActualizaMotivos
        public void ActualizaMotivos(int lineId, string codUdo, string ot, string motivo)
        {
            try
            {
                var query = "CALL SYP_SP_MARCAJE_DMS('" + codUdo + "', '" + ot + "', " + lineId + ", 0,'','','','" + motivo + "','','UPDMOTIVO')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ActualizaMotivos) " + ex.Message);
            }
        }
        #endregion ActualizaMotivos

        #region ActualizaMotivosBorrar
        public void ActualizaMotivosBorrar(int lineId, string codUdo, string ot)
        {
            try
            {
                var query = "CALL SYP_SP_MARCAJE_DMS('" + codUdo + "', '" + ot + "', " + lineId + ", 0,'','','','','','DELMOTIVO')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ActualizaMotivos) " + ex.Message);
            }
        }
        #endregion ActualizaMotivosBorrar

        #region ActualizaInicio
        public void ActualizaInicio(int lineId, string codUdo, string ot, string tipo, string motivo)
        {
            try
            {
                string userSap = Globals.oCompany.UserName.ToUpper();
                int hora = DateTime.Now.Hour;
                int minuto = DateTime.Now.Minute;
                char min = '0';

                string fecha = DateTime.Now.ToString("yyyyMMdd");

                string horaAlt = hora + ":" + minuto.ToString().PadLeft(2, min);

                var query = "CALL SYP_SP_MARCAJE_DMS('" + codUdo + "', '" + ot + "', " + lineId + ", " + hora +""+ minuto.ToString().PadLeft(2, min) + ",'" + fecha.ToString() + "','"+ horaAlt + "','"+ userSap + "','" + motivo + "','" + tipo + "','UPDINICIO')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ActualizInicio) " + ex.Message);
            }
        }
        #endregion ActualizaInicio

        #region ObtieneMotivo
        public string ObtieneMotivo(string motivo)
        {
            string resp = "";
            try
            {
                Globals.Query = "CALL SYP_SP_MARCAJE_DMS('', '', 0, 0,'','','','" + motivo + "','','MOTIVO')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = Globals.ORec.Fields.Item("Name").Value.ToString();
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ObtieneMotivo)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ObtieneMotivo

        #region ObtieneUserName
        public string ObtieneUserName(string user)
        {
            string resp = "";
            try
            {
                Globals.Query = "CALL SYP_SP_MARCAJE_DMS('', '', 0, 0,'','','','','" + user + "','USERNAME')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = Globals.ORec.Fields.Item("Name").Value.ToString();
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ObtieneUserName)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ObtieneUserName

        #region CreaOtPorAgendamiendoCitas
        public void CreaOtPorAgendamiendoCitas(BusinessObjectInfo businessObjectInfo)
        {
            int iRet = -1;
            string errMsg = null;

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docNum = General.ObtenerTagXml(keyXml, "Code");

                foreach (var cab in ConsultaCitasPendientes(docNum))
                {
                    if (ValidaPlaca(cab.placa))
                    {
                        foreach (var otSer in ConsultaDatosLlamadaServicio(cab.placa, docNum, "OT"))
                        {
                            ServiceCalls ot = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                            ot.ServiceBPType = ServiceTypeEnum.srvcSales;
                            ot.CustomerCode = otSer.cardCode;
                            ot.InternalSerialNum = otSer.chasis;
                            ot.ItemCode = otSer.itemcode;
                            ot.Subject = docNum;
                            ot.CallType = Convert.ToInt32(otSer.tipoOt);
                            ot.Description = otSer.comentario;
                            ot.ProblemSubType = Convert.ToInt32(otSer.sucursal);

                            iRet = ot.Add();
                            if (iRet != 0)
                            {
                                Globals.oCompany.GetLastError(out iRet, out errMsg);
                                throw new Exception("Error en crear Orden de Trabajo./ " + errMsg);
                            }
                            else
                            {
                                string callId;
                                Globals.oCompany.GetNewObjectCode(out callId);

                                ActualizaEstadoCitas(docNum, cab.lineId, callId);
                                Globals.SBO_Application.StatusBar.SetText("OT para placa " + cab.placa + " creada con exito. #OT: " + callId, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            }
                        }
                    }
                    else
                    {
                        Globals.SBO_Application.StatusBar.SetText("No existe la tarjeta del vehículo de la placa " + cab.placa + " Favor registre la tarjeta del Vehículo y vuelva a procesar.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("Ec: Error en crear Orden de Trabajo " + ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion CreaOtPorAgendamiendoCitas

        #region ConsultaCitasPendientes
        public List<CitasPendientes> ConsultaCitasPendientes(string docNum)
        {
            List<CitasPendientes> datosCitas = new List<CitasPendientes>();
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('" + docNum + "',0,'','','','','','CITAS')";
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();

                while (!Globals.ORec.EoF)
                {
                    datosCitas.Add(new CitasPendientes()
                    {
                        lineId = Convert.ToInt32(Globals.ORec.Fields.Item("LineId").Value),
                        placa = Globals.ORec.Fields.Item("Placa").Value.ToString(),
                    });

                    Globals.ORec.MoveNext();
                }

                Globals.release(Globals.ORec);

                return datosCitas;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ConsultaCitasPendientes)  : " + ex.Message);
                return datosCitas;
            }
        }
        #endregion

        #region ConsultaActividadesPendientes
        public List<CitasPendientes> ConsultaActividadesPendientes(string docNum)
        {
            List<CitasPendientes> datosCitas = new List<CitasPendientes>();
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('" + docNum + "',0,'','','','','','ACTIVIDADES')";
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();

                while (!Globals.ORec.EoF)
                {
                    datosCitas.Add(new CitasPendientes()
                    {
                        lineId = Convert.ToInt32(Globals.ORec.Fields.Item("LineId").Value),
                        placa = Globals.ORec.Fields.Item("Placa").Value.ToString(),
                        
                    });

                    Globals.ORec.MoveNext();
                }

                Globals.release(Globals.ORec);

                return datosCitas;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ConsultaActividadesPendientes)  : " + ex.Message);
                return datosCitas;
            }
        }
        #endregion

        #region ConsultaDatosLlamadaServicio
        public List<DatosLlamadaServicio> ConsultaDatosLlamadaServicio(string placa, string docNum, string tipo)
        {
            List<DatosLlamadaServicio> datosOt = new List<DatosLlamadaServicio>();
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('" + docNum + "',0,'" + placa + "','','','','','" + tipo + "')";
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();

                while (!Globals.ORec.EoF)
                {
                    datosOt.Add(new DatosLlamadaServicio()
                    {
                        cardCode = Globals.ORec.Fields.Item("CardCode").Value.ToString(),
                        chasis = Globals.ORec.Fields.Item("Chasis").Value.ToString(),
                        itemcode = Globals.ORec.Fields.Item("ItemCode").Value.ToString(),
                        sucursal = Globals.ORec.Fields.Item("Sucursal").Value.ToString(),
                        tipoOt = Globals.ORec.Fields.Item("TipoOt").Value.ToString(),
                        comentario = Globals.ORec.Fields.Item("Comentario").Value.ToString(),
                        activityCode = Convert.ToInt32(Globals.ORec.Fields.Item("CodeActividad").Value),
                        hora = Globals.ORec.Fields.Item("Hora").Value.ToString(),
                        fecha = Globals.ORec.Fields.Item("Fecha").Value.ToString(),
                        servicio = Globals.ORec.Fields.Item("Servicio").Value.ToString()
                    });

                    Globals.ORec.MoveNext();
                }

                Globals.release(Globals.ORec);

                return datosOt;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ConsultaDatosOT)  : " + ex.Message);
                return datosOt;
            }
        }
        #endregion

        #region ValidaPlaca
        public bool ValidaPlaca(string placa)
        {
            bool resp = false;
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('',0,'" + placa + "','','','','','PLACA')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = true;
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ValidaPlaca)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ValidaPlaca

        #region ValidaActividad
        public bool ValidaActividad(int code)
        {
            bool resp = false;
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('',0,'" + code + "','','','','','VALACTIVIDAD')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = true;
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ValidaActividad)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ValidaPlaca

        #region ActualizaEstadoCitas
        public void ActualizaEstadoCitas(string docNum, int lineId, string ot)
        {
            try
            {
                var query = "CALL SYP_SP_CITAS_DMS('" + docNum + "', " + lineId + ",'','" + ot + "','','','','ACTUALIZA')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ActualizaEstadoCitas) " + ex.Message);
            }
        }
        #endregion ActualizaEstadoCitas

        #region ActualizaEstadoActividades
        public void ActualizaEstadoActividades(string docNum, int lineId, string code)
        {
            try
            {
                var query = "CALL SYP_SP_CITAS_DMS('" + docNum + "', " + lineId + ",'','" + code + "','','','','ACTUALIZAACT')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ActualizaEstadoActividades) " + ex.Message);
            }
        }
        #endregion ActualizaEstadoActividades

        #region Udo tecnicos
        public void AsignaTareasMarcaje(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docNum = General.ObtenerTagXml(keyXml, "Code");

                foreach (var cab in ConsultaTareasPendientes(docNum))
                {
                    if (cab.estado == "ASIGNAR")
                    {
                        if (ValidaTecnicoAsignado(cab.ot, cab.servicio, cab.tecnico))
                        {
                            AsignarTrabajos(docNum, cab.lineId);

                        }
                    }

                    if (cab.estado == "REASIGNAR")
                    {
                        ReAsignarTrabajos(docNum, cab.ot, cab.tecnico, cab.nameTecnico, cab.servicio);
                    }

                    if (cab.estado == "QUITAR")
                    {
                        QuitarTrabajos(docNum, cab.ot, cab.servicio);
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("Ec: Error en Asignar las tareas " + ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion Udo tecnicos

        #region ConsultaTareasPendientes
        public List<TareasPendiente> ConsultaTareasPendientes(string docNum)
        {
            List<TareasPendiente> datosTareas = new List<TareasPendiente>();
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('" + docNum + "',0,'','','','','','TECNICOS')";
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();

                while (!Globals.ORec.EoF)
                {
                    datosTareas.Add(new TareasPendiente()
                    {
                        lineId = Convert.ToInt32(Globals.ORec.Fields.Item("LineId").Value),
                        tecnico = Globals.ORec.Fields.Item("Tecnico").Value.ToString(),
                        estado = Globals.ORec.Fields.Item("Estado").Value.ToString(),
                        ot = Globals.ORec.Fields.Item("Ot").Value.ToString(),
                        servicio = Globals.ORec.Fields.Item("Servicio").Value.ToString(),
                        nameTecnico = Globals.ORec.Fields.Item("NameTecnico").Value.ToString()
                    });

                    Globals.ORec.MoveNext();
                }

                Globals.release(Globals.ORec);

                return datosTareas;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ConsultaTareasPendientes)  : " + ex.Message);
                return datosTareas;
            }
        }
        #endregion

        #region ValidaTecnicoAsignado
        public bool ValidaTecnicoAsignado(string ot, string servicio, string tecnico)
        {
            bool resp = true;
            try
            {
                Globals.Query = "CALL SYP_SP_CITAS_DMS('',0,'','" + ot + "','" + tecnico + "','" + servicio + "','','VALTEC')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = false;
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ValidaTecnicoAsignado)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ValidaTecnicoAsignado

        #region AsignarTrabajos
        public void AsignarTrabajos(string docNum, int lineId)
        {
            try
            {
                var query = "CALL SYP_SP_CITAS_DMS('" + docNum + "', " + lineId + ",'','','','','','ASIGNAR')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
                Globals.SBO_Application.StatusBar.SetText("Registro Asignado con Exito  : ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(AsignarTrabajos) " + ex.Message);
            }
        }
        #endregion AsignarTrabajos

        #region ReAsignarTrabajos
        public void ReAsignarTrabajos(string docNum, string ot, string tecnico, string nomTecnico, string servicio)
        {
            try
            {
                var query = "CALL SYP_SP_CITAS_DMS('" + docNum + "', 0,'','" + ot + "','" + tecnico + "','" + servicio + "','" + nomTecnico + "','REASIGNAR')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
                Globals.SBO_Application.StatusBar.SetText("Registro Reasignado con Exito  : ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ReAsignarTrabajos) " + ex.Message);
            }
        }
        #endregion ReAsignarTrabajos

        #region QuitarTrabajos
        public void QuitarTrabajos(string docNum, string ot, string servicio)
        {
            try
            {
                var query = "CALL SYP_SP_CITAS_DMS('" + docNum + "', 0,'','" + ot + "','','" + servicio + "','','QUITAR')";
                Globals.runQuery(query);
                Globals.release(Globals.ORec);
                Globals.SBO_Application.StatusBar.SetText("Registro quitado con Exito", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(QuitarTrabajos) " + ex.Message);
            }
        }
        #endregion QuitarTrabajos

        #region Registro de Actividad

        public void CreaActividadPorAgendamiendoCitas(BusinessObjectInfo businessObjectInfo)
        {
            int iRet = -1;
            string errMsg = null;

            try
            {
                Globals.oCmpSrv = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
                ActivitiesService oActSrv = (ActivitiesService)Globals.oCmpSrv.GetBusinessService(ServiceTypes.ActivitiesService);
                Activity oAct = (Activity)oActSrv.GetDataInterface(ActivitiesServiceDataInterfaces.asActivity);
                ActivityParams oParams = (ActivityParams)oActSrv.GetDataInterface(ActivitiesServiceDataInterfaces.asActivityParams);

                string keyXml = businessObjectInfo.ObjectKey;
                string docNum = General.ObtenerTagXml(keyXml, "Code");

                foreach (var cab in ConsultaActividadesPendientes(docNum))
                {
                    if (ValidaPlaca(cab.placa))
                    {
                        foreach (var oActi in ConsultaDatosLlamadaServicio(cab.placa, docNum , "OTACTIVIDADES"))
                        {
                            if (oActi.activityCode != 0)
                            {
                                oParams.ActivityCode = oActi.activityCode;
                                oAct = oActSrv.GetActivity(oParams);
                            }

                            oAct.HandledByRecipientList = Convert.ToInt32(General.ObtieneParametro("DMS_LISTA_ACTI", "U_SYP_ATRIB1"));
                            oAct.CardCode = oActi.cardCode;
                            oAct.Activity = BoActivities.cn_Task;
                            oAct.Details = oActi.servicio;
                            oAct.Subject = 1;
                            oAct.StartTime = DateTime.Parse(oActi.hora); 
                            oAct.StartDate = DateTime.Parse(oActi.fecha);
                            oAct.Duration = 60;
                            oAct.Notes = "Actividad Generada desde Agendamiendo de Citas";


                            if (oActi.activityCode != 0)
                            {
                                oActSrv.UpdateActivity(oAct);
                            }
                            else
                            {
                                ActivityParams oPa;
                                oPa = oActSrv.AddActivity(oAct);
                                long singleActCode = 0;
                                singleActCode = oPa.ActivityCode;
                                if (singleActCode != 0)
                                    ActualizaEstadoActividades(docNum, cab.lineId, singleActCode.ToString());

                            }
                        }
                    }
                    else
                    {
                        Globals.SBO_Application.StatusBar.SetText("No existe la tarjeta del vehículo de la placa " + cab.placa + " Favor registre la tarjeta del Vehículo y vuelva a procesar.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("Ec: Error en crear Orden de Trabajo " + ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion

        #region AsociarDocumentoLlamadaServicio
        public void AsociarDocumentoLlamadaServicio(BusinessObjectInfo businessObjectInfo, string tipoDocumento)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");
                string numeroOt = "";
                StockTransfer sTra = null;
                Documents oDoc  = null;

                switch (tipoDocumento)
                {
                    case "940":
                        sTra = (StockTransfer)Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer);
                        sTra.GetByKey(Convert.ToInt32(docEntry));
                        numeroOt = (sTra.UserFields.Fields.Item("U_SYP_NOT")).Value.ToString();
                        break;
                    case "139":
                        oDoc = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                        oDoc.GetByKey(Convert.ToInt32(docEntry));
                        numeroOt = (oDoc.UserFields.Fields.Item("U_SYP_NOT")).Value.ToString();
                        break;
                    case "133":
                        oDoc = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                        oDoc.GetByKey(Convert.ToInt32(docEntry));
                        numeroOt = (oDoc.UserFields.Fields.Item("U_SYP_NOT")).Value.ToString();
                        break;
                    case "140":
                        oDoc = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oDeliveryNotes);
                        oDoc.GetByKey(Convert.ToInt32(docEntry));
                        numeroOt = (oDoc.UserFields.Fields.Item("U_SYP_NOT")).Value.ToString();
                        break;
                    case "149":
                        oDoc = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oQuotations);
                        oDoc.GetByKey(Convert.ToInt32(docEntry));
                        numeroOt = (oDoc.UserFields.Fields.Item("U_SYP_NOT")).Value.ToString();
                        break;
                }

                if (String.IsNullOrEmpty(numeroOt)) return;

                ServiceCalls llSer = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                llSer.GetByKey(Convert.ToInt32(numeroOt));
                llSer.Expenses.SetCurrentLine(llSer.Expenses.Count - 1);
                llSer.Expenses.Add();

                switch (tipoDocumento)
                {
                    case "940":
                        llSer.Expenses.DocEntry = Convert.ToInt32(sTra.DocEntry);
                        llSer.Expenses.DocumentType = BoSvcEpxDocTypes.edt_StockTransfer;
                        break;
                    case "139":
                        llSer.Expenses.DocEntry = Convert.ToInt32(oDoc.DocEntry);
                        llSer.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Order;
                        break;
                    case "133":
                        llSer.Expenses.DocEntry = Convert.ToInt32(oDoc.DocEntry);
                        llSer.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Invoice;
                        break;
                    case "140":
                        llSer.Expenses.DocEntry = Convert.ToInt32(oDoc.DocEntry);
                        llSer.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Delivery;
                        break;
                    case "149":
                        llSer.Expenses.DocEntry = Convert.ToInt32(oDoc.DocEntry);
                        llSer.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Quotation;
                        break;
                }
                

                if (llSer.Update() != 0)
                {
                    int lErrorCode;
                    string sErrMsg;
                    Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                    throw new Exception(sErrMsg);
                }

                Globals.SBO_Application.StatusBar.SetText("Se actualizó llamada de sevicio: ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex) 
            {
                Globals.SBO_Application.StatusBar.SetText(ex.Message);
            }

        }
        #endregion AsociarDocumentoLlamadaServicio

        #region CreaUdoTecnico
        public void CreaUdoTecnico(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                var keyXml = businessObjectInfo.ObjectKey;
                var docEntry = General.ObtenerTagXml(keyXml, "ServiceCallID");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                ServiceCalls oLls = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                oLls.GetByKey(Convert.ToInt32(docEntry));

                string udo = Convert.ToString(oLls.UserFields.Fields.Item("U_SYP_TECNICOS").Value);
                udo = udo.Replace("0", "");

                if (string.IsNullOrEmpty(udo))
                {
                    oGeneralService = Globals.oCmpSrv.GetGeneralService("SLRT");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        string fecha = DateTime.Now.ToString("yyyy/MM/dd");
                        // Se definen los datos de UDO
                        oGeneralData.SetProperty("Code", oLls.ServiceCallID.ToString());
                        oGeneralData.SetProperty("Name", oLls.ServiceCallID.ToString());
                        oGeneralData.SetProperty("U_SYP_ESTADO", "PENDIENTE");
                        oGeneralData.SetProperty("U_SYP_FECHA", fecha);
                        oGeneralService.Add(oGeneralData);
                        Globals.SBO_Application.SetStatusBarMessage(
                            "SYP: UDO-Tecnicos: Registro por defecto creado con éxito", BoMessageTime.bmt_Medium,
                            false);

                        oLls.UserFields.Fields.Item("U_SYP_TECNICOS").Value = Convert.ToString(oLls.ServiceCallID);
                        oLls.Update();

                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Tecnicos: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(CreaUdoTecnico)  : " + ex.Message);
            }
        }
        #endregion CreaUdoTecnico

        #region UpdateUdoTecnico
        public void UpdateUdoTecnico(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                var keyXml = businessObjectInfo.ObjectKey;
                var docEntry = General.ObtenerTagXml(keyXml, "ServiceCallID");
                bool flagUpdate = false;

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                ServiceCalls oLls = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                oLls.GetByKey(Convert.ToInt32(docEntry));

                string udo = Convert.ToString(oLls.UserFields.Fields.Item("U_SYP_TECNICOS").Value);
                udo = udo.Replace("0", "");
                if (!string.IsNullOrEmpty(udo))
                {

                    oGeneralService = Globals.oCmpSrv.GetGeneralService("SLRT");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        oGeneralParams.SetProperty("Code", Convert.ToString(oLls.ServiceCallID));
                        oGeneralData = oGeneralService.GetByParams(oGeneralParams);

                        Globals.Query = "CALL SYP_SP_DMS_GENERAL('" + docEntry + "', '', '', '', '', '', '', 'UDOTEC')";
                        Globals.runQuery(Globals.Query);
                        Globals.ORec.MoveFirst();

                        for (int i = 0; i < Globals.ORec.RecordCount; i++)
                        {
                            string itemCode = Globals.ORec.Fields.Item("ItemCode").Value.ToString();
                            string dscription = Globals.ORec.Fields.Item("Dscription").Value.ToString();
                            string parte = Globals.ORec.Fields.Item("Parte").Value.ToString();

                            oChildren = oGeneralData.Child("SYP_DET_REGTEC");
                            oChild = oChildren.Add();
                            oChild.SetProperty("U_SYP_ITEM_CODE", itemCode);
                            oChild.SetProperty("U_SYP_ITEM_NAME", dscription);
                            oChild.SetProperty("U_SYP_PART_VEH", parte);
                            flagUpdate = true;
                            Globals.ORec.MoveNext();
                        }
                        Globals.release(Globals.ORec);

                        if (!flagUpdate) return;
                        oGeneralService.Update(oGeneralData);
                        Globals.SBO_Application.SetStatusBarMessage(
                            "SYP: UDO-Tecnicos: Operaciones de técnicos agregadas con éxito",
                            BoMessageTime.bmt_Medium,
                            false);
                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Tecnicos: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(UpdateUdoTecnico)  : " + ex.Message);
            }
        }

        #endregion

        #region CreaUdoEstadoVehiculo
        public void CreaUdoEstadoVehiculo(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "ServiceCallID");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                ServiceCalls calls = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                calls.GetByKey(Convert.ToInt32(docEntry));


                string udo = calls.UserFields.Fields.Item("U_SYP_INVTDA").Value.ToString();

                if (String.IsNullOrEmpty(udo) || calls.ServiceCallID.ToString() != udo)
                {

                    oGeneralService = Globals.oCmpSrv.GetGeneralService("OINVTDA");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        // Se definen los datos de UDO
                        oGeneralData.SetProperty("Code", calls.ServiceCallID.ToString());
                        oGeneralData.SetProperty("Name", calls.ServiceCallID.ToString());

                        Globals.Query = "CALL SYP_SP_DMS_GENERAL('" + docEntry + "', '', '', '', '', '', '', 'ESTADOVEH')";
                        Globals.runQuery(Globals.Query);
                        Globals.ORec.MoveFirst();

                        for (int i = 0; i < Globals.ORec.RecordCount; i++)
                        {
                            string parte = Globals.ORec.Fields.Item("Parte").Value.ToString();

                            oChildren = oGeneralData.Child("SYP_DET_INVTDA");
                            oChild = oChildren.Add();
                            oChild.SetProperty("U_SYP_DESCD", parte);
                            Globals.ORec.MoveNext();
                        }
                        Globals.release(Globals.ORec);

                        oGeneralService.Add(oGeneralData);

                        Globals.SBO_Application.SetStatusBarMessage("SYP: UDO-Estado del vehículo: Registro por defecto creado con éxito", BoMessageTime.bmt_Medium, false);
                        calls.UserFields.Fields.Item("U_SYP_INVTDA").Value = calls.ServiceCallID.ToString();
                        calls.Update();

                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Estado de vehículo: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(CreaUdoEstadoVehiculo)  : " + ex.Message);
            }
        }
        #endregion

        #region CreaUdoInventarioVehiculos
        public void CreaUdoInventarioVehiculos(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "ServiceCallID");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                ServiceCalls calls = (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                calls.GetByKey(Convert.ToInt32(docEntry));

                string udo = calls.UserFields.Fields.Item("U_SYP_INVTVEH").Value.ToString();

                if (String.IsNullOrEmpty(udo) || Convert.ToString(calls.ServiceCallID) != udo)
                {

                    oGeneralService = Globals.oCmpSrv.GetGeneralService("OINVTVEH");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        // Se definen los datos de UDO
                        oGeneralData.SetProperty("Code", calls.ServiceCallID.ToString());
                        oGeneralData.SetProperty("Name", calls.ServiceCallID.ToString());

                        Globals.Query = "CALL SYP_SP_DMS_GENERAL('" + docEntry + "', '', '', '', '', '', '', 'INVENTARIOVEH')";
                        Globals.runQuery(Globals.Query);
                        Globals.ORec.MoveFirst();

                        for (int i = 0; i < Globals.ORec.RecordCount; i++)
                        {
                            string parte = Globals.ORec.Fields.Item("Parte").Value.ToString();

                            oChildren = oGeneralData.Child("SYP_DET_INVTVEH");
                            oChild = oChildren.Add();
                            oChild.SetProperty("U_SYP_DESC", parte);
                            oChild.SetProperty("U_SYP_OPCION", "N");
                            Globals.ORec.MoveNext();
                        }
                        Globals.release(Globals.ORec);

                        oGeneralService.Add(oGeneralData);

                        Globals.SBO_Application.SetStatusBarMessage("SYP: UDO-Inventario de vehiculos: Registro por defecto creado con éxito", BoMessageTime.bmt_Medium, false);
                        calls.UserFields.Fields.Item("U_SYP_INVTVEH").Value = calls.ServiceCallID.ToString();
                        calls.Update();

                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Inventario de vehiculos: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(CreaUdoInventarioVehiculos)  : " + ex.Message);
            }
        }
        #endregion

        #region CreaUdoFormaPago
        public void CreaUdoFormaPago(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Documents oSOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oSOrder.GetByKey(Convert.ToInt32(docEntry));

                string udo = oSOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value.ToString();

                if (String.IsNullOrEmpty(udo) || Convert.ToString(oSOrder.DocEntry) != udo)
                {
                    Globals.Query = "SELECT T0.\"Code\" FROM \"@SYP_FORPAGO\" T0 WHERE T0.\"Code\" = '" + oSOrder.DocEntry + "'";
                    Globals.runQuery(Globals.Query);
                    string codeUdo = Globals.ORec.Fields.Item("Code").Value.ToString();
                    Globals.release(Globals.ORec);

                    if (codeUdo != "" && udo != codeUdo)
                    {
                        oSOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value = oSOrder.DocEntry.ToString();
                        oSOrder.Update();
                        return;
                    }

                    oGeneralService = Globals.oCmpSrv.GetGeneralService("OFORPAGO");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        // Se definen los datos de UDO
                        oGeneralData.SetProperty("Code", oSOrder.DocEntry.ToString());
                        oGeneralData.SetProperty("Name", oSOrder.DocEntry.ToString());
                        oGeneralData.SetProperty("U_SYP_CODSN", oSOrder.CardCode);
                        oGeneralData.SetProperty("U_SYP_NAMESN", oSOrder.CardName);
                        oGeneralData.SetProperty("U_SYP_DATE", oSOrder.DocDate);

                        //oChildren = oGeneralData.Child("SYP_DET_FORPAGO");
                        //oChild = oChildren.Add();
                        //oChild.SetProperty("U_SYP_FORPAGO", "Efectivo");
                        //oChild.SetProperty("U_SYP_MONTOFP", 0.00);
                        oGeneralService.Add(oGeneralData);
                        Globals.SBO_Application.SetStatusBarMessage(
                            "SYP: UDO-Forma de Pago: Registro por defecto creado con éxito", BoMessageTime.bmt_Medium,
                            false);

                        oSOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value = oSOrder.DocEntry.ToString();
                        oSOrder.Update();

                        Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '" + oSOrder.DocEntry.ToString() + "', '', '', '', '', '', 'SALDOOV')";
                        Globals.runQuery(Globals.Query);

                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Vehículo parte de Pago: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(CreaUdoFormaPago)  : " + ex.Message);
            }
        }
        #endregion

        #region CreaUdoDispositivo
        public void CreaUdoDispositivo(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Documents oSOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oSOrder.GetByKey(Convert.ToInt32(docEntry));

                string udo = oSOrder.UserFields.Fields.Item("U_SYP_ENTDPA").Value.ToString();

                if (String.IsNullOrEmpty(udo) || Convert.ToString(oSOrder.DocEntry) != udo)
                {
                    Globals.Query = "SELECT T0.\"Code\" FROM \"@SYP_FORPAGO\" T0 WHERE T0.\"Code\" = '" + oSOrder.DocEntry + "'";
                    Globals.runQuery(Globals.Query);
                    string codeUdo = Globals.ORec.Fields.Item("Code").Value.ToString();
                    Globals.release(Globals.ORec);

                    if (codeUdo != "" && udo != codeUdo)
                    {
                        oSOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value = oSOrder.DocEntry.ToString();
                        oSOrder.Update();
                        return;
                    }

                    oGeneralService = Globals.oCmpSrv.GetGeneralService("OFORPAGO");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    try
                    {
                        // Se definen los datos de UDO
                        oGeneralData.SetProperty("Code", oSOrder.DocEntry.ToString());
                        oGeneralData.SetProperty("Name", oSOrder.DocEntry.ToString());
                        oGeneralData.SetProperty("U_SYP_CODSN", oSOrder.CardCode);
                        oGeneralData.SetProperty("U_SYP_NAMESN", oSOrder.CardName);
                        oGeneralData.SetProperty("U_SYP_DATE", oSOrder.DocDate);

                        oChildren = oGeneralData.Child("SYP_DET_FORPAGO");
                        oChild = oChildren.Add();
                        oChild.SetProperty("U_SYP_FORPAGO", "Efectivo");
                        oChild.SetProperty("U_SYP_MONTOFP", 0.00);
                        oGeneralService.Add(oGeneralData);
                        Globals.SBO_Application.SetStatusBarMessage(
                            "SYP: UDO-Forma de Pago: Registro por defecto creado con éxito", BoMessageTime.bmt_Medium,
                            false);

                        oSOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value = oSOrder.DocEntry.ToString();
                        oSOrder.Update();

                        Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '" + oSOrder.DocEntry.ToString() + "', '', '', '', '', '', 'SALDOOV')";
                        Globals.runQuery(Globals.Query);

                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: UDO-Vehículo parte de Pago: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText("(CreaUdoDispositivo)  : " + ex.Message);
            }
        }
        #endregion

        #region ActualizaOvTotalFormaPago
        public void ActualizaOvTotalFormaPago(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                #region Obtengo Datos
                string keyXml = businessObjectInfo.ObjectKey;
                string code = General.ObtenerTagXml(keyXml, "Code"); // Code del UDO
                int docEntryPago = 0;

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                oGeneralService = Globals.oCmpSrv.GetGeneralService("OFORPAGO");
                oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);


                oGeneralParams.SetProperty("Code", code);
                oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                double totalFpCabecera = Convert.ToDouble(oGeneralData.GetProperty("U_SYP_TFORPAG"));
                
                string docEntryOv = oGeneralData.GetProperty("Name").ToString();
                DateTime fechaContaDate = (DateTime) oGeneralData.GetProperty("U_SYP_DATE");
                string enviarPago = oGeneralData.GetProperty("U_SYP_ENVIAR_PAGO").ToString();

                string fechaConta = fechaContaDate.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
                #endregion

                #region  Realizo Pagos Recibidos

                List<int> listIdFormaPago = new List<int>();

                Documents oSOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oSOrder.GetByKey(Convert.ToInt32(docEntryOv));

                string tieneDisp = oSOrder.UserFields.Fields.Item(Constantes.DISPTIENE).Value.ToString();
                string asientoDisp = oSOrder.UserFields.Fields.Item(Constantes.DISPASIENTO).Value.ToString();
                string valorDisp = oSOrder.UserFields.Fields.Item(Constantes.DISPVALOR).Value.ToString();

                if (enviarPago.Equals("NO"))
                {
                    return;
                }

                if (String.IsNullOrEmpty(asientoDisp))
                {
                    Globals.SBO_Application.SetStatusBarMessage(
                        "Debe generar el Asiento del Dispositivo antes de generar el Pago Recibido.",
                        BoMessageTime.bmt_Short);
                    return;
                }

                int numeroPagos = ObtieneNumeroPagos(docEntryOv, fechaConta, enviarPago);

                for (int i = 0; i < numeroPagos; i++)
                {
                    listIdFormaPago = new List<int>();

                    Globals.Query = "CALL SYP_SP_PAGOS_RECIBIDOS('" + docEntryOv + "', '" + fechaConta + "', '" + enviarPago + "','DISPOSITIVO')";
                    Globals.runQuery(Globals.Query);
                    int dipositivo = Convert.ToInt32(Globals.ORec.Fields.Item(0).Value);
                    Globals.release(Globals.ORec);

                    Globals.Query = "CALL SYP_SP_PAGOS_RECIBIDOS('" + docEntryOv + "', '" + fechaConta + "', '" + enviarPago + "','PAGOS')";
                    Globals.runQuery(Globals.Query);

                    if (Globals.ORec.RecordCount > 0)
                    {

                        Payments oPre = (Payments) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oIncomingPayments));

                        //Datos Generales del Pago Recibido Preliminar
                        oPre.DocDate = DateTime.ParseExact(fechaConta, "yyyyMMdd", CultureInfo.InvariantCulture);
                        oPre.DocType = BoRcptTypes.rCustomer;
                        oPre.CardCode = oSOrder.CardCode;
                        oPre.DocObjectCode = BoPaymentsObjectType.bopot_IncomingPayments;
                        oPre.DocTypte = BoRcptTypes.rCustomer;
                        oPre.CounterReference = oSOrder.DocEntry.ToString();
                        oPre.UserFields.Fields.Item("U_SYP_OVENTA").Value = oSOrder.DocNum.ToString();
                        oPre.UserFields.Fields.Item("U_SYP_SUC").Value =
                            oSOrder.UserFields.Fields.Item("U_SYP_SUC").Value;

                        if (tieneDisp.Equals("SI") && dipositivo > 0)
                        {
                            oPre.Invoices.InvoiceType = BoRcptInvTypes.it_JournalEntry;
                            oPre.Invoices.DocEntry = Convert.ToInt32(asientoDisp);
                            oPre.Invoices.SumApplied = Convert.ToDouble(valorDisp, CultureInfo.InvariantCulture);
                            //oPre.Invoices.UserFields.Fields.Item("U_SYP_FPAGO").Value = t.codigoFp;
                            oPre.Invoices.Add();
                        }

                        Globals.ORec.MoveFirst();
                        while (!Globals.ORec.EoF)
                        {
                            int lineId = Convert.ToInt32(Globals.ORec.Fields.Item(0).Value);
                            string formapago = Globals.ORec.Fields.Item(1).Value.ToString();
                            DateTime fechaEmision = (DateTime) Globals.ORec.Fields.Item(2).Value;
                            DateTime fechaVencimiento = (DateTime) Globals.ORec.Fields.Item(3).Value;
                            string codigoBanco = Globals.ORec.Fields.Item(4).Value.ToString();
                            string nombreBanco = Globals.ORec.Fields.Item(5).Value.ToString();
                            string documento = Globals.ORec.Fields.Item(6).Value.ToString();
                            double importePago = Convert.ToDouble(Globals.ORec.Fields.Item(7).Value,
                                CultureInfo.InvariantCulture);
                            string cuentaContable = Globals.ORec.Fields.Item(8).Value.ToString();
                            string medioPago = Globals.ORec.Fields.Item(9).Value.ToString();
                            string codigoFp = Globals.ORec.Fields.Item(10).Value.ToString();

                            listIdFormaPago.Add(lineId);

                            switch (medioPago)
                            {
                                case "01": //1. Cheques
                                {
                                    oPre.Checks.Add();
                                    oPre.CheckAccount = cuentaContable;
                                    oPre.Checks.CountryCode = "EC";
                                    oPre.Checks.BankCode = codigoBanco;
                                    oPre.Checks.CheckNumber = int.Parse("0" + documento);
                                    oPre.Checks.CheckSum = importePago;
                                    oPre.Checks.DueDate = fechaVencimiento;
                                    break;
                                }
                                case "02": //2. Transferencias
                                {
                                    oPre.TransferAccount = cuentaContable;
                                    oPre.TransferDate = fechaEmision;
                                    oPre.TransferReference = documento;
                                    oPre.TransferSum = importePago;
                                    break;
                                }
                                case "03": //3. Tarjeta de Crédito
                                {

                                    GlobalsAlt.Query = "SELECT \"CreditCard\" FROM OCRC WHERE \"AcctCode\" = '" +
                                                       cuentaContable + "' AND \"CardName\" ='" + formapago + "'";
                                    GlobalsAlt.runQuery(GlobalsAlt.Query);
                                    int codigoTarjeta = Convert.ToInt32("0" + GlobalsAlt.ORec.Fields.Item(0).Value);

                                    oPre.CreditCards.Add();
                                    oPre.CreditCards.CreditCard = codigoTarjeta;
                                    oPre.CreditCards.CreditAcct = cuentaContable;
                                    oPre.CreditCards.CreditCardNumber = "9999";
                                    oPre.CreditCards.VoucherNum = "9999";
                                    oPre.CreditCards.OwnerIdNum = "9999";
                                    oPre.CreditCards.CardValidUntil = DateTime.Today;
                                    oPre.CreditCards.CreditSum = importePago;
                                    break;
                                }

                                case "04": //4. Fondo en Efectivo
                                {

                                    oPre.CashAccount = cuentaContable;
                                    oPre.CashSum = importePago;
                                    break;
                                }

                                default:
                                {
                                    break;
                                }
                            }

                            Globals.ORec.MoveNext();
                        }

                        int result = oPre.Add();

                        if (result != 0)
                        {
                            int tempInt;
                            string tempString;
                            Globals.oCompany.GetLastError(out tempInt, out tempString);

                            Globals.SBO_Application.SetStatusBarMessage(
                                "SYP: Error en crear Pago Recibido >> [" + tempInt + "] [" + tempString + "]",
                                BoMessageTime.bmt_Short);
                            //oForm.Close();
                            return;
                        }
                        docEntryPago = Int32.Parse(Globals.oCompany.GetNewObjectKey());

                        Globals.SBO_Application.SetStatusBarMessage("SYP: Pago Recibido creado con éxito " + docEntryPago,BoMessageTime.bmt_Medium,false);

                        Globals.release(Globals.ORec);
                    }

                    Payments oPNew = (Payments)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oIncomingPayments));
                    if (oPNew.GetByKey(docEntryPago))
                    {
                        string serie = ConsultarSeries(oPNew.Series.ToString());
                        ActualizarEstadoFormaPago(code, serie, docEntryPago.ToString(), listIdFormaPago);
                    }
                }

                #endregion  Realizo Pagos Recibidos

                #region Actualizo OV
                try
                {
                    Documents oOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                    oOrder.GetByKey(Convert.ToInt32(docEntryOv));

                    double totalOv = Convert.ToDouble(oOrder.DocTotal.ToString(CultureInfo.InvariantCulture));
                    oOrder.UserFields.Fields.Item("U_SYP_FPAGO").Value = totalFpCabecera;
                    oOrder.UserFields.Fields.Item("U_SYP_SALDO").Value = totalOv - totalFpCabecera;// - totalVpCabecera;
                    oOrder.Update();
                }
                catch (Exception ex)
                {
                    var msjError = "SYP: UDO-Forma de Pago: " + ex.Message;
                    throw new Exception(msjError);
                }
                #endregion Actualizo OV
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }
        #endregion

        #region ObtieneNumeroPagos
        public int ObtieneNumeroPagos(string docEntryOv, string fechaConta, string enviarPago)
        {
            int resp = 0;
            try
            {
                Globals.Query = "CALL SYP_SP_PAGOS_RECIBIDOS('" + docEntryOv + "', '" + fechaConta + "', '" + enviarPago + "','NUMPAGOS')";
                Globals.runQuery(Globals.Query);
                if (Globals.ORec.RecordCount > 0)
                {
                    resp = Convert.ToInt32(Globals.ORec.Fields.Item(0).Value.ToString());
                }
                Globals.release(Globals.ORec);

                return resp;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("(ObtieneNumeroPagos)  : " + ex.Message);
                return resp;
            }
        }
        #endregion ObtieneNumeroPagos

        #region CrearUdoDesgloseBonos
        public void CrearUdoDesgloseBonos(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();
                Documents oOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oOrder.GetByKey(Convert.ToInt32(docEntry));

                //Ini Jmarquez se comenta porque no se usan estos campos
                string ovCompletada = oOrder.UserFields.Fields.Item("U_SYP_COMPLET").Value.ToString();
                string estadoAutorizacion = oOrder.UserFields.Fields.Item("U_SYP_ESTAUTOR").Value.ToString();

                if(ovCompletada == "1" && estadoAutorizacion == "1")
                 {
                    return;
                }

                string NoActualizarBonosOvCompletada = "";

                //Parametro parametroNAB = ParametroServiceUtil.get(LN.b1.Properties.Parametros.NO_ACTUALIZAR_BONOS_OV_COMPLETADA);
                string parametroNAB = General.ObtieneParametro("DMS_ACTUALIZ_OV", "U_SYP_ATRIB1");

                if (!String.IsNullOrEmpty(parametroNAB))
                {
                    NoActualizarBonosOvCompletada = parametroNAB;//parametroNAB.Atributo1;

                    if (NoActualizarBonosOvCompletada.ToUpper() == "SI")
                    {
                        if (ovCompletada == "1" && estadoAutorizacion == "0")
                        {
                            return;
                        }
                    }
                }
                //Fin Jmarquez se comenta porque no se usan estos campos

                for (int i = 0; i < oOrder.Lines.Count; i++)
                {
                    bool flagExisteDesglose = false;
                    oOrder.Lines.SetCurrentLine(i);

                    string campoIdDesgloseBono = oOrder.Lines.UserFields.Fields.Item("U_SYP_DSGL_BONO").Value.ToString();
                    string idDesgloseBono = oOrder.DocEntry + "-" + oOrder.Lines.LineNum;
                    double montoBonos = Convert.ToDouble("0" + oOrder.Lines.UserFields.Fields.Item("U_SYP_MONTO_BONO").Value, CultureInfo.InvariantCulture);

                    if (/* oOrder.Lines.LineStatus == BoStatus.bost_Close ||*/ (ovCompletada == "1" && estadoAutorizacion == "1"))
                    {
                        continue; // Se sale del ciclo si los montos son iguales
                    }

                    string qry = "SELECT SUM(IFNULL(T1.\"U_SYP_MONTO\",0)) \"SUMMONTO\", T0.\"Code\" FROM \"@SYP_DBON\" T0 LEFT JOIN \"@SYP_DBON_DET\" T1 ON T1.\"Code\" = T0.\"Code\" WHERE T0.\"Code\" = '" + idDesgloseBono + "' GROUP BY T0.\"Code\"";
                    Globals.runQuery(qry);

                    double sumBonosUdo = Convert.ToDouble("0" + Globals.ORec.Fields.Item("SUMMONTO").Value, CultureInfo.InvariantCulture);

                    if (Globals.ORec.Fields.Item("Code").Value.ToString() == idDesgloseBono) flagExisteDesglose = true; // Exite udo de desglose

                    if (Math.Abs(sumBonosUdo - montoBonos) < 0)
                    {
                        //Si no existe udo
                        if (campoIdDesgloseBono != Globals.ORec.Fields.Item("Code").Value.ToString() && Math.Abs(montoBonos) < 0)
                        {
                            oOrder.Lines.UserFields.Fields.Item("U_SYP_DSGL_BONO").Value = "";
                            oOrder.Update();
                        }
                        continue; // Se sale del ciclo si los montos son iguales
                    }

                    //Cargo Variables
                    string modelo = oOrder.Lines.ItemCode;
                    string modeloDescripcion = oOrder.Lines.ItemDescription;
                    string financiera = General.ObtieneParametro("DMS_FINACIERA", "U_SYP_ATRIB1");
                    string tipoVenta = oOrder.UserFields.Fields.Item("U_SYP_TPOVENTA").Value.ToString();
                    string claseVenta = oOrder.UserFields.Fields.Item("U_SYP_TVENTA").Value.ToString();
                    string listaPrecio = "";//oOrder.UserFields.Fields.Item("U_SYP_LISTAP").Value.ToString();
                    double porcentaje = 0;//Convert.ToDouble("0" + oOrder.UserFields.Fields.Item("U_SYP_PIE_PTJE").Value.ToString(), CultureInfo.InvariantCulture);
                    string tipoCredito = "";//oOrder.UserFields.Fields.Item("U_SYP_TCRED").Value.ToString();
                    string dealerNet = string.Empty;
                    string margenAP = string.Empty;
                    qry = "CALL \"SYP_SP_DESGLOSE_BONOS\" "
                                  + " ( '" + modelo + "'"
                                  + " , '" + financiera + "'"
                                  + " , '" + tipoVenta + "'"
                                  + " , '" + claseVenta + "'"
                                  + " , '" + listaPrecio + "'"
                                  + " , " + porcentaje
                                  + " , '" + tipoCredito + "'"
                                  + " , null )";
                    Globals.runQuery(qry);
                    System.Data.DataTable dtBonos = Globals.convertRecordSet(Globals.ORec);

                    try
                    {
                        oGeneralService = Globals.oCmpSrv.GetGeneralService("DBON");
                        oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                        oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                        //if (dtBonos.Rows.Count > 0)
                        //{
                        //    dealerNet = dtBonos.Rows[0]["DEALERNET"].ToString();
                        //    margenAP = dtBonos.Rows[0]["MARGEN"].ToString();
                        //}

                        if (flagExisteDesglose)
                        {
                            oGeneralParams.SetProperty("Code", idDesgloseBono);
                            oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                        }

                        oGeneralData.SetProperty("Code", idDesgloseBono);
                        oGeneralData.SetProperty("U_SYP_CODMOD", modelo);
                        oGeneralData.SetProperty("U_SYP_DESCMOD", modeloDescripcion);
                        //oGeneralData.SetProperty("U_SYP_FINAN", financiera);
                        //oGeneralData.SetProperty("U_SYP_PTJEAPLI", porcentaje);
                        //oGeneralData.SetProperty("U_SYP_DEALERNET", dealerNet);
                        //oGeneralData.SetProperty("U_SYP_MARGENAP", margenAP);

                        oChildren = oGeneralData.Child("SYP_DBON_DET");

                        if (flagExisteDesglose)
                        {
                            while (oChildren.Count != 0)
                            {
                                oChildren.Remove(0);
                            }
                        }

                        DataRow[] result = dtBonos.Select();

                        if (result.Length == 0) continue; // Se sale del bucle 
                        foreach (DataRow row in result)
                        {
                            string codigoBono = row["BONO_CODE"].ToString();
                            string descripcionBono = row["BONO_DESC"].ToString();
                            double monto = Convert.ToDouble(row["MONTO"].ToString(), CultureInfo.InvariantCulture);
                            double montoSinIva = Convert.ToDouble(row["MONTOSINIVA"].ToString(), CultureInfo.InvariantCulture);
                            //string bonoRecuperable = row["BONO_RECUP"].ToString();

                            oChild = oChildren.Add();
                            oChild.SetProperty("U_SYP_CODEB", codigoBono);
                            oChild.SetProperty("U_SYP_DESCB", descripcionBono);
                            oChild.SetProperty("U_SYP_MONTO", monto);
                            oChild.SetProperty("U_SYP_MONTOSINIVA", montoSinIva);
                            //oChild.SetProperty("U_SYP_RECUP", bonoRecuperable);
                        }

                        if (flagExisteDesglose)
                        {
                            oGeneralService.Update(oGeneralData);
                            Globals.SBO_Application.StatusBar.SetText("DMS: UDO Desglose de bonos actualizado con éxito.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                        }
                        else
                        {
                            oGeneralService.Add(oGeneralData);
                            Globals.SBO_Application.StatusBar.SetText("DMS: UDO Desglose de bonos creado con éxito.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                        }
                        oOrder.Lines.UserFields.Fields.Item("U_SYP_DSGL_BONO").Value = idDesgloseBono;
                        oOrder.Update();
                    }
                    catch (Exception ex)
                    {
                        var msjError = "DMS: UDO Desglose de bonos Error: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox("DMS: UDO DBON: " + ex.Message);
            }
        }
        #endregion

        #region ActualizaPrecios
        public void ActualizaPrecios(Form oForm, int formTypeCode)
        {

            try
            {
                string tipoProcesoVenta = "";
                try
                {
                    try
                    {
                        tipoProcesoVenta = Globals.getUdfValue(Constantes.TIPOVENTA, Globals.UdfType.ComboBox, -1 * oForm.Type,formTypeCode);
                    }
                    catch
                    {tipoProcesoVenta = Globals.getUdfValue(Constantes.TIPOVENTA, Globals.UdfType.ComboBox, oForm.Type, formTypeCode);}
                }
                catch (Exception)
                {Globals.SBO_Application.SetStatusBarMessage("(Muestre los campos de usuario en el Menu Visualizar) ");}
                

                //Si no tiene valor el campo de usuario no se aplica cambio de bodega
                if ("".Equals(tipoProcesoVenta))
                {
                    Globals.SBO_Application.SetStatusBarMessage("No ha seleccionado el tipo de venta, esto puede provocar que el sistema no realice calculos automaticos.",BoMessageTime.bmt_Medium,false);
                    return;
                }

                string cardCode = ((EditText)oForm.Items.Item(Constantes.CARDCODE).Specific).Value;

                Matrix matrixDetalle = (Matrix)oForm.Items.Item("38").Specific;

                for (int i = 1; i <= matrixDetalle.RowCount - 1; i++)
                {
                    try
                    {
                        ((EditText) matrixDetalle.Columns.Item(Constantes.PRECIOUNITARIO).Cells.Item(i).Specific).Active = true;
                    }
                    catch (Exception) { return; }


                    EditText itemCode = (EditText)matrixDetalle.Columns.Item(Constantes.ITEMCODE).Cells.Item(i).Specific;
                    EditText precioUnitario = (EditText) matrixDetalle.Columns.Item(Constantes.PRECIOUNITARIO).Cells.Item(i).Specific;
                    EditText descuentoBono = (EditText)matrixDetalle.Columns.Item(Constantes.MONTOBONO).Cells.Item(i).Specific;

                    Globals.Query = Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '', '" + itemCode.Value + "', '" + cardCode + "', '', '', '" + tipoProcesoVenta + "', 'GRUPO')";
                    Globals.runQuery(Globals.Query);

                    double valorBono = Convert.ToDouble(Globals.ORec.Fields.Item("Bono").Value, CultureInfo.InvariantCulture);
                    double precioFinal = Convert.ToDouble(Globals.ORec.Fields.Item("PrecioFinal").Value, CultureInfo.InvariantCulture);

                    if (valorBono > 0)
                    {
                        descuentoBono.Active = true;
                        descuentoBono.Value = valorBono.ToString(CultureInfo.InvariantCulture);
                        descuentoBono.Active = false;

                        if (precioFinal > 0)
                        {
                            precioUnitario.Active = true;
                            precioUnitario.Value = precioFinal.ToString(CultureInfo.InvariantCulture);
                            precioUnitario.Active = false;
                            Globals.SBO_Application.SetStatusBarMessage("Se han actualizado los precios, bonos y descuentos relacionados", BoMessageTime.bmt_Medium, false);
                        }
                        else
                        {
                            Globals.SBO_Application.SetStatusBarMessage("La resta del precio menos el descuento por bono recae en una cantidad negativa, Revise el precio y actualice.", BoMessageTime.bmt_Medium, false);
                            return;
                        }
                        
                        //((EditText)matrixDetalle.Columns.Item(Constantes.PRECIOLISTA).Cells.Item(i).Specific).Value = "0";
                        //((EditText)matrixDetalle.Columns.Item(Constantes.PRECIOLISTA).Cells.Item(i).Specific).Value = pLista.ToString(CultureInfo.InvariantCulture);
                        //if(!price.Equals("0"))
                        //    ((EditText)matrixDetalle.Columns.Item(Constantes.PRECIOUNITARIO).Cells.Item(i).Specific).Value = price.ToString(CultureInfo.InvariantCulture);
                    }
                }

                oForm.ActiveItem = "16";
                

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage("(ActualizaPrecios) " + ex.Message);
            }
        }
        #endregion

        #region CrearSolicitudAnticipo
        public void CrearSolicitudAnticipo(Form oForm)
        {
            int docEntryAnticipo = 0;
            int numPag = 0;
            string docNum = ((EditText) oForm.Items.Item(Constantes.DOCNUM).Specific).Value;
            string serie = ((ComboBox)oForm.Items.Item(Constantes.SERIE).Specific).Value.Trim();
            string docEntry = General.ConsultarDocEntry(docNum, "ORDR", serie);

            GlobalsAlt.Query = "CALL SYP_SP_DMS_GENERAL('', '', '', '', '" + docEntry + "', '', '', 'GetTotalFormaDePago')";
            GlobalsAlt.runQuery(GlobalsAlt.Query);
            
            if (GlobalsAlt.ORec.RecordCount > 0)
            {
                GlobalsAlt.ORec.MoveFirst();
                while (!GlobalsAlt.ORec.EoF)
                {
                    #region Orden de Venta

                    string fecha = GlobalsAlt.ORec.Fields.Item("Fecha").Value.ToString();

                    Documents orden = ((Documents) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders)));
                    if (orden.GetByKey(Convert.ToInt32(docEntry)))
                    {
                        var solicitudAnticipo =
                            (Documents) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oDownPayments));

                        solicitudAnticipo.DocDate = DateTime.ParseExact(fecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                        solicitudAnticipo.DownPaymentType = DownPaymentTypeEnum.dptInvoice;
                        solicitudAnticipo.DocTotal = Convert.ToDouble(GlobalsAlt.ORec.Fields.Item("Monto").Value,CultureInfo.InvariantCulture);
                        solicitudAnticipo.UserFields.Fields.Item("U_SYP_MDTD").Value = "IV";
                        solicitudAnticipo.UserFields.Fields.Item("U_SYP_DESDOC").Value = "Interno de Ventas";

                        for (int i = 0; i < orden.Lines.Count; i++)
                        {
                            orden.Lines.SetCurrentLine(i);
                            double cantidadCompra = orden.Lines.Quantity;
                            if (cantidadCompra > 0) //Se crea solicitud de traslado
                            {
                                solicitudAnticipo.Lines.BaseEntry = orden.DocEntry;
                                solicitudAnticipo.Lines.BaseType = 17;
                                solicitudAnticipo.Lines.BaseLine = orden.Lines.LineNum;
                                solicitudAnticipo.Lines.ItemCode = orden.Lines.ItemCode;
                                solicitudAnticipo.Lines.TaxCode = General.ObtieneParametro("IVAST", "U_SYP_ATRIB1");
                                solicitudAnticipo.Lines.UnitPrice = orden.Lines.Price + orden.Lines.TaxTotal;
                                solicitudAnticipo.Lines.Add();
                            }
                        }

                        int result = solicitudAnticipo.Add();

                        if (result != 0)
                        {
                            int tempInt;
                            string tempString;
                            Globals.oCompany.GetLastError(out tempInt, out tempString);

                            Globals.SBO_Application.SetStatusBarMessage(
                                "SYP: Error en crear Factura de Anticipo >> [" + tempInt + "] [" + tempString + "]",
                                BoMessageTime.bmt_Short);
                        }


                        docEntryAnticipo = Convert.ToInt32(Globals.oCompany.GetNewObjectKey());
                        
                        Globals.SBO_Application.SetStatusBarMessage(
                            "SYP: Factura de Anticipo creada con éxito " + docEntryAnticipo, BoMessageTime.bmt_Medium,
                            false);

                        #endregion

                        #region Pago Recibido

                        if (numPag.Equals(0))
                        {
                            FormaPago[] listaFormaPago = ConsultarFormaPago(docEntry, fecha);

                            foreach (FormaPago t in listaFormaPago)
                            {
                                var pagoRecibidoPreliminar =
                                    (Payments) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oPaymentsDrafts));

                                //Datos Generales del Pago Recibido Preliminar
                                pagoRecibidoPreliminar.DocDate = DateTime.ParseExact(fecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                                pagoRecibidoPreliminar.DocType = BoRcptTypes.rCustomer;
                                pagoRecibidoPreliminar.CardCode = orden.CardCode;
                                pagoRecibidoPreliminar.DocObjectCode = BoPaymentsObjectType.bopot_IncomingPayments;
                                pagoRecibidoPreliminar.DocTypte = BoRcptTypes.rCustomer;
                                pagoRecibidoPreliminar.CounterReference = docEntry;
                                pagoRecibidoPreliminar.UserFields.Fields.Item("U_SYP_OVENTA").Value = orden.DocNum.ToString();
                                pagoRecibidoPreliminar.UserFields.Fields.Item("U_SYP_SUC").Value = orden.UserFields.Fields.Item("U_SYP_SUC").Value;

                                //Detalle de documentos - Asignación de Solicitud de Anticipo
                                pagoRecibidoPreliminar.Invoices.InvoiceType = BoRcptInvTypes.it_DownPayment;
                                pagoRecibidoPreliminar.Invoices.DocEntry = docEntryAnticipo;
                                pagoRecibidoPreliminar.Invoices.SumApplied = t.importePago;
                                pagoRecibidoPreliminar.Invoices.UserFields.Fields.Item("U_SYP_FPAGO").Value = t.codigoFp;
                                pagoRecibidoPreliminar.Invoices.Add();

                                //Detalle de pago - Asignación de Medio de Pago

                                switch (t.medioPago)
                                {
                                    case "01":
                                    {
                                        //1. Cheques
                                        pagoRecibidoPreliminar.CheckAccount = t.cuentaContable;
                                        pagoRecibidoPreliminar.Checks.CountryCode = "EC";
                                        pagoRecibidoPreliminar.Checks.BankCode = t.codigoBanco;
                                        pagoRecibidoPreliminar.Checks.CheckNumber = int.Parse("0" + t.documento);
                                        //POR DEFINIR Y VALIDAR
                                        pagoRecibidoPreliminar.Checks.CheckSum = t.importePago;
                                        pagoRecibidoPreliminar.Checks.DueDate = t.fechaVencimiento;
                                        break;
                                    }
                                    case "02":
                                    {
                                        //2. Transferencias
                                        pagoRecibidoPreliminar.TransferAccount = t.cuentaContable;
                                        pagoRecibidoPreliminar.TransferDate = t.fechaEmision;
                                        pagoRecibidoPreliminar.TransferReference = t.documento;
                                        pagoRecibidoPreliminar.TransferSum = t.importePago;
                                        break;
                                    }
                                    case "04":
                                    {
                                        //3. Fondo en Efectivo
                                        pagoRecibidoPreliminar.CashAccount = t.cuentaContable;
                                        pagoRecibidoPreliminar.CashSum = t.importePago;
                                        break;
                                    }

                                    case "03":
                                    {
                                        //4. Tarjeta de Crédito
                                        Globals.Query = "SELECT \"CreditCard\" FROM OCRC WHERE \"AcctCode\" = '" + 
                                                           t.cuentaContable + "' AND \"CardName\" ='" + t.formapago +
                                                           "'";
                                        Globals.runQuery(Globals.Query);
                                        int codigoTarjeta = Convert.ToInt32("0" + Globals.ORec.Fields.Item(0).Value);

                                        pagoRecibidoPreliminar.CreditCards.CreditCard = Convert.ToInt32(t.cuentaContable);
                                        pagoRecibidoPreliminar.CreditCards.CreditAcct = t.cuentaContable;
                                        pagoRecibidoPreliminar.CreditCards.CreditCardNumber = "9999";
                                        pagoRecibidoPreliminar.CreditCards.VoucherNum = "9999";
                                        pagoRecibidoPreliminar.CreditCards.OwnerIdNum = "9999";
                                        pagoRecibidoPreliminar.CreditCards.CardValidUntil = DateTime.Today;
                                        pagoRecibidoPreliminar.CreditCards.CreditSum = t.importePago;
                                        break;
                                    }
                                    default:
                                    {
                                        break;
                                    }

                                }

                                int resultP = pagoRecibidoPreliminar.Add();

                                if (resultP != 0)
                                {
                                    int tempInt;
                                    string tempString;
                                    Globals.oCompany.GetLastError(out tempInt, out tempString);

                                    Globals.SBO_Application.SetStatusBarMessage(
                                        "SYP: Error en crear Pago Preliminar >> [" + tempInt + "] [" + tempString + "]",
                                        BoMessageTime.bmt_Short);
                                }
                                var docEntryPagoPreliminar = Int32.Parse(Globals.oCompany.GetNewObjectKey());
                                Globals.SBO_Application.SetStatusBarMessage(
                                    "SYP: Pago Recibido Preliminar creada con éxito " + docEntryPagoPreliminar,
                                    BoMessageTime.bmt_Medium,
                                    false);
                            }
                        }
                        numPag++;
                    }

                    #endregion

                    GlobalsAlt.ORec.MoveNext();
                }
                GlobalsAlt.release(GlobalsAlt.ORec);
            }

            //return docEntryAnticipo;
        }
        #endregion

        #region CrearPagoRecibidoPreliminar

        public void CrearPagoRecibidoPreliminar(Form oForm, int docEntryAnticipo)
        {
            string docNum = ((EditText) oForm.Items.Item(Constantes.DOCNUM).Specific).Value;
            string serie = ((ComboBox) oForm.Items.Item(Constantes.SERIE).Specific).Value.Trim();
            string docEntry = General.ConsultarDocEntry(docNum, "ORDR", serie);

            Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '', '', '', '" + docEntry + "', '', '', 'GetTotalFormaDePago')";
            Globals.runQuery(Globals.Query);

            if (Globals.ORec.RecordCount > 0)
            {
                Globals.ORec.MoveFirst();
                while (!Globals.ORec.EoF)
                {
                    string fecha = Globals.ORec.Fields.Item("Fecha").Value.ToString();

                    FormaPago[] listaFormaPago = ConsultarFormaPago(docEntry, fecha);

                    Documents orden = ((Documents) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders)));
                    if (orden.GetByKey(Convert.ToInt32(docEntry)))
                    {
                        foreach (FormaPago t in listaFormaPago)
                        {
                            var pagoRecibidoPreliminar =
                                (Payments) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oPaymentsDrafts));

                            //Datos Generales del Pago Recibido Preliminar
                            pagoRecibidoPreliminar.DocDate = DateTime.ParseExact(fecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                            pagoRecibidoPreliminar.DocType = BoRcptTypes.rCustomer;
                            pagoRecibidoPreliminar.CardCode = orden.CardCode;
                            pagoRecibidoPreliminar.DocObjectCode = BoPaymentsObjectType.bopot_IncomingPayments;
                            pagoRecibidoPreliminar.DocTypte = BoRcptTypes.rCustomer;
                            pagoRecibidoPreliminar.CounterReference = orden.DocNum.ToString();
                            pagoRecibidoPreliminar.UserFields.Fields.Item("U_SYP_OVENTA").Value = orden.DocNum.ToString();
                            pagoRecibidoPreliminar.UserFields.Fields.Item("U_SYP_SUC").Value = orden.UserFields.Fields.Item("U_SYP_SUC").Value;

                            //Detalle de documentos - Asignación de Solicitud de Anticipo
                            pagoRecibidoPreliminar.Invoices.InvoiceType = BoRcptInvTypes.it_DownPayment;
                            pagoRecibidoPreliminar.Invoices.DocEntry = docEntryAnticipo;
                            pagoRecibidoPreliminar.Invoices.SumApplied = t.importePago;
                            pagoRecibidoPreliminar.Invoices.UserFields.Fields.Item("U_SYP_FPAGO").Value = t.codigoFp;
                            pagoRecibidoPreliminar.Invoices.Add();

                            //Detalle de pago - Asignación de Medio de Pago

                            switch (t.medioPago)
                            {
                                case "01":
                                {
                                    //1. Cheques
                                    pagoRecibidoPreliminar.CheckAccount = t.cuentaContable;
                                    pagoRecibidoPreliminar.Checks.CountryCode = "EC";
                                    pagoRecibidoPreliminar.Checks.BankCode = t.codigoBanco;
                                    pagoRecibidoPreliminar.Checks.CheckNumber = int.Parse("0" + t.documento);
                                    //POR DEFINIR Y VALIDAR
                                    pagoRecibidoPreliminar.Checks.CheckSum = t.importePago;
                                    pagoRecibidoPreliminar.Checks.DueDate = t.fechaVencimiento;
                                    break;
                                }
                                case "02":
                                {
                                    //2. Transferencias
                                    pagoRecibidoPreliminar.TransferAccount = t.cuentaContable;
                                    pagoRecibidoPreliminar.TransferDate = t.fechaEmision;
                                    pagoRecibidoPreliminar.TransferReference = t.documento;
                                    pagoRecibidoPreliminar.TransferSum = t.importePago;
                                    break;
                                }
                                case "04":
                                {
                                    //3. Fondo en Efectivo
                                    pagoRecibidoPreliminar.CashAccount = t.cuentaContable;
                                    pagoRecibidoPreliminar.CashSum = t.importePago;
                                    break;
                                }

                                case "03":
                                {
                                    //4. Tarjeta de Crédito
                                    Globals.Query = "SELECT \"CreditCard\" FROM OCRC WHERE \"AcctCode\" = '" +
                                                       t.cuentaContable + "' AND \"CardName\" ='" + t.formapago + "'";
                                    Globals.runQuery(Globals.Query);
                                    int codigoTarjeta = Convert.ToInt32("0" + Globals.ORec.Fields.Item(0).Value);

                                    pagoRecibidoPreliminar.CreditCards.CreditCard = codigoTarjeta;
                                    pagoRecibidoPreliminar.CreditCards.CreditAcct = t.cuentaContable;
                                    pagoRecibidoPreliminar.CreditCards.CreditCardNumber = "9999";
                                    pagoRecibidoPreliminar.CreditCards.VoucherNum = "9999";
                                    pagoRecibidoPreliminar.CreditCards.OwnerIdNum = "9999";
                                    pagoRecibidoPreliminar.CreditCards.CardValidUntil = DateTime.Today;
                                    pagoRecibidoPreliminar.CreditCards.CreditSum = t.importePago;
                                    break;
                                }
                                default:
                                {
                                    break;
                                }

                            }

                            int result = pagoRecibidoPreliminar.Add();

                            if (result != 0)
                            {
                                int tempInt;
                                string tempString;
                                Globals.oCompany.GetLastError(out tempInt, out tempString);

                                Globals.SBO_Application.SetStatusBarMessage(
                                    "SYP: Error en crear Pago Preliminar >> [" + tempInt + "] [" + tempString + "]",
                                    BoMessageTime.bmt_Short);
                                //oForm.Close();
                                return;
                            }
                            var docEntryPagoPreliminar = Int32.Parse(Globals.oCompany.GetNewObjectKey());
                            Globals.SBO_Application.SetStatusBarMessage(
                                "SYP: Pago Recibido Preliminar creada con éxito " + docEntryPagoPreliminar,
                                BoMessageTime.bmt_Medium,
                                false);
                        }
                    }

                    Globals.ORec.MoveNext();
                }
                Globals.release(Globals.ORec);
            }
        }

        #endregion

        #region ConsultarFormaPago
        private FormaPago[] ConsultarFormaPago(string code, string fecha)
        {
            FormaPago[] resultado = null;
            try
            {
                Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '', '', '', '" + code + "', '" + fecha + "', '', 'GetFormasDePagoPendientes')";
                Globals.runQuery(Globals.Query);

                if (Globals.ORec.RecordCount > 0)
                {
                    int i = 0;
                    resultado = new FormaPago[Globals.ORec.RecordCount];
                    Globals.ORec.MoveFirst();
                    while (!Globals.ORec.EoF)
                    {
                        resultado[i] = new FormaPago();

                        int lineId = Convert.ToInt32(Globals.ORec.Fields.Item(0).Value);
                        string formapago = Globals.ORec.Fields.Item(1).Value.ToString();
                        DateTime fechaEmision = (DateTime)Globals.ORec.Fields.Item(2).Value;
                        DateTime fechaVencimiento = (DateTime)Globals.ORec.Fields.Item(3).Value;
                        string codigoBanco = Globals.ORec.Fields.Item(4).Value.ToString();
                        string nombreBanco = Globals.ORec.Fields.Item(5).Value.ToString();
                        string documento = Globals.ORec.Fields.Item(6).Value.ToString();
                        double importePago = Convert.ToDouble(Globals.ORec.Fields.Item(7).Value, CultureInfo.InvariantCulture);
                        string cuentaContable = Globals.ORec.Fields.Item(8).Value.ToString();
                        string medioPago = Globals.ORec.Fields.Item(9).Value.ToString();
                        string codigoFp = Globals.ORec.Fields.Item(10).Value.ToString();

                        resultado[i].lineId = lineId;
                        resultado[i].formapago = formapago;
                        resultado[i].fechaEmision = fechaEmision;
                        resultado[i].fechaVencimiento = fechaVencimiento;
                        resultado[i].codigoBanco = codigoBanco;
                        resultado[i].nombreBanco = nombreBanco;
                        resultado[i].documento = documento;
                        resultado[i].importePago = importePago;
                        resultado[i].cuentaContable = cuentaContable;
                        resultado[i].medioPago = medioPago;
                        resultado[i].codigoFp = codigoFp;

                        i++;
                        Globals.ORec.MoveNext();
                    }
                    Globals.release(Globals.ORec);
                }
                return resultado;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, false);
            }
            return resultado;
        }
        #endregion

        #region AsignarDatosDelLote
        public void AsignarDatosDelLote(Form oForm, ItemEvent pVal)
        {
            if (oForm == null) throw new ArgumentNullException("oForm");
            oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);

            Matrix matrixEntrada = (Matrix)oForm.Items.Item(Constantes.MATRIX).Specific;

            if (matrixEntrada.RowCount > 1)
            {
                CellPosition oCell = matrixEntrada.GetCellFocus();
                if (oCell != null)
                {
                    EditText oCantidad = (EditText)matrixEntrada.GetCellSpecific(oCell.ColumnIndex, oCell.rowIndex);
                    if (oCantidad.DataBind.Alias == "Quantity")
                    {
                        string itemCode = ((EditText)matrixEntrada.Columns.Item("1").Cells.Item(oCell.rowIndex).Specific).Value;
                        if (string.IsNullOrEmpty(itemCode)) return;

                        if (!ValidaArticuloManejadoPorSerie(itemCode)) return;

                        string grupoArticulo = ConsultarGrupoArticulo(itemCode);
                        string grupoServicioTercero = General.ObtieneParametro("DMS_GRP_SERV_TERCERO", "U_SYP_ATRIB1");

                        matrixEntrada.Columns.Item(Constantes.CANTIDAD).Cells.Item(1).Click();
                        Globals.SBO_Application.ActivateMenuItem("5896"); // Simula click en detalles de series y lotes 
                        Matrix matrixLote = (Matrix)Globals.SBO_Application.Forms.ActiveForm.Items.Item("3").Specific;

                        //int kilometraje = Convert.ToInt32("0" + matrixEntrada.Columns.Item("U_SYP_KM").Cells.Item(1).Specific.Value);
                        int anio = Convert.ToInt32("0" + ((EditText)matrixEntrada.Columns.Item("U_SYP_ANIO").Cells.Item(1).Specific).Value);
                        string patente = ((EditText)matrixEntrada.Columns.Item("U_SYP_CDO").Cells.Item(1).Specific).Value;
                        string color = ((ComboBox)matrixEntrada.Columns.Item("U_SYP_COLOR").Cells.Item(1).Specific).Value;
                        string lineNum = oCell.rowIndex + "";
                        //try { matrixLote.Columns.Item("U_SYP_KM").Cells.Item(1).Specific.Value = kilometraje; }
                        //catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo kilometraje", BoMessageTime.bmt_Short); }
                        try { ((EditText)matrixLote.Columns.Item("U_SYP_ANIO").Cells.Item(1).Specific).Value = anio.ToString(); }
                        catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo año", BoMessageTime.bmt_Short); }
                        try { ((ComboBox)matrixLote.Columns.Item("U_SYP_COLOR").Cells.Item(1).Specific).Select(color); }
                        catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo color", BoMessageTime.bmt_Short); }

                        ((EditText) matrixLote.Columns.Item(Constantes.ITEMCODE).Cells.Item(1).Specific).Value = patente;

                        if (grupoArticulo == grupoServicioTercero)
                        {
                            Form oFormUdf = Globals.SBO_Application.Forms.Item(oForm.UDFFormUID);
                            try
                            {
                                patente = ((EditText)oFormUdf.Items.Item("U_SYP_PATENTE").Specific).Value;
                                ((EditText)matrixLote.Columns.Item("1").Cells.Item(1).Specific).Value = patente;
                            }
                            catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo Patente", BoMessageTime.bmt_Short); }
                            try
                            {
                                string numeroOt = ((EditText)oFormUdf.Items.Item("U_SYP_NOT").Specific).Value;
                                ((EditText)matrixLote.Columns.Item("54").Cells.Item(1).Specific).Value = "OT:" + numeroOt + "-L:" + lineNum;
                            }
                            catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo Número de OT", BoMessageTime.bmt_Short); }
                        }
                    }
                    else
                    {
                        Globals.SBO_Application.StatusBar.SetText("SYP: Debe posicionar el cursor sobre el campo cantidad en el detalle", BoMessageTime.bmt_Short);
                    }
                }
                else
                {
                    Globals.SBO_Application.StatusBar.SetText("SYP: Debe posicionar el cursor sobre el campo cantidad en el detalle", BoMessageTime.bmt_Short);
                }
            }
        }
        #endregion

        #region ValidaArticuloManejadoPorSerie 
        public bool ValidaArticuloManejadoPorSerie(string itemCode)
        {
            bool resp = false;
            try
            {

                string query = "SELECT A.\"ManSerNum\" FROM \"OITM\" A WHERE A.\"ItemCode\"  = '" + itemCode + "'";
                Globals.Query = query;
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();
                string manSerNum = Globals.ORec.Fields.Item("ManSerNum").Value.ToString();
                Globals.release(Globals.ORec);

                if (manSerNum.Equals("Y"))
                {
                    resp = true;
                }
                else
                {
                    Globals.SBO_Application.StatusBar.SetText("SYP: El articulo no es manejado por serie", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    resp = false;
                }

                return resp;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, false);
            }
            return resp;
        }
        #endregion

        #region ConsultarGrupoArticulo
        public string ConsultarGrupoArticulo(string itemCode)
        {
            string docentry = "";
            try
            {

                string query = "SELECT A.\"ItmsGrpCod\" FROM \"OITM\" A WHERE A.\"ItemCode\"  = '" + itemCode + "'";
                Globals.Query = query;
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();
                while (!Globals.ORec.EoF)
                {
                    docentry = Globals.ORec.Fields.Item(0).Value.ToString();
                    Globals.ORec.MoveNext();
                }
                Globals.release(Globals.ORec);
                return docentry;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, false);
            }
            return docentry;
        }
        #endregion

        #region AsignarDatosDelLoteEntrega
        public void AsignarDatosDelLoteEntrega(Form oForm, ItemEvent pVal)
        {
            if (oForm == null) throw new ArgumentNullException("oForm");
            oForm = Globals.SBO_Application.Forms.Item(pVal.FormUID);

            Matrix matrixEntrega = (Matrix)oForm.Items.Item(Constantes.MATRIX).Specific;

            if (matrixEntrega.RowCount > 1)
            {
                CellPosition oCell = matrixEntrega.GetCellFocus();
                if (oCell != null)
                {
                    EditText oCantidad = (EditText)matrixEntrega.GetCellSpecific(oCell.ColumnIndex, oCell.rowIndex);
                    if (oCantidad.DataBind.Alias == "Quantity")
                    {
                        string itemCode = ((EditText)matrixEntrega.Columns.Item(Constantes.ITEMCODE).Cells.Item(oCell.rowIndex).Specific).Value;
                        if (string.IsNullOrEmpty(itemCode)) return;
                        if (!ValidaArticuloManejadoPorSerie(itemCode)) return;

                        string grupoArticulo = ConsultarGrupoArticulo(itemCode);
                        string grupoServicioTercero = General.ObtieneParametro("DMS_GRP_SERV_TERCERO", "U_SYP_ATRIB1");
                        Form oFormUdf = Globals.SBO_Application.Forms.Item(oForm.UDFFormUID);
                        string vin = "";
                        string numeroOt = "";
                        matrixEntrega.Columns.Item("11").Cells.Item(oCell.rowIndex).Click();
                        Globals.SBO_Application.ActivateMenuItem("5896"); // Simula click en detalles de series y lotes 
                        Globals.SBO_Application.Forms.ActiveForm.Items.Item("10000059").Click();// Click campo filtro

                        Matrix matrixSerie = (Matrix)Globals.SBO_Application.Forms.ActiveForm.Items.Item("5").Specific;

                        if (grupoArticulo == grupoServicioTercero)
                        {
                            try
                            {
                                numeroOt = ((EditText)oFormUdf.Items.Item("U_SYP_NOT").Specific).Value;
                            }
                            catch { Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo Número de OT", BoMessageTime.bmt_Short); }

                            if (numeroOt == "")
                            {
                                Globals.SBO_Application.StatusBar.SetText("SYP: El Número de OT no puede estar vacio", BoMessageTime.bmt_Short);
                                return;
                            }
                            Globals.SBO_Application.SendKeys("OT:" + numeroOt + "{TAB}");

                            if (matrixSerie.VisualRowCount > 0)
                            {
                                matrixSerie.Columns.Item("19").Cells.Item(1).Click();
                                Globals.SBO_Application.Forms.ActiveForm.Items.Item("8").Click();// Click campo [>]
                                Globals.SBO_Application.Forms.ActiveForm.Items.Item("1").Click();// Click OK [>]
                            }
                            else
                            {
                                Globals.SBO_Application.StatusBar.SetText("SYP: Por favor verificar que el numero de serie con la OT exista en la bodega seleccionada", BoMessageTime.bmt_Short);
                            }
                        }
                        else
                        {
                            try
                            {
                                vin = ((EditText)matrixEntrega.Columns.Item("6").Cells.Item(oCell.rowIndex).Specific).Value;
                            }
                            catch
                            {
                                Globals.SBO_Application.StatusBar.SetText("DMS: Debe habilitar el campo VIN", BoMessageTime.bmt_Short);
                            }

                            if (vin == "")
                            {
                                Globals.SBO_Application.StatusBar.SetText("SYP: El VIN no puede estar vacio", BoMessageTime.bmt_Short);
                                return;
                            }
                            Globals.SBO_Application.SendKeys(vin + "{TAB}");

                            if (matrixSerie.VisualRowCount > 0)
                            {
                                try
                                {
                                    matrixSerie.Columns.Item("19").Cells.Item(1).Click();
                                    Globals.SBO_Application.Forms.ActiveForm.Items.Item("8").Click();// Click campo [>]
                                    Globals.SBO_Application.Forms.ActiveForm.Items.Item("1").Click();// Click ACTUALIZAR [>]
                                    Globals.SBO_Application.Forms.ActiveForm.Items.Item("1").Click();// Click OK [>]
                                }
                                catch (Exception)
                                {}
                            }
                            else
                            {
                                Globals.SBO_Application.StatusBar.SetText("SYP: Por favor verificar que el numero de VIN exista en la bodega seleccionada", BoMessageTime.bmt_Short);
                            }
                        }

                    }
                    else
                    {
                        Globals.SBO_Application.StatusBar.SetText("SYP: Debe posicionar el cursor sobre el campo cantidad en el detalle", BoMessageTime.bmt_Short);
                    }
                }
                else
                {
                    Globals.SBO_Application.StatusBar.SetText("SYP: Debe posicionar el cursor sobre el campo cantidad en el detalle", BoMessageTime.bmt_Short);
                }
            }
        }
        #endregion

        //Nuevos metodos
        #region ExternoFacturaInterna 
        public void ExternoFacturaInterna(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");
                bool docCancel = false;

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Documents oInvoice = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oInvoice.GetByKey(Convert.ToInt32(docEntry));

                Globals.runQuery("SELECT 'X', CAST(T1.\"TransId\" AS NVARCHAR(20)) AS \"TransId\" FROM OJDT T1 WHERE T1.\"Ref1\"  = 'FV:" + oInvoice.DocNum + "'");
                string existe = Globals.ORec.Fields.Item(0).Value.ToString();
                string asientoRelacionado = Globals.ORec.Fields.Item(1).Value.ToString();


                if (oInvoice.CancelStatus == CancelStatusEnum.csCancellation) docCancel = true;

                string asientoRelacionadoVenta = "" + oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value;

                if (asientoRelacionadoVenta != asientoRelacionado && existe == "X")
                {
                    oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(asientoRelacionado);
                    oInvoice.Update();
                    Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return;
                }

                if (asientoRelacionadoVenta == asientoRelacionado && existe == "X") return; // Se sale del metodo si ya existe  Asiento



                int transId = oInvoice.TransNum;
                string claseVenta = oInvoice.UserFields.Fields.Item("U_SYP_TVENTA").Value.ToString();
                string sn = oInvoice.CardCode;

                if (!String.IsNullOrEmpty(transId.ToString()) && (claseVenta.Equals("3") || claseVenta.Equals("4") || claseVenta.Equals("5"))
                    && sn.Equals(General.ObtieneParametro("DMS_SN_FACT_INTERNA", "U_SYP_ATRIB1")))
                {
                    try
                    {
                        JournalEntries oJournalOrigin = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//3
                        oJournalOrigin.GetByKey(transId);

                        JournalEntries oJournalNew = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//4

                        oJournalNew.ReferenceDate = oJournalOrigin.ReferenceDate;
                        oJournalNew.Memo = "Extorno Interno " + oJournalOrigin.Memo.Substring(1, 25);
                        oJournalNew.Reference = "FV:" + oInvoice.DocNum;

                        string c1 = General.ObtieneParametro("ASIENTO_C1", "U_SYP_ATRIB1");
                        string c2 = General.ObtieneParametro("ASIENTO_C2", "U_SYP_ATRIB1");

                        for (int i = 0; oJournalOrigin.Lines.Count > i; i++)
                        {

                            oJournalOrigin.Lines.SetCurrentLine(i);
                            bool b1 = oJournalOrigin.Lines.AccountCode.Substring(0, 2).Equals(c1);
                            bool b2 = oJournalOrigin.Lines.AccountCode.Substring(0, 4).Equals(c2);

                            if (!b1)
                            {
                                if (!b2)
                                {
                                    oJournalNew.Lines.ShortName = oJournalOrigin.Lines.ShortName;
                                    oJournalNew.Lines.AccountCode = oJournalOrigin.Lines.AccountCode;
                                    oJournalNew.Lines.Debit = docCancel ? oJournalOrigin.Lines.Debit : oJournalOrigin.Lines.Credit;
                                    oJournalNew.Lines.Credit = docCancel ? oJournalOrigin.Lines.Credit : oJournalOrigin.Lines.Debit;
                                    oJournalNew.Lines.CostingCode = oJournalOrigin.Lines.CostingCode;
                                    oJournalNew.Lines.CostingCode2 = oJournalOrigin.Lines.CostingCode2;
                                    oJournalNew.Lines.CostingCode3 = oJournalOrigin.Lines.CostingCode3;
                                    oJournalNew.Lines.CostingCode4 = oJournalOrigin.Lines.CostingCode4;
                                    oJournalNew.Lines.CostingCode5 = oJournalOrigin.Lines.CostingCode5;

                                    oJournalNew.Lines.Add();
                                }
                            }
                        }

                        int result = oJournalNew.Add();

                        if (result != 0)
                        {
                            int tempInt;
                            string tempString;
                            Globals.oCompany.GetLastError(out tempInt, out tempString);

                            Globals.SBO_Application.SetStatusBarMessage("SYP: Error en crear asiento de extorno de factura interna >> [" + tempInt + "] [" + tempString + "]", BoMessageTime.bmt_Short);
                            //oForm.Close();
                            return;
                        }

                        int transIdNew = Int32.Parse(Globals.oCompany.GetNewObjectKey());
                        Globals.SBO_Application.SetStatusBarMessage("SYP: Asiento de extorno creado con éxito " + transIdNew, BoMessageTime.bmt_Medium, false);
                        oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(transIdNew);
                        oInvoice.Update();
                        Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: Asiento de extorno: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {

                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }
        #endregion

        #region CreaAsientoContablizacionVehiculosUsados
        public void CreaAsientoContablizacionVehiculosUsados(BusinessObjectInfo businessObjectInfo)
        {
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");
                bool docCancel = false;


                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();
                Documents oInvoices = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oInvoices.GetByKey(Convert.ToInt32(docEntry));

                Globals.runQuery("SELECT 'X', CAST(T1.\"TransId\" AS NVARCHAR(20)) AS \"TransId\" FROM OJDT T1 WHERE T1.\"Ref1\"  = 'FV:" + oInvoices.DocNum + "'");
                string existe = Globals.ORec.Fields.Item(0).Value.ToString();
                string asientoRelacionado = Globals.ORec.Fields.Item(1).Value.ToString();


                if (oInvoices.CancelStatus == CancelStatusEnum.csCancellation) docCancel = true;

                string asientoRelacionadoVenta = "" + oInvoices.UserFields.Fields.Item("U_SYP_ASREL").Value;

                if (asientoRelacionadoVenta != asientoRelacionado && existe == "X")
                {
                    oInvoices.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(asientoRelacionado);
                    oInvoices.Update();
                    Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return;
                }

                if (asientoRelacionadoVenta == asientoRelacionado && existe == "X") return; // Se sale del metodo si ya existe  Asiento

                string query = "CALL \"DMS_SP_ASIENTO_GASTOS_VEH_FACT_EXE\" (" + docEntry + ")";
                Globals.runQuery(query);
                System.Data.DataTable dtJrnl = Globals.convertRecordSet(Globals.ORec);

                if (dtJrnl.Select().Length == 0) return;

                JournalEntries jrn = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//7
                #region //Creación de asiento
                jrn.ReferenceDate = oInvoices.DocDate;
                jrn.DueDate = oInvoices.DocDate;
                jrn.TaxDate = oInvoices.DocDate;

                //Lineas del asiento
                foreach (var dr in dtJrnl.Select())
                {
                    jrn.Reference = "FV:" + oInvoices.DocNum;
                    jrn.Reference2 = dr["Ref2"].ToString();
                    jrn.Reference3 = dr["Ref3"].ToString();
                    jrn.Memo = dr["Comentario"] + (docCancel ? " CANCEL" : "");

                    jrn.Lines.AccountCode = dr["Cuenta Contable"].ToString();

                    jrn.Lines.Debit = Convert.ToDouble(dr[docCancel ? " Haber" : "Debe"], CultureInfo.InvariantCulture);
                    jrn.Lines.Credit = Convert.ToDouble(dr[docCancel ? " Debe" : "Haber"], CultureInfo.InvariantCulture);

                    jrn.Lines.CostingCode = dr["CogsOcrCod"].ToString();
                    jrn.Lines.CostingCode2 = dr["CogsOcrCo2"].ToString();
                    jrn.Lines.CostingCode3 = dr["CogsOcrCo3"].ToString();
                    jrn.Lines.CostingCode4 = dr["CogsOcrCo4"].ToString();

                    jrn.Lines.Reference2 = dr["Ref1"].ToString();
                    jrn.Lines.Reference2 = dr["Ref2"].ToString();
                    jrn.Lines.AdditionalReference = dr["Ref3"].ToString();
                    jrn.Lines.Add();
                }
                #endregion //Creación de asiento

                #region //Confirmación de asiento
                if (jrn.Add() != 0)
                {
                    Globals.SBO_Application.StatusBar.SetText("Error al crear Asiento: " + Globals.oCompany.GetLastErrorCode() + " - " + Globals.oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Short);
                    Globals.SBO_Application.MessageBox("Error al crear Asiento: " + Globals.oCompany.GetLastErrorCode() + " - " + Globals.oCompany.GetLastErrorDescription());
                }
                else
                {
                    string trasnIdAsiento = Globals.oCompany.GetNewObjectKey();
                    Globals.SBO_Application.StatusBar.SetText("Se creo el Asiento Nro: " + trasnIdAsiento + " de Reconocimiento de gastos", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    oInvoices.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(trasnIdAsiento);
                    oInvoices.Update();
                    Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                #endregion //Confirmación de asiento
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }
        #endregion

        #region ExternoNcInterna
        public void ExternoNcInterna(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");
                bool docCancel = false;

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Documents oInvoice = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oCreditNotes);
                oInvoice.GetByKey(Convert.ToInt32(docEntry));

                Globals.runQuery("SELECT 'X', CAST(T1.\"TransId\" AS NVARCHAR(20)) AS \"TransId\" FROM OJDT T1 WHERE T1.\"Ref1\"  = 'NC:" + oInvoice.DocNum + "'");
                string existe = Globals.ORec.Fields.Item(0).Value.ToString();
                string asientoRelacionado = Globals.ORec.Fields.Item(1).Value.ToString();


                if (oInvoice.CancelStatus == CancelStatusEnum.csCancellation) docCancel = true;

                string asientoRelacionadoVenta = "" + oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value;

                if (asientoRelacionadoVenta != asientoRelacionado && existe == "X")
                {
                    oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(asientoRelacionado);
                    oInvoice.Update();
                    Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return;
                }

                if (asientoRelacionadoVenta == asientoRelacionado && existe == "X") return; // Se sale del metodo si ya existe  Asiento

                int transId = oInvoice.TransNum;
                string claseVenta = oInvoice.UserFields.Fields.Item("U_SYP_TVENTA").Value.ToString();
                string sn = oInvoice.CardCode;

                if (!String.IsNullOrEmpty(transId.ToString()) && (claseVenta.Equals("3") || claseVenta.Equals("4") || claseVenta.Equals("5"))
                    && sn.Equals(General.ObtieneParametro("DMS_SN_FACT_INTERNA", "U_SYP_ATRIB1")))
                {
                    try
                    {
                        JournalEntries oJournalOrigin = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//6
                        oJournalOrigin.GetByKey(transId);

                        JournalEntries oJournalNew = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);

                        oJournalNew.ReferenceDate = oJournalOrigin.ReferenceDate;
                        oJournalNew.Memo = "Extorno Interno " + oJournalOrigin.Memo.Substring(1, 25);
                        oJournalNew.Reference = "NC:" + oInvoice.DocNum;

                        string c1 = General.ObtieneParametro("ASIENTO_C1", "U_SYP_ATRIB1");
                        string c2 = General.ObtieneParametro("ASIENTO_C2", "U_SYP_ATRIB1");

                        for (int i = 0; oJournalOrigin.Lines.Count > i; i++)
                        {
                            oJournalOrigin.Lines.SetCurrentLine(i);
                            bool b1 = oJournalOrigin.Lines.AccountCode.Substring(0, 2).Equals(c1);
                            bool b2 = oJournalOrigin.Lines.AccountCode.Substring(0, 4).Equals(c2);

                            if (!b1)
                            {
                                if (!b2)
                                {
                                    oJournalNew.Lines.ShortName = oJournalOrigin.Lines.ShortName;
                                    oJournalNew.Lines.AccountCode = oJournalOrigin.Lines.AccountCode;
                                    oJournalNew.Lines.Debit = docCancel ? oJournalOrigin.Lines.Debit : oJournalOrigin.Lines.Credit;
                                    oJournalNew.Lines.Credit = docCancel ? oJournalOrigin.Lines.Credit : oJournalOrigin.Lines.Debit;
                                    oJournalNew.Lines.CostingCode = oJournalOrigin.Lines.CostingCode;
                                    oJournalNew.Lines.CostingCode2 = oJournalOrigin.Lines.CostingCode2;
                                    oJournalNew.Lines.CostingCode3 = oJournalOrigin.Lines.CostingCode3;
                                    oJournalNew.Lines.CostingCode4 = oJournalOrigin.Lines.CostingCode4;
                                    oJournalNew.Lines.CostingCode5 = oJournalOrigin.Lines.CostingCode5;

                                    oJournalNew.Lines.Add();
                                }
                            }
                        }


                        int result = oJournalNew.Add();

                        if (result != 0)
                        {
                            int tempInt;
                            string tempString;
                            Globals.oCompany.GetLastError(out tempInt, out tempString);

                            Globals.SBO_Application.SetStatusBarMessage("SYP: Error en crear asiento de extorno de factura interna >> [" + tempInt + "] [" + tempString + "]", BoMessageTime.bmt_Short);
                            //oForm.Close();
                            return;
                        }

                        int transIdNew = Int32.Parse(Globals.oCompany.GetNewObjectKey());
                        Globals.SBO_Application.SetStatusBarMessage("SYP: Asiento de extorno creado con éxito " + transIdNew, BoMessageTime.bmt_Medium, false);
                        oInvoice.UserFields.Fields.Item("U_SYP_ASREL").Value = Convert.ToInt32(transIdNew);
                        oInvoice.Update();
                        Globals.SBO_Application.StatusBar.SetText("Se actualizo asiento relacionado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    }
                    catch (Exception ex)
                    {
                        var msjError = "SYP: Asiento de extorno: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }
        #endregion

        #region ActualizarEstadoFormaDePago 
        public void ActualizarEstadoFormaDePago(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");
                bool banderaActualizacion = false;

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Payments incPaym = (Payments)Globals.oCompany.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                incPaym.GetByKey(Convert.ToInt32(docEntry));

                if (incPaym.UserFields.Fields.Item("U_SYP_OVENTA").Value == "") return; // No se encuentra enlazado a ninguna orden de venta 

                string docEntryOv = General.ConsultarDocEntry(incPaym.UserFields.Fields.Item("U_SYP_OVENTA").Value.ToString(), "ORDR", incPaym.Series.ToString());

                Documents oOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oOrder.GetByKey(Convert.ToInt32(docEntryOv));
                string codeFormaPago = oOrder.UserFields.Fields.Item("U_SYP_CFPAGO").Value.ToString();
                //string codeVhFormaPago = oOrder.UserFields.Fields.Item("U_SYP_CVHPP").Value.ToString();


                if (!String.IsNullOrEmpty(docEntryOv) && !String.IsNullOrEmpty(codeFormaPago)/* && !String.IsNullOrEmpty(codeVhFormaPago)*/)
                {
                    incPaym.Invoices.SetCurrentLine(0);
                    string linkFormaPago = incPaym.Invoices.UserFields.Fields.Item("U_SYP_FPAGO").Value.ToString();
                    if (linkFormaPago == "") return; // No se encuentra enlazado a ninguna forma de pago 
                    string[] splitLinkFormaPago = linkFormaPago.Split('-');

                    oGeneralService = Globals.oCmpSrv.GetGeneralService(splitLinkFormaPago[0] == "FP" ? "OFORPAGO" : "OVPPAGO");
                    oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                    oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                    string msjError;
                    try
                    {
                        oGeneralParams.SetProperty("Code", oOrder.DocNum.ToString());
                        oGeneralData = oGeneralService.GetByParams(oGeneralParams);

                        if (splitLinkFormaPago[0] == "FP")
                        {
                            oChildren = oGeneralData.Child("SYP_DET_FORPAGO");
                            for (int i = 0; i < oChildren.Count; i++)
                            {

                                if (splitLinkFormaPago[2] == oChildren.Item(i).GetProperty("U_SYP_FORPAGO")
                                    && Convert.ToInt32(splitLinkFormaPago[1]) == Convert.ToInt32(oChildren.Item(i).GetProperty("LineId")))
                                {
                                    if (incPaym.Cancelled == BoYesNoEnum.tNO)
                                    {
                                        oChildren.Item(i).SetProperty("U_SYP_STATUS", "CO");
                                    }
                                    else
                                    {

                                        ActualizarTotalSolicituAnticipo(incPaym.Invoices.DocEntry, Convert.ToDouble(oChildren.Item(i).GetProperty("U_SYP_MONTOFP"), CultureInfo.InvariantCulture));
                                        oChildren.Item(i).SetProperty("U_SYP_MONTOFP", 0.00);
                                        oChildren.Item(i).SetProperty("U_SYP_STATUS", "ES");

                                    }

                                    banderaActualizacion = true;
                                }
                            }

                        }
                        else
                        {

                            oChildren = oGeneralData.Child("SYP_DET_VEHPPAGO");
                            for (int i = 0; i < oChildren.Count; i++)
                            {

                                if (splitLinkFormaPago[2] == oChildren.Item(i).GetProperty("U_SYP_CODITM")
                                    && Convert.ToInt32(splitLinkFormaPago[1]) == Convert.ToInt32(oChildren.Item(i).GetProperty("LineId")))
                                {
                                    if (incPaym.Cancelled == BoYesNoEnum.tNO)
                                    {
                                        oChildren.Item(i).SetProperty("U_SYP_STATUS", "CO");
                                    }
                                    else
                                    {
                                        ActualizarTotalSolicituAnticipo(incPaym.Invoices.DocEntry, Convert.ToDouble(oChildren.Item(i).GetProperty("U_SYP_TASACION"), CultureInfo.InvariantCulture));
                                        oChildren.Item(i).SetProperty("U_SYP_TASACION", 0.00);
                                        oChildren.Item(i).SetProperty("U_SYP_STATUS", "ES");
                                    }
                                    banderaActualizacion = true;
                                }
                            }

                        }

                        if (banderaActualizacion)
                        {
                            oGeneralService.Update(oGeneralData);
                            Globals.SBO_Application.SetStatusBarMessage("SYP: Estado de Forma de pago actualizado con exito :" + linkFormaPago, BoMessageTime.bmt_Medium, false);

                            string docNum = oOrder.DocNum + "";
                            Globals.Query = Globals.Query = "CALL SYP_SP_DMS_GENERAL('', '', '', '', '" + docNum  + "', '', '', 'GetTotalFormaDePagoContabilizado')";
                            Globals.runQuery(Globals.Query);
                            double totalDocumento = Convert.ToDouble(Globals.ORec.Fields.Item(0).Value, CultureInfo.InvariantCulture);

                            oOrder.UserFields.Fields.Item("U_SYP_PAGOCONT").Value = totalDocumento;

                            if (oOrder.Update() != 0)
                            {
                                int tempInt;
                                string tempString;
                                Globals.oCompany.GetLastError(out tempInt, out tempString);

                                Globals.SBO_Application.SetStatusBarMessage("DMS: Error al actualizar total pagado >> [" + tempInt + "] [" + tempString + "]", BoMessageTime.bmt_Short);
                                return;
                            }
                            Globals.SBO_Application.SetStatusBarMessage("DMS: Total pagado actualizado con exito:", BoMessageTime.bmt_Medium, false);
                        }
                        else
                        {
                            msjError = "SYP: No se realizo ninguna actualizacion de formas de pago";
                            throw new Exception(msjError);
                        }

                    }
                    catch (Exception ex)
                    {
                        msjError = "SYP: Actualizacion de estado de Forma de pago" + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }
        #endregion

        #region CreateStockTransferRequest
        public void CreateStockTransferRequest(BusinessObjectInfo businessObjectInfo)
        {
            //Se obtiene el docEntry
            string keyXml = businessObjectInfo.ObjectKey;
            string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

            StockTransfer oStockTransDoc = (StockTransfer)Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer);
            if (oStockTransDoc.GetByKey(Convert.ToInt32(docEntry)))
            {
                //Prefijo de traslado de stock
                Globals.runQuery("SELECT \"DocEntry\" FROM OWTQ T1 WHERE T1.\"U_SYP_TS_ORIGEN\" = 'TS-" + oStockTransDoc.DocNum + "'");

                int docnum = Convert.ToInt32(Globals.ORec.Fields.Item(0).Value);
                if (docnum != 0)
                {
                    Globals.SBO_Application.SetStatusBarMessage("SYP: Solicitud de traslado ya existe, no se procede con la creción automatica: " + Globals.oCompany.GetNewObjectKey(), BoMessageTime.bmt_Short, false);
                    return;
                }
                //Creación de solicitud transferencia por rechazo
                CrearSolicitudTransferencia(oStockTransDoc);
            }
        }
        #endregion

        #region ActualizarTotalSolicituAnticipo
        private void ActualizarTotalSolicituAnticipo(int docEntry, double montoArestar)
        {
            try
            {
                Documents solicitudAnticipo = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oDownPayments);
                solicitudAnticipo.GetByKey(Convert.ToInt32(docEntry));
                solicitudAnticipo.DocTotal = Convert.ToDouble(solicitudAnticipo.DocTotal - montoArestar, CultureInfo.InvariantCulture);
                int result = solicitudAnticipo.Update();

                if (result != 0)
                {
                    int tempInt;
                    string tempString;
                    Globals.oCompany.GetLastError(out tempInt, out tempString);

                    Globals.SBO_Application.SetStatusBarMessage(
                        "SYP: Error al disminuir el Total de la solicitud de anticipo >> [" + tempInt + "] [" + tempString + "]",
                        BoMessageTime.bmt_Short);
                    //oForm.Close();
                    return;
                }
                Globals.SBO_Application.SetStatusBarMessage(
                    "SYP: Total de solicitud de anticipo disminuido con exito - DocEntry:" + docEntry, BoMessageTime.bmt_Medium,
                    false);
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(
                        "SYP Exception: Error al disminuir el Total de la solicitud de anticipo >> [" + ex.Message + "]",
                        BoMessageTime.bmt_Short);
            }
        }
        #endregion

        #region CrearSolicitudTransferencia
        private void CrearSolicitudTransferencia(StockTransfer stockTransDoc)
        {

            #region Declaracion de variables
            StockTransfer oStckTransfReq = (StockTransfer)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInventoryTransferRequest);
            bool flagAddLine = false;
            #endregion Declaracion de variables
            try
            {
                #region Cabecera
                oStckTransfReq.TaxDate = stockTransDoc.TaxDate;
                oStckTransfReq.DocDate = stockTransDoc.DocDate;
                oStckTransfReq.DueDate = stockTransDoc.DocDate;
                if (!string.IsNullOrEmpty(stockTransDoc.CardCode))
                {
                    oStckTransfReq.CardCode = stockTransDoc.CardCode;
                }


                if (string.IsNullOrEmpty(stockTransDoc.UserFields.Fields.Item("U_SYP_BOD_DEST_FINAL").Value.ToString())) return;
                if (stockTransDoc.UserFields.Fields.Item("U_SYP_TIPO_GEN_ST").Value.ToString() != "OT") return; // No aplica si no es una solicitud generada de origen a transito  

                //oStckTransfReq.UserFields.Fields.Item("U_SYP_TIPO_GEN_ST").Value = "TD";
                oStckTransfReq.UserFields.Fields.Item("U_SYP_TS_ORIGEN").Value = "TS-" + stockTransDoc.DocNum;
                oStckTransfReq.UserFields.Fields.Item("U_SYP_ST_ORIGEN").Value = stockTransDoc.UserFields.Fields.Item("U_SYP_ST_ORIGEN").Value;
                #endregion Cabecera
                #region Detalle y Detalle de Lote
                for (int i = 0; i < stockTransDoc.Lines.Count; i++)
                {
                    stockTransDoc.Lines.SetCurrentLine(i);
                    #region detalle campos de sap
                    if (stockTransDoc.UserFields.Fields.Item("U_SYP_BOD_DEST_FINAL").Value.ToString() == stockTransDoc.Lines.WarehouseCode) continue;
                    if (!string.IsNullOrEmpty(stockTransDoc.Lines.ItemCode)) { oStckTransfReq.Lines.ItemCode = stockTransDoc.Lines.ItemCode; }
                    if (Math.Abs(stockTransDoc.Lines.Quantity) > 0) { oStckTransfReq.Lines.Quantity = stockTransDoc.Lines.Quantity; }
                    oStckTransfReq.UserFields.Fields.Item("U_SYP_BOD_DEST_FINAL").Value = stockTransDoc.UserFields.Fields.Item("U_SYP_BOD_DEST_FINAL").Value;
                    oStckTransfReq.UserFields.Fields.Item("U_SYP_BOD_ORIGEN").Value = stockTransDoc.Lines.FromWarehouseCode;
                    oStckTransfReq.Lines.FromWarehouseCode = stockTransDoc.Lines.WarehouseCode;
                    oStckTransfReq.Lines.WarehouseCode = stockTransDoc.UserFields.Fields.Item("U_SYP_BOD_DEST_FINAL").Value.ToString();
                    #endregion detalle campos de sap
                    #region Detalle Serie
                    for (int j = 0; j < stockTransDoc.Lines.SerialNumbers.Count; j++)
                    {
                        stockTransDoc.Lines.SerialNumbers.SetCurrentLine(j);
                        oStckTransfReq.Lines.SerialNumbers.BaseLineNumber = stockTransDoc.Lines.LineNum;
                        oStckTransfReq.Lines.SerialNumbers.InternalSerialNumber = stockTransDoc.Lines.SerialNumbers.InternalSerialNumber;
                        oStckTransfReq.Lines.SerialNumbers.Add();
                    }
                    #endregion Detalle Lote
                    //AGREGAR LINEA 
                    oStckTransfReq.Lines.Add();
                    flagAddLine = true;
                }
                #endregion Detalle y Detalle de Lote
                #region Confirmación de creación
                if (!flagAddLine) return;// Si no se agrego ninguna linea en el detallle no se crea la ST
                var lRetCode = oStckTransfReq.Add();
                if (lRetCode != 0) // if failed
                {
                    int tempInt;
                    string tempString;
                    Globals.oCompany.GetLastError(out tempInt, out tempString);

                    Globals.SBO_Application.SetStatusBarMessage("Error al crear documento en SAP: " + tempString, BoMessageTime.bmt_Short);
                    Globals.SBO_Application.MessageBox("Error al crear documento en SAP: " + tempString);
                }
                else
                {
                    Globals.SBO_Application.SetStatusBarMessage("SYP: Creación exitosa de solicitud de traslado: " + Globals.oCompany.GetNewObjectKey(), BoMessageTime.bmt_Short, false);
                }
                #endregion Confirmación de creación
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage("Error al crear documento de entrega " + ex.Message, BoMessageTime.bmt_Short);
                Globals.SBO_Application.MessageBox("Error al crear documento de entrega " + ex.Message);
            }
        }
        #endregion

        #region CreaAsientoBonos 
        public void CreaAsientoBonos(BusinessObjectInfo businessObjectInfo)
        {
            int lErrorCode = 0;
            string sErrMsg = "";
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Documents oFactura = (Documents) Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oFactura.GetByKey(Convert.ToInt32(docEntry));

                var transid =
                    Convert.ToInt32("0" + Convert.ToString(oFactura.UserFields.Fields.Item("U_SYP_ASBO").Value));

                try
                {
                    int callId = Convert.ToInt32(oFactura.UserFields.Fields.Item("U_SYP_NOT").Value);

                    ServiceCalls oLlamada =
                        (ServiceCalls) Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                    oLlamada.GetByKey(callId);
                    oLlamada.Expenses.SetCurrentLine(oLlamada.Expenses.Count - 1);
                    oLlamada.Expenses.Add();
                    oLlamada.Expenses.DocEntry = Convert.ToInt32(docEntry);
                    oLlamada.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Invoice;

                    if (oLlamada.Update() != 0)
                    {
                        Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                        throw new Exception(sErrMsg);
                    }
                }
                catch (Exception ex)
                {
                }

                Globals.Query = "CALL SYP_ASIENTOBONO_DET (" + docEntry + ")";//1
                Globals.runQuery(Globals.Query);

                if (Globals.ORec.RecordCount > 0 & transid == 0)
                {
                    var oAsiento = (JournalEntries) Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);

                    oAsiento.AutoVAT = BoYesNoEnum.tNO;
                    oAsiento.ReferenceDate = oFactura.DocDate;
                    oAsiento.DueDate = oFactura.DocDate;
                    oAsiento.TaxDate = oFactura.DocDate;
                    oAsiento.ProjectCode = oFactura.Project;
                    oAsiento.Reference = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                    oAsiento.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                    oAsiento.Reference3 = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                    oAsiento.Memo = "DMS: Asiento de Bono DMSBOne.";

                    Globals.ORec.MoveFirst();

                    while (!Globals.ORec.EoF)
                    {
                        oAsiento.Lines.AccountCode = Globals.ORec.Fields.Item("Debe").Value.ToString();

                        oAsiento.Lines.Credit = 0;
                        oAsiento.Lines.Debit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value,CultureInfo.InvariantCulture);
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        oAsiento.Lines.AccountCode = Globals.ORec.Fields.Item("Haber").Value.ToString();
                        oAsiento.Lines.Credit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value.ToString(),CultureInfo.InvariantCulture);
                        oAsiento.Lines.Debit = 0;
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        Globals.ORec.MoveNext();
                    }

                    if (oAsiento.Add() != 0)
                    {
                        Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                        throw new Exception(sErrMsg);
                    }

                    string transidNew = Globals.oCompany.GetNewObjectKey();
                    oFactura.UserFields.Fields.Item("U_SYP_ASBO").Value = transidNew;
                    oFactura.Update();
                    Globals.SBO_Application.SetStatusBarMessage(
                        "DMS: Asiento por bonos generado exitosamente: " + transidNew, BoMessageTime.bmt_Medium, false);

                }
                Globals.release(Globals.ORec);
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                var msjError = "SYP: No se pudo crear asiento por bonos: " + ex.Message;
                throw new Exception(msjError);
            }

        }

        #endregion CreaAsientoBonos

        #region CreaAsientoAprovisionamiento 
        public void CreaAsientoAprovisionamiento(BusinessObjectInfo businessObjectInfo)
        {
            int lErrorCode = 0;
            string sErrMsg = "";
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Documents oFactura = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oFactura.GetByKey(Convert.ToInt32(docEntry));

                var transid =
                    Convert.ToInt32("0" + Convert.ToString(oFactura.UserFields.Fields.Item("U_SYP_ASAP").Value));

                try
                {
                    int callId = Convert.ToInt32(oFactura.UserFields.Fields.Item("U_SYP_ASAP").Value);

                    ServiceCalls oLlamada =
                        (ServiceCalls)Globals.oCompany.GetBusinessObject(BoObjectTypes.oServiceCalls);
                    oLlamada.GetByKey(callId);
                    oLlamada.Expenses.SetCurrentLine(oLlamada.Expenses.Count - 1);
                    oLlamada.Expenses.Add();
                    oLlamada.Expenses.DocEntry = Convert.ToInt32(docEntry);
                    oLlamada.Expenses.DocumentType = BoSvcEpxDocTypes.edt_Invoice;

                    if (oLlamada.Update() != 0)
                    {
                        Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                        throw new Exception(sErrMsg);
                    }
                }
                catch (Exception ex)
                {
                }

                Globals.Query = "CALL SYP_ASIENTOAPROVISIONAMIENTO_DET (" + docEntry + ")";//1
                Globals.runQuery(Globals.Query);

                if (Globals.ORec.RecordCount > 0 & transid == 0)
                {
                    var oAsiento = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);

                    oAsiento.AutoVAT = BoYesNoEnum.tNO;
                    oAsiento.ReferenceDate = oFactura.DocDate;
                    oAsiento.DueDate = oFactura.DocDate;
                    oAsiento.TaxDate = oFactura.DocDate;
                    oAsiento.ProjectCode = oFactura.Project;
                    oAsiento.Reference = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                    oAsiento.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                    oAsiento.Reference3 = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                    oAsiento.Memo = "DMS: Asiento Aprovisionamiento.";

                    Globals.ORec.MoveFirst();

                    while (!Globals.ORec.EoF)
                    {
                        oAsiento.Lines.AccountCode = Globals.ORec.Fields.Item("Debe").Value.ToString();

                        oAsiento.Lines.Credit = 0;
                        oAsiento.Lines.Debit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value, CultureInfo.InvariantCulture);
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        oAsiento.Lines.AccountCode = Globals.ORec.Fields.Item("Haber").Value.ToString();
                        oAsiento.Lines.Credit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value.ToString(), CultureInfo.InvariantCulture);
                        oAsiento.Lines.Debit = 0;
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        Globals.ORec.MoveNext();
                    }

                    if (oAsiento.Add() != 0)
                    {
                        Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                        throw new Exception(sErrMsg);
                    }

                    string transidNew = Globals.oCompany.GetNewObjectKey();
                    oFactura.UserFields.Fields.Item("U_SYP_ASAP").Value = transidNew;
                    oFactura.Update();
                    Globals.SBO_Application.SetStatusBarMessage(
                        "DMS: Asiento por Aprovisionamiento generado exitosamente: " + transidNew, BoMessageTime.bmt_Medium, false);

                }
                Globals.release(Globals.ORec);
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                var msjError = "SYP: No se pudo crear asiento de aprovisionamiento: " + ex.Message;
                throw new Exception(msjError);
            }

        }
        #endregion CreaAsientoAprovisionamiento

        #region AnulaAsientoBonos
        public void AnulaAsientoBonos(BusinessObjectInfo businessObjectInfo, string tipoBono)
        {
            int lErrorCode = 0;
            string sErrMsg = "";

            SAPbobsCOM.ICompany oCompany = Globals.oCompany;
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Documents oNc = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oCreditNotes);
                oNc.GetByKey(Convert.ToInt32(docEntry));

                var transid = Convert.ToInt32("0" + Convert.ToString(oNc.UserFields.Fields.Item(tipoBono).Value));

                if (transid == 0)
                {
                    return; // Sale del metodo si el campo no esta lleno
                }

                Recordset oDet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                oDet.DoQuery("SELECT COUNT(*) FROM OJDT T0 WHERE T0.\"StornoToTr\" = '" + transid + "'");

                var storno = Convert.ToInt32(oDet.Fields.Item(0).Value);

                if (storno == 0)
                {
                    try
                    {
                        /*VerticallSoft 20-08-2019 ANJ inicio*/
                        JournalEntries oAsiento = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//2
                        oAsiento.GetByKey(Convert.ToInt32(transid));

                        oAsiento.ReferenceDate = oNc.DocDate;
                        oAsiento.DueDate = oNc.DocDueDate;
                        oAsiento.TaxDate = oNc.TaxDate;
                        int _code = oAsiento.Update();

                        if (oAsiento.Cancel() != 0)
                        {
                            Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                            throw new Exception(sErrMsg);
                        }

                        oAsiento = null;
                        oCompany.StartTransaction();

                        string transidNew = Globals.oCompany.GetNewObjectKey();

                        JournalEntries oAsientoAnulado = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//3
                        oAsientoAnulado.GetByKey(Convert.ToInt32(transidNew));
                        oAsientoAnulado.ReferenceDate = oNc.DocDate;
                        oAsientoAnulado.DueDate = oNc.DocDueDate;
                        oAsientoAnulado.TaxDate = oNc.TaxDate;
                        _code = 0;
                        _code = oAsientoAnulado.Update();

                        if (_code != 0)
                        {
                            if (oCompany.InTransaction)
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                            Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                            throw new Exception(sErrMsg);
                        }

                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        }

                        Globals.SBO_Application.SetStatusBarMessage("DMS: Asiento cancelado exitosamente: " + transidNew, BoMessageTime.bmt_Medium, false);

                        /*VerticallSoft 20-08-2019 ANJ fin*/
                    }
                    catch (Exception ex)
                    {
                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }

                        var msjError = "SYP: No se pudo cancelar asiento: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText(ex.Message);
            }
        }
        #endregion AnulaAsientoBonos

        #region AnulaAsientoBonosFactura
        public void AnulaAsientoBonosFactura(BusinessObjectInfo businessObjectInfo, string tipoBono)
        {
            int lErrorCode = 0;
            string sErrMsg = "";

            SAPbobsCOM.ICompany oCompany = Globals.oCompany;
            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Documents oFac = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oFac.GetByKey(Convert.ToInt32(docEntry));

                var transid = Convert.ToInt32("0" + Convert.ToString(oFac.UserFields.Fields.Item(tipoBono).Value));

                if (transid == 0)
                {
                    return; // Sale del metodo si el campo no esta lleno
                }

                Recordset oDet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                oDet.DoQuery("SELECT COUNT(*) FROM OJDT T0 WHERE T0.\"StornoToTr\" = '" + transid + "'");

                var storno = Convert.ToInt32(oDet.Fields.Item(0).Value);

                if (storno == 0)
                {
                    try
                    {
                        /*VerticallSoft 20-08-2019 ANJ inicio*/
                        JournalEntries oAsiento = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//2
                        oAsiento.GetByKey(Convert.ToInt32(transid));

                        //oAsiento.ReferenceDate = oFac.DocDate;
                        //oAsiento.DueDate = oFac.DocDueDate;
                        //oAsiento.TaxDate = oFac.TaxDate;
                        oAsiento.Lines.Reference1 = oFac.CardCode;
                        oAsiento.Lines.Reference2 = oFac.DocNum.ToString();
                        oAsiento.Lines.AdditionalReference = oFac.DocEntry.ToString();

                        int _code = oAsiento.Update();

                        if (oAsiento.Cancel() != 0)
                        {
                            Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                            throw new Exception(sErrMsg);
                        }

                        oAsiento = null;
                        oCompany.StartTransaction();

                        string transidNew = Globals.oCompany.GetNewObjectKey();

                        JournalEntries oAsientoAnulado = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);//3
                        oAsientoAnulado.GetByKey(Convert.ToInt32(transidNew));
                        //oAsientoAnulado.ReferenceDate = oFac.DocDate;
                        //oAsientoAnulado.DueDate = oFac.DocDueDate;
                        //oAsientoAnulado.TaxDate = oFac.TaxDate;

                        _code = 0;
                        _code = oAsientoAnulado.Update();

                        if (_code != 0)
                        {
                            if (oCompany.InTransaction)
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                            Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                            throw new Exception(sErrMsg);
                        }

                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        }

                        Globals.SBO_Application.SetStatusBarMessage("DMS: Asiento cancelado exitosamente: " + transidNew, BoMessageTime.bmt_Medium, false);

                        /*VerticallSoft 20-08-2019 ANJ fin*/
                    }
                    catch (Exception ex)
                    {
                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }

                        var msjError = "SYP: No se pudo cancelar asiento: " + ex.Message;
                        throw new Exception(msjError);
                    }
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.StatusBar.SetText(ex.Message);
            }
        }
        #endregion AnulaAsientoBonosFactura

        #region CreaAsientoDispositivo 
        public void CreaAsientoDispositivo(string docEntry)
        {
            int lErrorCode = 0;
            string sErrMsg = "";
            try
            {
                Documents oOrder = (Documents)Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oOrder.GetByKey(Convert.ToInt32(docEntry));

                var transid =
                    Convert.ToInt32("0" + Convert.ToString(oOrder.UserFields.Fields.Item("U_SYP_ASDI").Value));

                string cuentaAsociada = General.ObtieneParametro("DMS_AS_DISP", "U_SYP_ATRIB1");

                Globals.Query = "CALL SYP_ASIENTODISPOSITIVO_DET (" + docEntry + ")";//1
                Globals.runQuery(Globals.Query);

                if (Globals.ORec.RecordCount > 0 & transid == 0)
                {
                    var oAsiento = (JournalEntries)Globals.oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);

                    oAsiento.AutoVAT = BoYesNoEnum.tNO;
                    oAsiento.ReferenceDate = oOrder.DocDate;
                    oAsiento.DueDate = oOrder.DocDate;
                    oAsiento.TaxDate = oOrder.DocDate;
                    oAsiento.ProjectCode = oOrder.Project;
                    oAsiento.Reference = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                    oAsiento.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                    oAsiento.Reference3 = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                    oAsiento.Memo = "DMS: Asiento de Dispositivo.";

                    Globals.ORec.MoveFirst();

                    while (!Globals.ORec.EoF)
                    {
                        //oAsiento.Lines.AccountCode = Globals.ORec.Fields.Item("Debe").Value.ToString();
                        oAsiento.Lines.ShortName = Globals.ORec.Fields.Item("Debe").Value.ToString();

                        oAsiento.Lines.Credit = 0;
                        oAsiento.Lines.Debit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value, CultureInfo.InvariantCulture);
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        oAsiento.Lines.ShortName = Globals.ORec.Fields.Item("Haber").Value.ToString();
                        oAsiento.Lines.AccountCode = cuentaAsociada;
                        oAsiento.Lines.Credit = Convert.ToDouble(Globals.ORec.Fields.Item("Monto").Value.ToString(), CultureInfo.InvariantCulture);
                        oAsiento.Lines.Debit = 0;
                        oAsiento.Lines.ProjectCode = Globals.ORec.Fields.Item("Project").Value.ToString();
                        oAsiento.Lines.VatLine = BoYesNoEnum.tNO;
                        oAsiento.Lines.CostingCode = Globals.ORec.Fields.Item("OcrCode").Value.ToString();
                        oAsiento.Lines.CostingCode2 = Globals.ORec.Fields.Item("OcrCode2").Value.ToString();
                        oAsiento.Lines.CostingCode3 = Globals.ORec.Fields.Item("OcrCode3").Value.ToString();
                        oAsiento.Lines.CostingCode4 = Globals.ORec.Fields.Item("OcrCode4").Value.ToString();
                        oAsiento.Lines.CostingCode5 = Globals.ORec.Fields.Item("OcrCode5").Value.ToString();
                        oAsiento.Lines.Reference1 = Globals.ORec.Fields.Item("Ref1").Value.ToString();
                        oAsiento.Lines.Reference2 = Globals.ORec.Fields.Item("Ref2").Value.ToString();
                        oAsiento.Lines.AdditionalReference = Globals.ORec.Fields.Item("Ref3").Value.ToString();
                        oAsiento.Lines.LineMemo = Globals.ORec.Fields.Item("Memo").Value.ToString();
                        oAsiento.Lines.Add();

                        Globals.ORec.MoveNext();
                    }

                    if (oAsiento.Add() != 0)
                    {
                        Globals.oCompany.GetLastError(out lErrorCode, out sErrMsg);
                        throw new Exception(sErrMsg);
                    }

                    string transidNew = Globals.oCompany.GetNewObjectKey();
                    oOrder.UserFields.Fields.Item("U_SYP_ASDI").Value = transidNew;
                    oOrder.Update();
                    Globals.SBO_Application.SetStatusBarMessage(
                        "DMS: Asiento por Dispositivo generado exitosamente: " + transidNew, BoMessageTime.bmt_Medium, false);

                }
                Globals.release(Globals.ORec);
            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                var msjError = "SYP: No se pudo crear asiento por bonos: " + ex.Message;
                throw new Exception(msjError);
            }

        }

        #endregion CreaAsientoDispositivo

        #region ActualizarDatosEntradaVehiculos

        public void ActualizarDatosEntradaVehiculos(BusinessObjectInfo businessObjectInfo)
        {

            try
            {
                string keyXml = businessObjectInfo.ObjectKey;
                string docEntry = General.ObtenerTagXml(keyXml, "DocEntry");

                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                Documents oOrder = (Documents) Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oOrder.GetByKey(Convert.ToInt32(docEntry));

                double totalFormaPago = Convert.ToDouble(oOrder.UserFields.Fields.Item(Constantes.DISPENTRADA).Value.ToString(),CultureInfo.InvariantCulture);


                oGeneralService = Globals.oCmpSrv.GetGeneralService("OFORPAGO");
                oGeneralData = (GeneralData) oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                string msjError;
                try
                {
                    oGeneralParams.SetProperty("Code", oOrder.DocEntry.ToString());
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                    oGeneralData.SetProperty("U_SYP_TFORPAG", totalFormaPago.ToString(CultureInfo.InvariantCulture));
                    oGeneralService.Update(oGeneralData);

                }
                catch (Exception ex)
                {
                    msjError = "SYP: Actualizacion de estado de Forma de pago" + ex.Message;
                    throw new Exception(msjError);
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }

        #endregion

        #region ActualizarEstadoFormaPago
        public void ActualizarEstadoFormaPago(string codeUdo, string serie, string docentry, List<int> lineId)
        {

            try
            {
                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();

                oGeneralService = Globals.oCmpSrv.GetGeneralService("OFORPAGO");
                oGeneralData = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralParams = (GeneralDataParams)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralDataParams);

                string msjError;
                try
                {
                    oGeneralParams.SetProperty("Code", codeUdo);
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                    oGeneralData.SetProperty("U_SYP_ENVIAR_PAGO", "NO");

                    GeneralData oChild;
                    GeneralDataCollection oChildren;
                    oChildren = oGeneralData.Child("SYP_DET_FORPAGO");

                    foreach (var i in lineId)
                    {
                        int row = i - 1;
                        oChildren.Item(row).SetProperty("U_SYP_STATUS", "CO");
                        oChildren.Item(row).SetProperty("U_SYP_SERIE_DISP", serie);
                        oChildren.Item(row).SetProperty("U_SYP_IDPAG_DISP", docentry);
                    }

                    oGeneralService.Update(oGeneralData);

                }
                catch (Exception ex)
                {
                    msjError = "SYP: Actualizacion de estado de Forma de pago" + ex.Message;
                    throw new Exception(msjError);
                }

            }
            catch (Exception ex) // Se va por excepcion si no encuentra registro
            {
                Globals.SBO_Application.MessageBox(ex.Message);
            }
        }

        #endregion

        #region ConsultarSeries
        public string ConsultarSeries(string idSerie)
        {
            string serie = "";
            try
            {
                string query = "SELECT \"SeriesName\" FROM \"NNM1\" WHERE \"Series\" = '" + idSerie + "'";
                Globals.Query = query;
                Globals.runQuery(Globals.Query);
                Globals.ORec.MoveFirst();
                while (!Globals.ORec.EoF)
                {
                    serie = Globals.ORec.Fields.Item(0).Value.ToString();
                    Globals.ORec.MoveNext();
                }
                Globals.release(Globals.ORec);
                return serie;

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, false);
            }
            return serie;
        }
        #endregion
    }

}
