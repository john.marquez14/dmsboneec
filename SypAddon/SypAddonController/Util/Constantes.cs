﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SypAddonController.Util
{
    public class Constantes
    {
        public static string NombreAddon = "SypDmsBOneEc";
        public static string MsjAddon = "SYP(DmsB1) : (?) ";

        public static string PLT_DATA_TABLE = "DT_0";
        public static string PLT_DATA_TABLE_DET = "DT_1";
        public static string PLT_MATRIX = "mtxMaes";
        public static string PLT_MATRIX_DET = "mtxDet";

        public static string MTX_SERVICIO = "mtxServic";
        public static string MTX_CODUDO = "mtxCodPad";

        public static string DMA_SUCURSAL = "U_SYP_SUC";
        public static string DMA_CLASEVENTA = "U_SYP_TVENTA";
        public static string DMA_NUMOT = "U_SYP_NOT";
        public static string DMA_NUMSINIESTRO = "U_SYP_NSIN";
        public static string DMA_COD_CIASEGURO = "U_SYP_CIASEGCOD";
        public static string DMA_DES_CIASEGURO = "U_SYP_CIASEGDES";
        public static string DMA_LIQUIDADOR = "U_SYP_LIQ";
        public static string DMA_CLIENTE = "U_SYP_CLIENTE";

        public static string LLS_SUCURSAL = "234000009";
        public static string LLS_CLASEVENTA = "44";
        public static string LLS_NUMOT = "12";
        public static string LLS_NUMSINIESTRO = "U_SYP_NSIN";
        public static string LLS_COD_CIASEGURO = "U_SYP_CIASEGCOD";
        public static string LLS_DES_CIASEGURO = "U_SYP_CIASEGDES";
        public static string LLS_LIQUIDADOR = "U_SYP_LIQ";
        public static string LLS_CLIENTE = "14";

        public static string BTN_ACTUALIZAR = "btnUpdtPr";
        public static string BTN_SOLICITUD = "btnPagopr";
        public static string BTN_SERIES = "btnLote";
        public static string PRECIOLISTA = "U_SYP_PLISTA";
        public static string TIPOVENTA = "U_SYP_TPOVENTA";
        public static string ITEMCODE = "1";
        public static string CARDCODE = "4";
        public static string DOCNUM = "8";
        public static string ESTADO = "81";
        public static string SERIE = "88";
        public static string PRECIOUNITARIO = "14";
        public static string MONTOBONO = "U_SYP_MONTO_BONO";
        public static string MATRIX = "38";
        public static string CANTIDAD = "11";
        public static string ITEMNAME = "3";

        public static string DISPTIENE = "U_SYP_TIENEDI";
        public static string DISPPAGO = "U_SYP_FPDISP";
        public static string DISPVALOR = "U_SYP_DISMEN";
        public static string DISPENTRADA = "U_SYP_ENTVEH";
        public static string DISPIDUDO = "U_SYP_CFPAGO";
        public static string DISPASIENTO = "U_SYP_ASDI";
    }
}
