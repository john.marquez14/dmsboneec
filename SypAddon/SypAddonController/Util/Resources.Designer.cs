﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SypAddonController.Util {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SypAddonController.Util.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT LPAD(IFNULL(MAX(&quot;DocEntry&quot;),0) + 1,10,0) AS &quot;Code&quot; FROM &quot;@SYP_CITAS_CAB&quot;.
        /// </summary>
        internal static string _01__Citas_Codigo_Automatico {
            get {
                return ResourceManager.GetString("01. Citas Codigo Automatico", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT NOW() FROM DUMMY.
        /// </summary>
        internal static string _02__Citas_Fecha_Automatica {
            get {
                return ResourceManager.GetString("02. Citas Fecha Automatica", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT &quot;Name&quot; as &quot;Code&quot;, &quot;Name&quot; FROM &quot;@SYP_SUCURSALES&quot;.
        /// </summary>
        internal static string _03__Citas_Sucursales {
            get {
                return ResourceManager.GetString("03. Citas Sucursales", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to declare comSeguro  nvarchar(50);
        ///declare cont  int;
        ///
        ///SELECT  count(*)  into cont  from OSCL Where &quot;callID&quot;=  $[OQUT.&quot;U_SYP_NOT&quot;.0] ;
        ///
        ///
        ///
        ///if (  ifnull(:cont ,0) &gt;  0 ) then
        ///
        ///  SELECT  A.&quot;U_SYP_CIASEGCOD&quot;,B.&quot;CardName&quot; from OSCL A INNER JOIN OCRD B ON A.&quot;U_SYP_CIASEGCOD&quot;   =B.&quot;CardCode&quot;  Where &quot;callID&quot;=  $[OQUT.&quot;U_SYP_NOT&quot;.0]  
        ///     UNION
        ///  SELECT  &quot;customer&quot;,B.&quot;CardName&quot; from OSCL A INNER JOIN OCRD B ON A.&quot;customer&quot;=B.&quot;CardCode&quot;  Where &quot;callID&quot;= $[OQUT.&quot;U_SYP_NOT&quot;.0] ;
        ///else
        ///select $[OQUT.&quot;CardCode [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string _04__CodCliente_con_aseguradora {
            get {
                return ResourceManager.GetString("04. CodCliente con aseguradora", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT &quot;PymntGroup&quot; from &quot;OCTG&quot; WHERE &quot;PymntGroup&quot; not in ( &apos;Vehiculo en parte de pago&apos;,&apos;Otro&apos;,&apos;12 meses&apos;)
        ///and &quot;GroupNum&quot; in (5,14,16,17,38).
        /// </summary>
        internal static string _05__Condicion_Forma_de_Pago_nueva {
            get {
                return ResourceManager.GetString("05. Condicion Forma de Pago nueva", resourceCulture);
            }
        }
    }
}
