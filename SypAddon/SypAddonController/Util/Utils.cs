﻿
using System;
using SAPbouiCOM;
using SypDevs;

namespace SypAddonController.Util
{
    public  class Utils
    {
        public enum TiposMensajes
        {
            SmtWarning,
            SmtSuccess,
            SmtError,
            SmtNone
        }

        public enum TiposMensajesFlotantes
        {
            Confirmation,
            Normal
        }

        public static void MostrarMensajeWarning(string msj)
        {
            Globals.SBO_Application.StatusBar.SetText(msj,BoMessageTime.bmt_Medium,BoStatusBarMessageType.smt_Warning);
        }

        public static void MostrarMensajeSuccess(string msj)
        {
            Globals.SBO_Application.StatusBar.SetText(msj, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
        }

        public static void MostrarMensajeError(string msj)
        {
            Globals.SBO_Application.StatusBar.SetText(msj, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
        }

        public static void MostrarMensajeNone(string msj)
        {
            Globals.SBO_Application.StatusBar.SetText(msj, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_None);
        }

        public static void MostrarMensajeConfirmacion(string msj)
        {
            Globals.SBO_Application.MessageBox(msj, 1, "SI", "NO");
        }

        public static void MostrarMensajeNormal(string msj)
        {
            Globals.SBO_Application.MessageBox(msj);
        }

        public static void MostrarMensaje(string msj, TiposMensajes tipo)
        {
            string msjMostrar = Constantes.MsjAddon.Replace("(?)", msj);
            switch (tipo)
            {
                case TiposMensajes.SmtWarning:
                    MostrarMensajeWarning(msjMostrar);
                    break;
                case TiposMensajes.SmtSuccess:
                    MostrarMensajeSuccess(msjMostrar);
                    break;
                case TiposMensajes.SmtError:
                    MostrarMensajeError(msjMostrar);
                    break;
                case TiposMensajes.SmtNone:
                    MostrarMensajeNone(msjMostrar);
                    break;
            }

        }

        public static void MostrarMensajeFlotante(string msj, TiposMensajesFlotantes  tipo)
        {
            string msjMostrar = Constantes.MsjAddon.Replace("(?)", msj);
            switch (tipo)
            {
                case TiposMensajesFlotantes.Confirmation:
                    MostrarMensajeConfirmacion(msjMostrar);
                    break;
                case TiposMensajesFlotantes.Normal:
                    MostrarMensajeNormal(msjMostrar);
                    break;
            }

        }

    }
}
