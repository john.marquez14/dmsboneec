﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbobsCOM;
using SypDevs;

namespace SypAddonController.Util
{
    public class GlobalsAlt
    {
        public static SAPbobsCOM.Recordset ORec = default(SAPbobsCOM.Recordset);
        public static string Query = null;

        public static SAPbobsCOM.Recordset runQuery(string query)
        {
            try
            {
                ORec = (Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                ORec.DoQuery(query);
                return ORec;
            }
            catch (Exception ex)
            {
                Globals.SBO_Application.SetStatusBarMessage(ex.Message);
                return null;
            }
        }

        public static object release(object objeto)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objeto);
            Query = null;
            GC.Collect();
            return null;
        }
    }
}
