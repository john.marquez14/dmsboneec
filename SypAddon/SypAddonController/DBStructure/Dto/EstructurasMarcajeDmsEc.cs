﻿using SAPbobsCOM;
using sypsoft_utilities.DB_Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SypAddonController.DBStructure.Dto
{
    class EstructurasMarcajeDmsEc
    {
        public static void CrearEstructuras()
        {
            MDStructure mdStructure = new MDStructure();

            #region Declaracion de Campos
            CampoUsuario udo0 = new CampoUsuario("@SYP_MARCAJE", "SYP_USER", "Usuario", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo1 = new CampoUsuario("@SYP_MARCAJE", "SYP_OT", "Ot", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo2 = new CampoUsuario("@SYP_MARCAJE", "SYP_ITEM_NAME", "Servicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo3 = new CampoUsuario("@SYP_MARCAJE", "SYP_PART_VEH", "Parte del Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo4 = new CampoUsuario("@SYP_MARCAJE", "SYP_GRAVEDAD", "Gravedad", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "A", "M", "B" }, new[] { "Alta", "Media", "Baja" }, null, null, 1);
            CampoUsuario udo5 = new CampoUsuario("@SYP_MARCAJE", "SYP_HRS_ESTM", "Horas estimadas", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo6 = new CampoUsuario("@SYP_MARCAJE", "SYP_COD_TEC", "Cod Tecnico", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo7 = new CampoUsuario("@SYP_MARCAJE", "SYP_NOM_TEC", "Tecnico", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo8 = new CampoUsuario("@SYP_MARCAJE", "SYP_ITEM_CODE", "Codigo Servicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udo10 = new CampoUsuario("@SYP_MARCAJE", "SYP_MR_HR_INI", "Hora inicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udo12 = new CampoUsuario("@SYP_MARCAJE", "SYP_DT_HR_INI", "Hora pausa", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udo14 = new CampoUsuario("@SYP_MARCAJE", "SYP_DT_HR_FIN", "Hora continuar", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udo16 = new CampoUsuario("@SYP_MARCAJE", "SYP_MR_HR_FIN", "Hora fin", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo18 = new CampoUsuario("@SYP_MARCAJE", "SYP_OBS", "Observación", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo19 = new CampoUsuario("@SYP_MARCAJE", "SYP_LINEID", "LineId", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udo20 = new CampoUsuario("@SYP_MARCAJE", "SYP_CODE", "codeUdo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);


            CampoUsuario udoDet0 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_USER", "Usuario", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet1 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_OT", "Ot", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet2 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_ITEM_NAME", "Servicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet10 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_MR_HR_INI", "Hora inicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet12 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_DT_HR_INI", "Hora pausa", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet14 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_DT_HR_FIN", "Hora continuar", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet16 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_MR_HR_FIN", "Hora fin", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet18 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_OBS", "Observación", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udoDet19 = new CampoUsuario("@SYP_MARCAJE_DET", "SYP_LINEID", "LineId", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Declaracion de Tablas de Usuario para Udos
            TablaUsuario tablaMarcaje = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_MARCAJE",
                tableName = "Marcaje",
                tableType = BoUTBTableType.bott_NoObject
            };

            TablaUsuario tablaMarcajeDet = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_MARCAJE_DET",
                tableName = "Marcaje Detalle",
                tableType = BoUTBTableType.bott_MasterData
            };
            #endregion

            #region Creacion de Tablas de Usuario
            mdStructure.crearTablaUsuario(tablaMarcaje);
            mdStructure.crearTablaUsuario(tablaMarcajeDet);
            #endregion

            #region Creacion de Campos de Usuario
            mdStructure.crearCampoUsuario(udo0);
            mdStructure.crearCampoUsuario(udo1);
            mdStructure.crearCampoUsuario(udo2);
            mdStructure.crearCampoUsuario(udo3);
            mdStructure.crearCampoUsuario(udo4);
            mdStructure.crearCampoUsuario(udo5);
            mdStructure.crearCampoUsuario(udo6);
            mdStructure.crearCampoUsuario(udo7);
            mdStructure.crearCampoUsuario(udo8);
            mdStructure.crearCampoUsuario(udo18);
            mdStructure.crearCampoUsuario(udo19);
            mdStructure.crearCampoUsuario(udo20);

            mdStructure.crearCampoUsuario(udoDet0);
            mdStructure.crearCampoUsuario(udoDet1);
            mdStructure.crearCampoUsuario(udoDet2);
            mdStructure.crearCampoUsuario(udoDet10);
            mdStructure.crearCampoUsuario(udoDet12);
            mdStructure.crearCampoUsuario(udoDet14);
            mdStructure.crearCampoUsuario(udoDet16);
            mdStructure.crearCampoUsuario(udoDet18);
            mdStructure.crearCampoUsuario(udoDet19);
            #endregion
        }
    }
}
