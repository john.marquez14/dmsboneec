﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbobsCOM;
using SypAddonController.Util;
using SypDevs;

namespace SypAddonController.DBStructure.Dto
{
    public class ParametrosDmsEc
    {
        public static void CreateParametros()
        {
            #region Pedidos Lotes
            try
            {
                Globals.oCmpSrv = Globals.oCompany.GetCompanyService();
                var oGeneralService = Globals.oCmpSrv.GetGeneralService("SYPPARAMGENERAL");
                GeneralData oGeneralDataUdo = (GeneralData)oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_ACTUALIZ_OV");
                    oGeneralDataUdo.SetProperty("Name", "No actualizar bonos OV completada");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "SI");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }catch (Exception){}


                try
                {
                    oGeneralDataUdo.SetProperty("Code", "IVAST");
                    oGeneralDataUdo.SetProperty("Name", "IVA de Solicitud de Traslado");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "IVA_EXE");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_GRP_SERV_TERCERO");
                    oGeneralDataUdo.SetProperty("Name", "Grupo de articulo - servicios tercero");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "112");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_FINACIERA");
                    oGeneralDataUdo.SetProperty("Name", "Financiera por Defecto");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "C1306181635");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_SN_FACT_INTERNA");
                    oGeneralDataUdo.SetProperty("Name", "DMS Socio de negocio Factura");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "C81198400-0");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "ASIENTO_C1");
                    oGeneralDataUdo.SetProperty("Name", "Cuenta 5 para asiento, 2 digitos");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "51");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "ASIENTO_C2");
                    oGeneralDataUdo.SetProperty("Name", "Cuenta 1 para asiento, 4 digitos");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "1109");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_CTA__GST_ACTIVADO");
                    oGeneralDataUdo.SetProperty("Name", "Cuenta de gasto activado");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "1109015");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_CTA_COST_VH_USAD");
                    oGeneralDataUdo.SetProperty("Name", "Cuenta otros costos de venta vh usados");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "5102004");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_ASI_BONO");
                    oGeneralDataUdo.SetProperty("Name", "Define si se crea o no el Asiento de bono");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "Y");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_ASI_APRO");
                    oGeneralDataUdo.SetProperty("Name", "Define si se crea o no el Asiento de Aprovisionamiento");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "Y");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_LISTA_ACTI");
                    oGeneralDataUdo.SetProperty("Name", "Define la lista de usuario definida para las actividades");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "1");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                try
                {
                    oGeneralDataUdo.SetProperty("Code", "DMS_AS_DISP");
                    oGeneralDataUdo.SetProperty("Name", "Cuenta para asiento de dispoditivo");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "2011030103");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                    oGeneralService.Add(oGeneralDataUdo);
                }
                catch (Exception) { }

                //try
                //{
                //    oGeneralDataUdo.SetProperty("Code", "");
                //    oGeneralDataUdo.SetProperty("Name", "");
                //    oGeneralDataUdo.SetProperty("U_SYP_ATRIB1", "");
                //    oGeneralDataUdo.SetProperty("U_SYP_ATRIB2", "");
                //    oGeneralDataUdo.SetProperty("U_SYP_ATRIB3", "DMSEC");
                //    oGeneralService.Add(oGeneralDataUdo);
                //}
                //catch (Exception) { }
            }
            catch (Exception) { }

            #endregion

        }
    }
}
