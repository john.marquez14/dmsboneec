﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sypsoft_utilities.DB_Structure;
using SAPbobsCOM;
using SypDevs;
using SypDevs.DB_Structure;

namespace SypAddonController.DBStructure.Dto
{
    public class EstructurasDmsBOneEc
    {
        //private static readonly string CategoryName = "00. SYP DMSB1";

        public static void CrearEstructuras()
        {
            MDStructure mdStructure = new MDStructure();

            #region Declaracion de Campos
            #region Tarjeta de Equipo
            CampoUsuario oins2 = new CampoUsuario("OINS", "SYP_COLOR", "Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_COLOR", null, null, null, null, 1);
            CampoUsuario oins4 = new CampoUsuario("OINS", "SYP_KMACTUAL", "Km Actual", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins6 = new CampoUsuario("OINS", "SYP_NLLAVE", "N° de Llave", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins8 = new CampoUsuario("OINS", "SYP_CODRADIO", "Código Radio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins10 = new CampoUsuario("OINS", "SYP_NMOTOR", "N° de Motor", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins12 = new CampoUsuario("OINS", "SYP_FCOMPRA", "Fecha de Compra", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins14 = new CampoUsuario("OINS", "SYP_MARCAVH", "Marca Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_MARCAVH", null, null, null, null, 1);
            CampoUsuario oins16 = new CampoUsuario("OINS", "SYP_ANOMODELO", "Año Modelo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 4, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oins18 = new CampoUsuario("OINS", "SYP_CONSCOMPRA", "Concesionario Compra", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 25, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Colores
            CampoUsuario color2 = new CampoUsuario("SYP_COLOR", "SYP_COD_COLOR", "Codigo Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario color4 = new CampoUsuario("SYP_COLOR", "SYP_DESCR_COLOR", "Descripcion Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 25, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario color6 = new CampoUsuario("SYP_COLOR", "SYP_NOM_COLOR", "Nombre de Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Llamada de Servicio
            //CampoUsuario oscl2 = new CampoUsuario("OSCL", "SYP_COLORCONO", "Color Cono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "AMARILLO", "AZUL", "ROJO", "BLANCO", "VERDE" }, new[] { "AMARILLO", "AZUL", "ROJO", "BLANCO", "VERDE" }, null, null, 1);
            CampoUsuario oscl4 = new CampoUsuario("OSCL", "SYP_NSIN", "Número de Siniestro", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 14, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl6 = new CampoUsuario("OSCL", "SYP_KM", "Kilometraje Actual", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario oscl8 = new CampoUsuario("OSCL", "SYP_NESTANQUE", "Nivel Estanque", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "RESERVA", "1/8", "1/4", "3/8", "1/2", "5/8", "3/4", "FULL" }, new[] { "RESERVA", "1/8", "1/4", "3/8", "1/2", "5/8", "3/4", "FULL" }, null, null, 1);
            CampoUsuario oscl10 = new CampoUsuario("OSCL", "SYP_CIASEGCOD", "Cód Cía. Seguros", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl12 = new CampoUsuario("OSCL", "SYP_CIASEGDES", "Desc Cía. Seguros", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario oscl14 = new CampoUsuario("OSCL", "SYP_NCITA", "Número de Cita", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl16 = new CampoUsuario("OSCL", "SYP_LIQ", "Nombre del liquidador", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl18 = new CampoUsuario("OSCL", "SYP_TECNICOS", "Tecnicos", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "UDO", "SLRT", null, null, null, null, 1);
            CampoUsuario oscl20 = new CampoUsuario("OSCL", "SYP_INVTVEH", "Inventario del Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "UDO", "OINVTVEH", null, null, null, null, 1);
            CampoUsuario oscl22 = new CampoUsuario("OSCL", "SYP_INVTDA", "Estado del Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "UDO", "OINVTDA", null, null, null, null, 1);
            CampoUsuario oscl24 = new CampoUsuario("OSCL", "SYP_MEDICION", "Medición de Conformidad", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "BUENO", "REGULAR", "MALO" }, new[] { "BUENO", "REGULAR", "MALO" }, null, null, 1);
            CampoUsuario oscl26 = new CampoUsuario("OSCL", "SYP_DEDUCIBLE", "Deducible ($)", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl28 = new CampoUsuario("OSCL", "SYP_COMENTARIO", "Comentario Interno", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oscl30 = new CampoUsuario("OSCL", "SYP_SUC", "Sucursal", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_SUCURSALES", null, null, null, null, 1);
            #endregion

            #region Udo Asignacion de Tecnicos
            CampoUsuario uctec2 = new CampoUsuario("@SYP_REG_TEC", "SYP_FECHA", "Fecha inicio", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario uctec4 = new CampoUsuario("@SYP_REG_TEC", "SYP_ESTADO", "Estado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "PENDIENTE", "INICIADO", "FINALIZADO" }, new[] { "PENDIENTE", "INICIADO", "FINALIZADO" }, "PENDIENTE", null, 1);

            CampoUsuario udtec2 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_COD_TEC", "Cod Tecnico", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec4 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_NOM_TEC", "Tecnico", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec6 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_ITEM_CODE", "Codigo Tarea", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec8 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_ITEM_NAME", "Tarea", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec10 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_PART_VEH", "Parte del Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec12 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_GRAVEDAD", "Gravedad", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "Alta", "Media", "Baja" }, new[] { "Alta", "Media", "Baja" }, "Media", null, 1);
            CampoUsuario udtec14 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_HRS_ESTM", "Horas estimadas", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec16 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_CONTROL_QA", "Control de calidad", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, new[] { "Aprobado", "No Aprobado" }, new[] { "Aprobado", "No Aprobado" }, null, null, 1);
            CampoUsuario udtec18 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_MR_HR_INI", "Hora inicio", BoFieldTypes.db_Date, BoFldSubTypes.st_Time, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec20 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_MR_FC_INI", "Fecha inicio", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec22 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_DT_HR_INI", "Hora pausa", BoFieldTypes.db_Date, BoFldSubTypes.st_Time, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec24 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_DT_FC_INI", "Fecha pausa", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec26 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_DT_HR_FIN", "Hora continuar", BoFieldTypes.db_Date, BoFldSubTypes.st_Time, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec28 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_DT_FC_FIN", "Fecha continuar", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec30 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_MR_HR_FIN", "Hora fin", BoFieldTypes.db_Date, BoFldSubTypes.st_Time, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec32 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_MR_FC_FIN", "Fecha fin", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udtec34 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_COMENTARIO", "Comentario", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 254, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec36 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_ESTADO", "Acción a Realizar", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, new[] { "ASIGNAR", "REASIGNAR", "QUITAR" }, new[] { "ASIGNAR", "REASIGNAR", "QUITAR" }, "ASIGNAR", null, 1);
            CampoUsuario udtec38 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_AUT", "Autorizado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);
            CampoUsuario udtec40 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_REPUESTO", "Repuesto", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udtec42 = new CampoUsuario("@SYP_DET_REGTEC", "SYP_CANTIDAD", "Cantidad", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);

            #endregion

            #region Udo Estado del vehiculo y Inventario del Vehiculo
            CampoUsuario udveh2 = new CampoUsuario("@SYP_DET_INVTDA", "SYP_DESCD", "Componente", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udveh4 = new CampoUsuario("@SYP_DET_INVTDA", "SYP_CMTRIOD", "Ubicacion", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udveh6 = new CampoUsuario("@SYP_DET_INVTDA", "SYP_ESTADO", "Estado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, new[] { "RAYA", "ABOLLADURA", "CHOQUE", "PIQUETE", "PINTURA_DAÑADA"}, new[] { "RAYA", "ABOLLADURA", "CHOQUE", "PIQUETE", "PINTURA_DAÑADA" }, null, null, 1);

            CampoUsuario udveh8 = new CampoUsuario("@SYP_DET_INVTVEH", "SYP_DESC", "Descripción", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udveh10 = new CampoUsuario("@SYP_DET_INVTVEH", "SYP_OPCION", "Opción", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "Y", "N" }, new[] { "SI", "NO" }, "Y", null, 1);
            CampoUsuario udveh12 = new CampoUsuario("@SYP_DET_INVTVEH", "SYP_CMTRIO", "Comentario", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Maestro de Socios de Negocio
            CampoUsuario ocrd2 = new CampoUsuario("OCRD", "SYP_USR_SAP", "Usuario Sap", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ocrd4 = new CampoUsuario("OCRD", "SYP_BAHIA", "Bahia", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Datos Maestros 
            CampoUsuario ordr2 = new CampoUsuario("ORDR", "SYP_TVENTA", "Clase de Venta", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_TIPOVENTA", null, null, null, null, 1);
            CampoUsuario ordr4 = new CampoUsuario("ORDR", "SYP_SUC", "Sucursal", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_SUCURSALES", null, null, null, null, 1);
            CampoUsuario ordr6 = new CampoUsuario("ORDR", "SYP_NSIN", "Número de Siniestro", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 14, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr8 = new CampoUsuario("ORDR", "SYP_CIASEGCOD", "Cód Cía. Seguros", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr10 = new CampoUsuario("ORDR", "SYP_CIASEGDES", "Desc Cía. Seguros", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr11 = new CampoUsuario("ORDR", "SYP_CLIENTE", "Número del Cliente", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr12 = new CampoUsuario("ORDR", "SYP_LIQ", "Nombre del liquidador", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr14 = new CampoUsuario("ORDR", "SYP_NOT", "Número de OT", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr16 = new CampoUsuario("ORDR", "SYP_FPAGO", "Forma de Pago", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr18 = new CampoUsuario("ORDR", "SYP_SALDO", "Saldo por Documentar", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr20 = new CampoUsuario("ORDR", "SYP_MCON", "Medio de Contacto", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr22 = new CampoUsuario("ORDR", "SYP_PAGOCONT", "Pago Contabilizado", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr24 = new CampoUsuario("ORDR", "SYP_VSEGURO", "Valor seguro", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr26 = new CampoUsuario("ORDR", "SYP_DISP", "Dispositivo", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr28 = new CampoUsuario("ORDR", "SYP_CFPAGO", "Pago Entrada Vehiculo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "UDO", "OFORPAGO", null, null, null, null, 1);
            CampoUsuario ordr30 = new CampoUsuario("ORDR", "SYP_TPOVENTA", "Tipo de Venta", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_TIPODEVENTA", null, null, null, null, 1);
            CampoUsuario ordr32 = new CampoUsuario("ORDR", "SYP_ASEGURADORA", "Aseguradora", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr34 = new CampoUsuario("ORDR", "SYP_COMPLET", "OV Completada", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "1", "0" }, new[] { "SI", "NO" }, "0", null, 1);
            CampoUsuario ordr36 = new CampoUsuario("ORDR", "SYP_ESTAUTOR", "Validar Credito", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "1", "0" }, new[] { "SI", "NO" }, "0", null, 1);
            CampoUsuario ordr38 = new CampoUsuario("ORDR", "SYP_ASREL", "Asiento Relacionado", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 11, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr40 = new CampoUsuario("ORDR", "SYP_TS_ORIGEN", "Transf. Stock Origen", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr42 = new CampoUsuario("ORDR", "SYP_ASBO", "Asiento Bono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr44 = new CampoUsuario("ORDR", "SYP_ASAP", "Asiento Aprovisionamiento", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ordr46 = new CampoUsuario("ORDR", "SYP_ASDI", "Asiento Dispositivo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Detalle de Datos Maestros  
            CampoUsuario dordr2 = new CampoUsuario("RDR1", "SYP_PART_VEH", "Parte del Vehículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dordr4 = new CampoUsuario("RDR1", "SYP_TART", "Tipo Artículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dordr6 = new CampoUsuario("RDR1", "SYP_MONTO_BONO", "Monto Bonos", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dordr8 = new CampoUsuario("RDR1", "SYP_COLOR", "Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_COLOR", null, null, null, null, 1);
            CampoUsuario dordr10 = new CampoUsuario("RDR1", "SYP_PLISTA", "Precio de Lista", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dordr12 = new CampoUsuario("RDR1", "SYP_DSGL_BONO", "Desglose de bonos", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, "UDO", "DBON", null, null, null, null, 1);
            CampoUsuario dordr14 = new CampoUsuario("RDR1", "SYP_ANIO", "Año", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 4, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dordr16 = new CampoUsuario("RDR1", "SYP_CDO", "Placa", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion


            #region Udo de Citas
            CampoUsuario uccita2 = new CampoUsuario("@SYP_CITAS_CAB", "SYP_SUCURSAL", "Sucursal", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario uccita4 = new CampoUsuario("@SYP_CITAS_CAB", "SYP_FECHA", "Fecha", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);

            CampoUsuario udcita2 = new CampoUsuario("@SYP_CITAS_DET", "SYP_SERVICIO", "Servicio", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita4 = new CampoUsuario("@SYP_CITAS_DET", "SYP_HORA", "Hora Cita", BoFieldTypes.db_Date, BoFldSubTypes.st_Time, 1, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita6 = new CampoUsuario("@SYP_CITAS_DET", "SYP_ASUNTO", "Tipo Ot", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, new[] { "Soporte Tecnico", "Mantenimiento", "Garantía", "Flotas", "Colisiones", "Talleres Moviles" }, new[] { "Soporte Tecnico", "Mantenimiento", "Garantía", "Flotas", "Colisiones", "Talleres Moviles" }, null, null, 1);
            CampoUsuario udcita7 = new CampoUsuario("@SYP_CITAS_DET", "SYP_ACTIVIDAD", "Calendarizar", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);
            CampoUsuario udcita8 = new CampoUsuario("@SYP_CITAS_DET", "SYP_NOMBRE", "Nombre", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita10 = new CampoUsuario("@SYP_CITAS_DET", "SYP_MAIL", "Correo Electronico", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita12 = new CampoUsuario("@SYP_CITAS_DET", "SYP_CELULAR", "Telefono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita14 = new CampoUsuario("@SYP_CITAS_DET", "SYP_PATENTE", "Placa", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 7, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita16 = new CampoUsuario("@SYP_CITAS_DET", "SYP_ESTADO", "Asignado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);
            CampoUsuario udcita18 = new CampoUsuario("@SYP_CITAS_DET", "SYP_COMENTARIO", "Observaciones", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 254, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udcita20 = new CampoUsuario("@SYP_CITAS_DET", "SYP_PROCESO", "Estado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, new[] { "Asignado", "Pendiente" }, new[] { "Asignado", "Pendiente" }, "Pendiente", null, 0);
            CampoUsuario udcita22 = new CampoUsuario("@SYP_CITAS_DET", "SYP_OT", "OT", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            CampoUsuario udcita24 = new CampoUsuario("@SYP_CITAS_DET", "SYP_CODE_ACT", "CodeActi", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 0);
            #endregion

            #region Datos de estado del Vehiculo
            CampoUsuario daveh02 = new CampoUsuario("@SYP_INVDANON", "SYP_UBICA", "Ubicacion", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario daveh04 = new CampoUsuario("@SYP_INVDANON", "SYP_COMPONENTE", "Componente", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Udo Pago Entdada Vehiculo/ 
            CampoUsuario ucforp2 = new CampoUsuario("@SYP_FORPAGO", "SYP_TFORPAG", "Total Forma Pago", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp4 = new CampoUsuario("@SYP_FORPAGO", "SYP_CODSN", "Código de Cliente", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp6 = new CampoUsuario("@SYP_FORPAGO", "SYP_NAMESN", "Nombre", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp8 = new CampoUsuario("@SYP_FORPAGO", "SYP_DATE", "Fecha Contabilización", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp10 = new CampoUsuario("@SYP_FORPAGO", "SYP_SALDO", "Saldo", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp12 = new CampoUsuario("@SYP_FORPAGO", "SYP_CHECKL", "Check", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucforp14 = new CampoUsuario("@SYP_FORPAGO", "SYP_ENVIAR_PAGO", "Enviar a Pagar", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);

            CampoUsuario udforp0 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_CUOTA", "Cuota Entrada", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp2 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_FORPAGO", "Foma de Pago", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp3 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_NFORPAGO", "Nombre Forma Pago", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp4 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_FCHEMISION", "Fecha de Emisión", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp6 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_FCHVCTO", "Fecha de Vencimiento", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp8 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_BANCO", "Banco", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp10 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_NRODOC", "Número de Documento", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp12 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_MONTOFP", "Monto", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp14 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_STATUS", "Estado contabilización", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "PC", "ES", "CO" }, new[] { "Por contabilizar", "En Solicitud", "Contabilizado" }, "PC", null, 0);
            CampoUsuario udforp16 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_PLAZOCRED", "Plazo crédito (meses)", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 6, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp18 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_PAGO_DISP", "Paga Dispositivo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);
            CampoUsuario udforp20 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_VALOR_DISP", "Valor dispositivo", BoFieldTypes.db_Float, BoFldSubTypes.st_Price, 16, BoYesNoEnum.tNO, null, null, null, null, "0", null, 1);
            CampoUsuario udforp22 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_SERIE_DISP", "Serie", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udforp24 = new CampoUsuario("@SYP_DET_FORPAGO", "SYP_IDPAG_DISP", "Número de transacción", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            
            #endregion

            #region Tabla tipo de Venta
            CampoUsuario tivent2 = new CampoUsuario("@SYP_TIPODEVENTA", "SYP_SINSTOCK", "Sin Stock", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Udo Bono por Modelo y Finan
            CampoUsuario ucbomo2 = new CampoUsuario("@SYP_BPMF", "SYP_CODMOD", "Código Modelo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 60, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucbomo4 = new CampoUsuario("@SYP_BPMF", "SYP_DESCMOD", "Descripción Modelo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbomo2 = new CampoUsuario("@SYP_BPMF_DET", "SYP_CODEB", "Código Bono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 60, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbomo4 = new CampoUsuario("@SYP_BPMF_DET", "SYP_DESCB", "Descripción Bono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbomo6 = new CampoUsuario("@SYP_BPMF_DET", "SYP_MONTO", "Monto sin IVA", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbomo8 = new CampoUsuario("@SYP_BPMF_DET", "SYP_FECHINI", "Fecha Inicio", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbomo10 = new CampoUsuario("@SYP_BPMF_DET", "SYP_FECFIN", "Fecha Fin", BoFieldTypes.db_Date, BoFldSubTypes.st_None, 8, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbomo8 = new CampoUsuario("@SYP_BPMF_DET", "SYP_MONTOSINIVA", "Monto sin IVA", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbomo10 = new CampoUsuario("@SYP_BPMF_DET", "SYP_FINAN", "Financiera Cod.", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbomo12 = new CampoUsuario("@SYP_BPMF_DET", "SYP_FINAN_DESC", "Financiera Desc.", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbomo14 = new CampoUsuario("@SYP_BPMF_DET", "SYP_PTJEMAX", "Porcentaje Máximo", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbomo16 = new CampoUsuario("@SYP_BPMF_DET", "SYP_PTJEMIN", "Porcentaje Min", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Bonos
            CampoUsuario bono2 = new CampoUsuario("@SYP_BONOS", "SYP_RECUP", "Recuperable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "0", "1"}, new[] { "NO", "SI" }, null, null, 1);
            CampoUsuario bono4 = new CampoUsuario("@SYP_BONOS", "SYP_DESCCTAH", "Desc Cta Haber", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario bono6 = new CampoUsuario("@SYP_BONOS", "SYP_TIPO_BONO", "Tipo bonificación", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "BON" }, new[] { "BONO" }, "BON", null, 1);
            CampoUsuario bono8 = new CampoUsuario("@SYP_BONOS", "SYP_CONTADO", "Contado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "0", "1" }, new[] { "NO", "SI" }, null, null, 1);
            CampoUsuario bono10 = new CampoUsuario("@SYP_BONOS", "SYP_CREDITO", "Credito", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, new[] { "0", "1" }, new[] { "NO", "SI" }, null, null, 1);
            CampoUsuario bono12 = new CampoUsuario("@SYP_BONOS", "SYP_CODCTAD", "Cod Cta Debe", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 12, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario bono14 = new CampoUsuario("@SYP_BONOS", "SYP_DESCTAD", "Desc Cta Debe", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario bono16 = new CampoUsuario("@SYP_BONOS", "SYP_CODCTAH", "Cod Cta Haber", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 12, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Relacion forma de pago cuenta
            CampoUsuario pcta2 = new CampoUsuario("@SYP_PAGOCTA", "SYP_CTA", "Cta Contable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Relacion grupo de articulos
            CampoUsuario regr2 = new CampoUsuario("@SYP_RELGRUPOART", "SYP_CVENTA", "Clase de Venta", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_TIPOVENTA", null, null, null, null, 1);
            CampoUsuario regr4 = new CampoUsuario("@SYP_RELGRUPOART", "SYP_GRART", "Grupo Artículo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario regr6 = new CampoUsuario("@SYP_RELGRUPOART", "SYP_ARTICULO", "Articulo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario regr8 = new CampoUsuario("@SYP_RELGRUPOART", "SYP_OTRACAT", "Otra Categoría", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Udo Desglose de Bonos
            CampoUsuario ucbono2 = new CampoUsuario("@SYP_DBON", "SYP_CODMOD", "Código Modelo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 60, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario ucbono4 = new CampoUsuario("@SYP_DBON", "SYP_DESCMOD", "Descripción Modelo", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario ucbono6 = new CampoUsuario("@SYP_DBON", "SYP_FINAN", "Financiera", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_FINAN", null, null, null, null, 1);
            //CampoUsuario ucbono8 = new CampoUsuario("@SYP_DBON", "SYP_PTJEAPLI", "Porcentaje Aplicado", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario ucbono10 = new CampoUsuario("@SYP_DBON", "SYP_DEALERNET", "Dealer Net Aplicado", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario ucbono12 = new CampoUsuario("@SYP_DBON", "SYP_MARGENAP", "Margen Aplicado", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbono4 = new CampoUsuario("@SYP_DBON_DET", "SYP_CODEB", "Código Bono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 60, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbono6 = new CampoUsuario("@SYP_DBON_DET", "SYP_DESCB", "Descripción Bono", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario udbono8 = new CampoUsuario("@SYP_DBON_DET", "SYP_MONTO", "Monto sin IVA", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbono10 = new CampoUsuario("@SYP_DBON_DET", "SYP_MONTOSINIVA", "Monto sin IVA", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            //CampoUsuario udbono12 = new CampoUsuario("@SYP_DBON_DET", "SYP_RECUP", "Recuperable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, BoYesNoEnum.tNO, null, null, new[] { "SI", "NO" }, new[] { "SI", "NO" }, "NO", null, 1);

            #endregion

            #region Lotes
            CampoUsuario obtn2 = new CampoUsuario("OBTN", "SYP_PCOSTO", "Costo Estimado", BoFieldTypes.db_Float, BoFldSubTypes.st_Sum, 16, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario obtn4 = new CampoUsuario("OBTN", "SYP_CONDICION", "Condicion", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 17, BoYesNoEnum.tNO, null, null, new[] { "10", "11", "12" }, new[] { "Propios", "Propios - Transformacion", "Test Car" }, "10", null, 1);
            CampoUsuario obtn6 = new CampoUsuario("OBTN", "SYP_CDO", "Placa", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario obtn8 = new CampoUsuario("OBTN", "SYP_COLOR", "Color", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, "Table", "SYP_COLOR", null, null, null, null, 1);
            CampoUsuario obtn10 = new CampoUsuario("OBTN", "SYP_ESTITM", "Estado", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20, BoYesNoEnum.tNO, null, null, new[] { "NO DISPONIBLE", "DISPONIBLE", "RESERVADO" }, new[] { "NO DISPONIBLE", "DISPONIBLE", "RESERVADO" }, "DISPONIBLE", null, 1);
            CampoUsuario obtn12 = new CampoUsuario("OBTN", "SYP_ANIO", "Año", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 4, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion

            #region Detalle Pagos
            CampoUsuario ovpm2 = new CampoUsuario("OVPM", "SYP_OVENTA", "Orden de Venta", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario dvpm2 = new CampoUsuario("VPM2", "SYP_FPAGO", "Forma pago", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 200, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion 

            #region Condiciones de pago
            CampoUsuario ictg2 = new CampoUsuario("OCTG", "SYP_PRECIB", "Forma de Pago", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion 


            #region Grupos de articulos
            CampoUsuario oitb2 = new CampoUsuario("OITB", "SYP_CTA_BONOS", "Cuenta Bonos", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            CampoUsuario oitb4 = new CampoUsuario("OITB", "SYP_CTA_APROVI", "Cuenta de Aprovis.", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15, BoYesNoEnum.tNO, null, null, null, null, null, null, 1);
            #endregion 

            #endregion

            #region Declaracion de Tablas de Usuario para Udos

            List<CampoUsuario> listaCamposUsuarioCabeceraTecnicos = new List<CampoUsuario>
            {
                uctec2, uctec4
            };

            List<CampoUsuario> listaCamposUsuarioDetalleTecnicos = new List<CampoUsuario>
            {
                udtec2,udtec4,udtec6,udtec8,udtec10,udtec12,udtec14,udtec16,udtec18,udtec20,udtec22,udtec24,udtec26,udtec28,udtec30,udtec32,udtec34,udtec36,udtec38,udtec40,udtec42
            };

            List<CampoUsuario> listaEstadoVehiculo = new List<CampoUsuario>
            {
            };

            List<CampoUsuario> listaDetalleEstadoVehiculo = new List<CampoUsuario>
            {
                udveh2, udveh4, udveh6
            };

            List<CampoUsuario> listaInventarioVehiculo = new List<CampoUsuario>
            {
            };

            List<CampoUsuario> listaDetalleInventarioVehiculo = new List<CampoUsuario>
            {
                udveh8, udveh10, udveh12
            };

            List<CampoUsuario> listaCitas = new List<CampoUsuario>
            {
                uccita2, uccita4
            };

            List<CampoUsuario> listaDetalleCitas = new List<CampoUsuario>
            {
                udcita2, udcita4, udcita6, udcita7, udcita8, udcita10, udcita12, udcita14, udcita16, udcita18, udcita20, udcita22
            };

            List<CampoUsuario> listaFormaPagos = new List<CampoUsuario>
            {
                ucforp2, ucforp4, ucforp6, ucforp8, ucforp10, ucforp14
            };

            List<CampoUsuario> listaDetalleFormaPagos = new List<CampoUsuario>
            {
                udforp0, udforp2, udforp3, udforp4, udforp6, udforp8, udforp10, udforp12, udforp14, udforp18, udforp20, udforp22, udforp24
            };

            List<CampoUsuario> listaBonoModelo = new List<CampoUsuario>
            {
                ucbomo2, ucbomo4
            };

            List<CampoUsuario> listaDetalleBonoModelo = new List<CampoUsuario>
            {
                udbomo2, udbomo4, udbomo6, udbomo8, udbomo10
            };

            List<CampoUsuario> listaDesgloseBono = new List<CampoUsuario>
            {
                ucbono2, ucbono4
            };

            List<CampoUsuario> listaDetalleDesgloseBono = new List<CampoUsuario>
            {
                udbono4, udbono6, udbono8//, udbono10, udbono12
            };

            TablaUsuario tablaCaberaTecnicos = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_REG_TEC",
                tableName = "Registro de Técnicos",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaCamposUsuarioCabeceraTecnicos
            };

            TablaUsuario tablaDetalleTecnicos = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DET_REGTEC",
                tableName = "Det_Tecnicos",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaCamposUsuarioDetalleTecnicos
            };

            TablaUsuario tablaCaberaEstadoVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_INVTDA",
                tableName = "Estado del Vehículo",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaEstadoVehiculo
            };

            TablaUsuario tablaDetalleEstadoVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DET_INVTDA",
                tableName = "Detalle Estado del Vehículo",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaDetalleEstadoVehiculo
            };

            TablaUsuario tablaCaberaInventarioVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_INVTVEH",
                tableName = "Inventario de Vehículo",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaEstadoVehiculo
            };

            TablaUsuario tablaDetalleInventarioVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DET_INVTVEH",
                tableName = "Detalle Inventario Vehículo",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaDetalleInventarioVehiculo
            };

            TablaUsuario tablaCaberaCitas = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_CITAS_CAB",
                tableName = "Agendamiento de Citas",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaCitas
            };

            TablaUsuario tablaDetalleCitas = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_CITAS_DET",
                tableName = "Agendamiento de Citas Detalle",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaDetalleCitas
            };

            TablaUsuario tablaCaberaFormaPago = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_FORPAGO",
                tableName = "FP Entrada Del Vehiculo",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaFormaPagos
            };

            TablaUsuario tablaDetalleFormaPago = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DET_FORPAGO",
                tableName = "Detalle Forma de Pago",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaDetalleFormaPagos
            };

            TablaUsuario tablaCabeceraBonoModelo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_BPMF",
                tableName = "Bono por Modelo y Finan",
                tableType = BoUTBTableType.bott_Document,
                camposUsuario = listaBonoModelo
            };

            TablaUsuario tablaDetalleBonoModelo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_BPMF_DET",
                tableName = "Bono por Modelo y Finan Det",
                tableType = BoUTBTableType.bott_DocumentLines,
                camposUsuario = listaDetalleBonoModelo
            };

            TablaUsuario tablaCabeceraDesgloseBono = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DBON",
                tableName = "Desglose de Bonos",
                tableType = BoUTBTableType.bott_MasterData,
                camposUsuario = listaDesgloseBono
            };

            TablaUsuario tablaDetalleDesgloseBono = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_DBON_DET",
                tableName = "Desg. bonos det.",
                tableType = BoUTBTableType.bott_MasterDataLines,
                camposUsuario = listaDetalleDesgloseBono
            };
            #endregion

            /////////////////////////////////// 

            #region Declaracion de Tablas de Usuario
            TablaUsuario tablaColores = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_COLOR",
                tableName = "Color",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaMarcaVehiculos = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_MARCAVH",
                tableName = "Marca Vehículo",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaMotivosPausas = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_MOTIVOS_PAUSA",
                tableName = "Motivos Pausas",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaClasesVenta = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_TIPOVENTA",
                tableName = "Clase de Venta",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaSucursales = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_SUCURSALES",
                tableName = "Sucursales",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario datosEstadoVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_INVDANON",
                tableName = "Inventario Estado del Veh",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario datosInventarioVehiculo = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_TBL_INVD",
                tableName = "Tabla Inventario Recepción",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaTiposVenta = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_TIPODEVENTA",
                tableName = "Tipo de Venta",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaPagoCuenta = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_PAGOCTA",
                tableName = "Relacion forma Pago-Cta",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaRelacionGrupoArt = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_RELGRUPOART",
                tableName = "Relación Grupo Articulos",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaBonos = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_BONOS",
                tableName = "Bonos",
                tableType = BoUTBTableType.bott_NoObject,
            };

            TablaUsuario tablaFinanciamiento = new TablaUsuario
            {
                objectId = "",
                sTableId = "SYP_FINAN",
                tableName = "Financieras",
                tableType = BoUTBTableType.bott_NoObject,
            };
            #endregion

            #region Creacion de Tablas de Usuario
            mdStructure.crearTablaUsuario(tablaCaberaTecnicos);
            mdStructure.crearTablaUsuario(tablaDetalleTecnicos);
            mdStructure.crearTablaUsuario(tablaCaberaEstadoVehiculo);
            mdStructure.crearTablaUsuario(tablaDetalleEstadoVehiculo);
            mdStructure.crearTablaUsuario(tablaCaberaInventarioVehiculo);
            mdStructure.crearTablaUsuario(tablaDetalleInventarioVehiculo);
            mdStructure.crearTablaUsuario(tablaCaberaCitas);
            mdStructure.crearTablaUsuario(tablaDetalleCitas);
            mdStructure.crearTablaUsuario(tablaColores);
            //mdStructure.crearTablaUsuario(tablaMarcaVehiculos);
            mdStructure.crearTablaUsuario(tablaMotivosPausas);
            mdStructure.crearTablaUsuario(tablaClasesVenta);
            mdStructure.crearTablaUsuario(tablaSucursales);
            mdStructure.crearTablaUsuario(datosEstadoVehiculo);
            mdStructure.crearTablaUsuario(datosInventarioVehiculo);
            mdStructure.crearTablaUsuario(tablaTiposVenta);
            mdStructure.crearTablaUsuario(tablaPagoCuenta);
            mdStructure.crearTablaUsuario(tablaRelacionGrupoArt);
            mdStructure.crearTablaUsuario(tablaBonos);
            mdStructure.crearTablaUsuario(tablaFinanciamiento);
            #endregion

            #region Creacion de Campos de Usuario
            //mdStructure.crearCampoUsuario(oins2);
            //mdStructure.crearCampoUsuario(oins4);
            //mdStructure.crearCampoUsuario(oins6);
            //mdStructure.crearCampoUsuario(oins8);
            //mdStructure.crearCampoUsuario(oins10);
            //mdStructure.crearCampoUsuario(oins12);
            //mdStructure.crearCampoUsuario(oins14);
            //mdStructure.crearCampoUsuario(oins16);
            //mdStructure.crearCampoUsuario(oins18);

            //mdStructure.crearCampoUsuario(color2);
            //mdStructure.crearCampoUsuario(color4);
            //mdStructure.crearCampoUsuario(color6);

            //mdStructure.crearCampoUsuario(oscl2);
            mdStructure.crearCampoUsuario(oscl4);
            mdStructure.crearCampoUsuario(oscl6);
            //mdStructure.crearCampoUsuario(oscl8);
            mdStructure.crearCampoUsuario(oscl10);
            mdStructure.crearCampoUsuario(oscl12);
            //mdStructure.crearCampoUsuario(oscl14);
            mdStructure.crearCampoUsuario(oscl16);
            mdStructure.crearCampoUsuario(oscl24);
            mdStructure.crearCampoUsuario(oscl26);
            mdStructure.crearCampoUsuario(oscl28);
            mdStructure.crearCampoUsuario(oscl30);

            mdStructure.crearCampoUsuario(ocrd2);
            mdStructure.crearCampoUsuario(ocrd4);

            mdStructure.crearCampoUsuario(ordr6);
            mdStructure.crearCampoUsuario(ordr8);
            mdStructure.crearCampoUsuario(ordr10);
            mdStructure.crearCampoUsuario(ordr11);
            mdStructure.crearCampoUsuario(ordr12);
            mdStructure.crearCampoUsuario(ordr14);
            mdStructure.crearCampoUsuario(ordr16);
            mdStructure.crearCampoUsuario(ordr18);
            mdStructure.crearCampoUsuario(ordr20);
            mdStructure.crearCampoUsuario(ordr22);
            mdStructure.crearCampoUsuario(ordr24);
            mdStructure.crearCampoUsuario(ordr26);
            mdStructure.crearCampoUsuario(ordr30);
            mdStructure.crearCampoUsuario(ordr32);
            mdStructure.crearCampoUsuario(ordr34);
            mdStructure.crearCampoUsuario(ordr36);
            mdStructure.crearCampoUsuario(ordr38);
            mdStructure.crearCampoUsuario(ordr40);
            mdStructure.crearCampoUsuario(ordr42);
            mdStructure.crearCampoUsuario(ordr44);
            mdStructure.crearCampoUsuario(ordr46);

            mdStructure.crearCampoUsuario(dordr2);
            mdStructure.crearCampoUsuario(dordr4);
            mdStructure.crearCampoUsuario(dordr6);
            mdStructure.crearCampoUsuario(dordr8);
            mdStructure.crearCampoUsuario(dordr10);
            mdStructure.crearCampoUsuario(dordr14);
            mdStructure.crearCampoUsuario(dordr16);

            mdStructure.crearCampoUsuario(daveh02);
            mdStructure.crearCampoUsuario(daveh04);

            mdStructure.crearCampoUsuario(tivent2);

            mdStructure.crearCampoUsuario(bono2);
            mdStructure.crearCampoUsuario(bono4);
            mdStructure.crearCampoUsuario(bono6);
            mdStructure.crearCampoUsuario(bono8);
            mdStructure.crearCampoUsuario(bono10);
            mdStructure.crearCampoUsuario(bono12);
            mdStructure.crearCampoUsuario(bono14);
            mdStructure.crearCampoUsuario(bono16);

            mdStructure.crearCampoUsuario(regr2);
            mdStructure.crearCampoUsuario(regr4);
            mdStructure.crearCampoUsuario(regr6);
            mdStructure.crearCampoUsuario(regr8);

            mdStructure.crearCampoUsuario(pcta2);

            mdStructure.crearCampoUsuario(obtn2);
            mdStructure.crearCampoUsuario(obtn4);
            mdStructure.crearCampoUsuario(obtn6);
            mdStructure.crearCampoUsuario(obtn8);
            mdStructure.crearCampoUsuario(obtn10);
            mdStructure.crearCampoUsuario(obtn12);

            mdStructure.crearCampoUsuario(ovpm2);
            mdStructure.crearCampoUsuario(dvpm2);

            mdStructure.crearCampoUsuario(ictg2);

            mdStructure.crearCampoUsuario(oitb2);
            mdStructure.crearCampoUsuario(oitb4);
            #endregion

            #region Declaracion de Udos

            DocumentUdo udoRegistroTecnicos = new DocumentUdo
            {
                menuPadreId = 3584,
                tablaCabecera = tablaCaberaTecnicos,
                tablaDetalle = tablaDetalleTecnicos,
                objectId = "SLRT",
                menuId = "SLRT",
                rebuildEnhancedForm = false,
                formName = "Numero OT",
                tipoForm = "C"
            };

            DocumentUdo udoEstadoVehiculo = new DocumentUdo
            {
                menuPadreId = 3584,
                tablaCabecera = tablaCaberaEstadoVehiculo,
                tablaDetalle = tablaDetalleEstadoVehiculo,
                objectId = "OINVTDA",
                menuId = "OINVTDA",
                rebuildEnhancedForm = false,
                formName = "Numero OT",
                tipoForm = "C"
            };

            DocumentUdo udoInventarioVehiculo = new DocumentUdo
            {
                menuPadreId = 3584,
                tablaCabecera = tablaCaberaInventarioVehiculo,
                tablaDetalle = tablaDetalleInventarioVehiculo,
                objectId = "OINVTVEH",
                menuId = "OINVTVEH",
                rebuildEnhancedForm = false,
                formName = "Numero OT",
                tipoForm = "C"
            };

            DocumentUdo udoCitas = new DocumentUdo
            {
                menuPadreId = 3584,
                tablaCabecera = tablaCaberaCitas,
                tablaDetalle = tablaDetalleCitas,
                objectId = "CIJM",
                menuId = "CIJM",
                rebuildEnhancedForm = false,
                formName = "",
                tipoForm = "C"
            };

            DocumentUdo udoFormaPAgo = new DocumentUdo
            {
                menuPadreId = 2048,
                tablaCabecera = tablaCaberaFormaPago,
                tablaDetalle = tablaDetalleFormaPago,
                objectId = "OFORPAGO",
                menuId = "OFORPAGO",
                rebuildEnhancedForm = false,
                formName = "",
                tipoForm = "C"
            };

            DocumentUdo udoBonoModelo = new DocumentUdo
            {
                menuPadreId = 43525,
                tablaCabecera = tablaCabeceraBonoModelo,
                tablaDetalle = tablaDetalleBonoModelo,
                objectId = "BPMF",
                menuId = "BPMF",
                rebuildEnhancedForm = false,
                formName = "",
                tipoForm = "C"
            };

            DocumentUdo udoDesgloseBono = new DocumentUdo
            {
                menuPadreId = 2048,
                tablaCabecera = tablaCabeceraDesgloseBono,
                tablaDetalle = tablaDetalleDesgloseBono,
                objectId = "DBON",
                menuId = "DBON",
                rebuildEnhancedForm = false,
                formName = "",
                tipoForm = "C"
            };
            #endregion

            #region Creacion de Udos
            mdStructure.RegistrarUdos(udoRegistroTecnicos, BoUDOObjType.boud_MasterData);//
            mdStructure.RegistrarUdos(udoEstadoVehiculo, BoUDOObjType.boud_MasterData);//
            mdStructure.RegistrarUdos(udoInventarioVehiculo, BoUDOObjType.boud_MasterData);//
            mdStructure.RegistrarUdos(udoCitas, BoUDOObjType.boud_MasterData);//
            mdStructure.RegistrarUdos(udoFormaPAgo, BoUDOObjType.boud_MasterData);//
            mdStructure.RegistrarUdos(udoBonoModelo, BoUDOObjType.boud_Document);//
            mdStructure.RegistrarUdos(udoDesgloseBono, BoUDOObjType.boud_MasterData);
            #endregion

            #region Creacion de Campos de Usuario atados a UDOS
            mdStructure.crearCampoUsuario(oscl18);
            mdStructure.crearCampoUsuario(oscl20);
            mdStructure.crearCampoUsuario(oscl22);
            mdStructure.crearCampoUsuario(udcita24);
            mdStructure.crearCampoUsuario(ordr2);
            mdStructure.crearCampoUsuario(ordr4);
            mdStructure.crearCampoUsuario(ordr28);
            mdStructure.crearCampoUsuario(ucforp12);
            mdStructure.crearCampoUsuario(udforp16);
            mdStructure.crearCampoUsuario(dordr12);
            #endregion
        }
    }
}
