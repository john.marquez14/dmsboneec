﻿using SypAddonController.Util;
using SypDevs;
using SypDevs.DB_Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SypAddonController.DBStructure.Dto
{
    public class BfDmsBOneEc
    {

        private static readonly string CATEGORIA = "00. DMS Objetos de Usuario";

        #region Busquedas formateadas

        public static void CreaBusquedasFormateadas()
        {
            try
            {
                MDQueries oMdQueries = new MDQueries();

                oMdQueries.CreateCategories(CATEGORIA);

                oMdQueries.CreateQueries(CATEGORIA, "01. Citas Codigo Automatico", Resources._01__Citas_Codigo_Automatico);
                oMdQueries.CreateQueries(CATEGORIA, "02. Citas Fecha Automatica", Resources._02__Citas_Fecha_Automatica);
                oMdQueries.CreateQueries(CATEGORIA, "03. Citas Sucursales", Resources._03__Citas_Sucursales);
                oMdQueries.CreateQueries(CATEGORIA, "04. CodCliente con aseguradora", Resources._04__CodCliente_con_aseguradora);
                oMdQueries.CreateQueries(CATEGORIA, "05. Condicion Forma de Pago nueva", Resources._05__Condicion_Forma_de_Pago_nueva);

                oMdQueries.CreateFMS(CATEGORIA, "01. Citas Codigo Automatico", "UDO_FT_CIJM",  "0_U_E", "-1", "U_SYP_SUCURSAL", "Y", "Y", "N");
                oMdQueries.CreateFMS(CATEGORIA, "02. Citas Fecha Automatica",  "UDO_FT_CIJM", "14_U_E", "-1", "U_SYP_SUCURSAL", "Y", "Y", "N");
                oMdQueries.CreateFMS(CATEGORIA, "03. Citas Sucursales",        "UDO_FT_CIJM", "13_U_E", "-1", "null", "N", "Y", "N");
                oMdQueries.CreateFMS(CATEGORIA, "04. CodCliente con aseguradora", "149", "4", "-1", "null", "N", "Y", "N");
                oMdQueries.CreateFMS(CATEGORIA, "05. Condicion Forma de Pago nueva", "UDO_FT_OFORPAGO", "0_U_G", "C_0_1", "null", "N", "N", "N");

            }
            catch (Exception ex)
            {
                Globals.SBO_Application.StatusBar.SetText("Error en crear Busquedas Formateadas: " + ex.Message);
            }
        }

        #endregion
    }
}
