﻿
using sypsoft_utilities.DB_Structure;
using SypAddonController.DBStructure.Dto;

namespace SypAddonController.DBStructure
{
    public class CreateStructure
    {
        public static void CrearEstructuras()
        {
            EstructurasDmsBOneEc.CrearEstructuras();
            BfDmsBOneEc.CreaBusquedasFormateadas();
            EstructurasMarcajeDmsEc.CrearEstructuras();
            DmsSequenciasEc.CreateSequencias();
            ParametrosDmsEc.CreateParametros();
        }
    }
}
